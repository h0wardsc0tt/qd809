USE [db_qd809]
GO
/****** Object:  StoredProcedure [dbo].[custom_select]    Script Date: 1/15/2019 12:27:51 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [dbo].[custom_select](
	@pageNumber INT = NULL,
	@itemsPerPage INT = NULL,
	@count INT OUTPUT,
	@qd809serial INT = NULL, 
	@productrange NVARCHAR(150) = NULL, 
	@partnumber NVARCHAR(150) = NULL, 
	@description NVARCHAR(MAX) = NULL, 
	@softwarerelease INT = NULL, 
	@firmwarerelease INT = NULL, 
	@hardwarerelease INT = NULL, 
	@datefromengineering NVARCHAR(15) = NULL, --date
	@engineeringtest NVARCHAR(150) = NULL, 
	@smoketest NVARCHAR(150) = NULL, 
	@customerbetatest NVARCHAR(150) = NULL, 
	@targettedregressiontest NVARCHAR(150) = NULL, 
	@scenariotest NVARCHAR(150) = NULL, 
	@fullregressiontest NVARCHAR(150) = NULL, 
	@integrationtestresult NVARCHAR(150) = NULL, 
	@integrationtestname NVARCHAR(150) = NULL, 
	@datefromintegrationtest NVARCHAR(15) = NULL,  --date
	@notes NVARCHAR(MAX) = NULL, 
	@productiontestsignoff NVARCHAR(150) = NULL, 
	@productiontestsignoffdate NVARCHAR(15) = NULL,  --date
	@productiontestdaterecevied NVARCHAR(15) = NULL,  --date
	@productiontestresult NVARCHAR(150) = NULL, 
	@productiontestname NVARCHAR(150) = NULL, 
	@engineeringtestname NVARCHAR(150) = NULL, 
	@productionpcr INT = NULL, 
	@released INT = NULL, 
	@revision NVARCHAR(150) = NULL, 
	@areastotest NVARCHAR(MAX) = NULL,
	@dateintegrationtest NVARCHAR(15) = NULL, --date
	@datefromengineering_from  NVARCHAR(15) = NULL, --date
    @datefromengineering_to  NVARCHAR(15) = NULL, --date
    @datefromintegrationtest_from  NVARCHAR(15) = NULL, --date
    @datefromintegrationtest_to  NVARCHAR(15) = NULL, --date
    @productiontestsignoffdate_from  NVARCHAR(15) = NULL, --date
    @productiontestsignoffdate_to  NVARCHAR(15) = NULL, --date
	@custom NVARCHAR(50) = NULL,
	@sortcolumn NVARCHAR(50) = NULL,
	@sortorder NVARCHAR(5) = NULL,
	@isactive INT = NULL
) AS

DECLARE
	@rowCount   NVARCHAR(100),
	@pageFetch  NVARCHAR(300),
	@cnt INT,
	@recordSet INT,
	@currentDate DateTime,
	@culture nvarchar(10),
	@emptyintegrationtestname bit,
	@emptyproductiontestname bit,
	@emptyintegrationtestresult bit,
	@emptyproductiontestresult bit

SET @culture = 'en-GB'
SET @currentDate = GETDATE()

SET @emptyintegrationtestname = NULL
IF(@integrationtestname = '-3')
BEGIN
	SET @emptyintegrationtestname = 1
	SET @integrationtestname = NULL
END

IF(@productiontestname = '-3')
BEGIN
	SET @emptyproductiontestname = 1
	SET @productiontestname = NULL
END

SET @emptyproductiontestresult = NULL
IF(@productiontestresult = '-3')
BEGIN
	SET @emptyproductiontestresult = 1
	SET @productiontestresult = NULL
END

SET @emptyintegrationtestresult = NULL
IF(@integrationtestresult = '-3')
BEGIN
	SET @emptyintegrationtestresult = 1
	SET @integrationtestresult = NULL
END

SELECT @cnt = count(*) 
FROM [TrilogyTestData]
WHERE isactive = 1
 AND (@custom  != 'qd809serial'  OR ([QD809 Serial] = @qd809serial))
 AND (@custom != 'search' OR (@datefromengineering_from IS NULL OR  [Date from engineering] >= convert(DATETIME, @datefromengineering_from, 103))
	AND (@datefromengineering_to IS NULL OR [Date from engineering] <= convert(DATETIME, @datefromengineering_to, 103))
	AND (@datefromintegrationtest_from IS NULL OR  [Date from Integration Test] >= convert(DATETIME, @datefromintegrationtest_from, 103))
	AND (@datefromintegrationtest_to IS NULL OR [Date from Integration Test] <= convert(DATETIME, @datefromintegrationtest_to, 103))
	AND (@productiontestsignoffdate_from IS NULL OR  [Production Test Signoff Date] >= convert(DATETIME, @productiontestsignoffdate_from, 103))
	AND (@productiontestsignoffdate_to IS NULL OR [Production Test Signoff Date] <=convert(DATETIME, @productiontestsignoffdate_to, 103) )
	AND (@integrationtestname IS NULL OR [Integration Test Name] = @integrationtestname )
	AND (@emptyintegrationtestname IS NULL OR [Integration Test Name] IS NULL )
	AND (@productiontestname IS NULL OR [Production Test Name] = @productiontestname )
	AND (@emptyproductiontestname IS NULL OR [Production Test Name] IS NULL )
	AND (@integrationtestresult IS NULL OR [Integration Test Result] = @integrationtestresult )
	AND (@emptyintegrationtestresult IS NULL OR [Integration Test Result] IS NULL )
	AND (@productiontestresult IS NULL OR [Production Test Result] = @productiontestresult )
	AND (@emptyproductiontestresult IS NULL OR [Production Test Result] IS NULL )
	AND (@productionpcr IS NULL OR CAST([Production PCR] AS VARCHAR(20)) LIKE '%' + CAST(@productionpcr AS VARCHAR(20)) + '%' )
	AND (@partnumber IS NULL OR [Part Number] LIKE '%' + @partnumber + '%' )
	AND (@revision IS NULL OR [Revision] LIKE '%' + @revision + '%' )
	)

SET @count = @cnt

SELECT *
	FROM
	(
		SELECT 
			 [QD809 Serial] AS qd809serial
			,[Product Range] AS productrange
			,[Part Number] AS partnumber
			,[Description] AS description
			,[Software Release] AS softwarerelease
			,[Firmware Release] AS firmwarerelease
			,[Hardware Release] AS hardwarerelease	
			,CONVERT(VARCHAR(10), [Date from engineering], 103) AS datefromengineering	
			,[Engineering Test] AS engineeringtest
			,[Smoke Test] AS smoketest
			,[Customer Beta Test] AS customerbetatest
			,[Targetted Regression Test] AS targettedregressiontest
			,[Scenario Test] AS scenariotest
			,[Full Regression Test] AS fullregressiontest
			,[Integration Test Result] AS integrationtestresult
			,[Integration Test Name] AS integrationtestname
			,CONVERT(VARCHAR(10), [Date from Integration Test], 103) AS datefromintegrationtest	
			,[Notes] AS notes
			,[Production Test Signoff]  AS productiontestsignoff	
			,CONVERT(VARCHAR(10), [Production Test Signoff Date], 103) AS productiontestsignoffdate	
			,CONVERT(VARCHAR(10), [Production Test Date Recevied], 103) AS productiontestdaterecevied			
			,[Production Test Result] AS productiontestresult
			,[Production Test Name] AS productiontestname
			,[Production PCR] AS productionpcr
			,[Released] AS released
			,[Revision] AS revision
			,[Areas To Test] AS areastotest	
			,[Engineering Test Name] AS engineeringtestname	
			,CONVERT(VARCHAR(10), [Date Integration Test], 103) AS dateintegrationtest	
			,qd809.isactive
			,[Previous Integration Test Name] AS previousintegrationtestname
			,[Previous Production Test Name] AS previousproductiontestname
			,[Previous Engineering Test Name] AS previousengineeringtestname
			,ROW_NUMBER() OVER 
			(
			ORDER BY
				 (case when @sortcolumn = '[QD809 Serial]' and @sortorder = 'asc' then [QD809 Serial] end)  asc,
				 (case when @sortcolumn = '[QD809 Serial]' and @sortorder = 'desc' then [QD809 Serial] end) desc,
				 (case when @sortcolumn = '[Product Range]' and @sortorder = 'asc' then sortProductionrange.ProductRange end) asc,
				 (case when @sortcolumn = '[Product Range]' and @sortorder = 'desc' then sortProductionrange.ProductRange end) desc,
				 (case when @sortcolumn = '[Part Number]' and @sortorder = 'asc' then LOWER([Part Number]) end) asc,
				 (case when @sortcolumn = '[Part Number]' and @sortorder = 'desc' then LOWER([Part Number]) end) desc,
				 (case when @sortcolumn = '[Revision]' and @sortorder = 'asc' then LOWER([Revision]) end) asc,
				 (case when @sortcolumn = '[Revision]' and @sortorder = 'desc' then LOWER([Revision]) end) desc,
				 (case when @sortcolumn = '[Date from engineering]' and @sortorder = 'asc' then [Date from engineering] end) asc,
				 (case when @sortcolumn = '[Date from engineering]' and @sortorder = 'desc' then [Date from engineering] end) desc,
				 (case when @sortcolumn = '[Integration Test Result]' and @sortorder = 'asc' then sortIntegrationresult.Results end) asc,
				 (case when @sortcolumn = '[Integration Test Result]' and @sortorder = 'desc' then sortIntegrationresult.Results end) desc,
				 (case when @sortcolumn = '[Production Test Result]' and @sortorder = 'asc' then sortProductionresult.Results end) asc,
				 (case when @sortcolumn = '[Production Test Result]' and @sortorder = 'desc' then sortProductionresult.Results end) desc,
				 (case when @sortcolumn = '[Production Test Signoff Date]' and @sortorder = 'asc' then [Production Test Signoff Date] end) asc,
				 (case when @sortcolumn = '[Production Test Signoff Date]' and @sortorder = 'desc' then [Production Test Signoff Date] end) desc,
				 (case when @sortcolumn = '[Date from Integration Test]' and @sortorder = 'asc' then [Date from Integration Test] end) asc,
				 (case when @sortcolumn = '[Date from Integration Test]' and @sortorder = 'desc' then [Date from Integration Test] end) desc,
				 (case when @sortcolumn = '[Production Test Date Recevied]' and @sortorder = 'asc' then [Production Test Date Recevied] end) asc,
				 (case when @sortcolumn = '[Production Test Date Recevied]' and @sortorder = 'desc' then [Production Test Date Recevied] end) desc,
				 (case when @sortcolumn = '[Production PCR]' and @sortorder = 'asc' then LOWER([Production PCR]) end) asc,
				 (case when @sortcolumn = '[Production PCR]' and @sortorder = 'desc' then LOWER([Production PCR]) end) desc,
				 (case when @sortcolumn = '[Integration Test Name]' and @sortorder = 'asc' then LOWER(sortUsers.FirstName) end) asc,
				 (case when @sortcolumn = '[Integration Test Name]' and @sortorder = 'desc' then LOWER(sortUsers.FirstName) end) desc,
				 (case when @sortcolumn = '[Production Test Name]' and @sortorder = 'asc' then LOWER(sortProductionUsers.FirstName) end) asc,
				 (case when @sortcolumn = '[Production Test Name]' and @sortorder = 'desc' then LOWER(sortProductionUsers.FirstName) end) desc

			) AS RowNum
		FROM [dbo].[TrilogyTestData] qd809
			LEFT JOIN Results sortIntegrationresult  ON sortIntegrationresult.[key] = qd809.[Integration Test Result]
			LEFT JOIN Results sortProductionresult  ON sortProductionresult.[key] = qd809.[Production Test Result]
			LEFT JOIN ProductRange sortProductionrange  ON sortProductionrange.[key] = qd809.[Product Range]
			LEFT JOIN AspNetUsers sortUsers ON sortUsers.Id = qd809.[Integration Test Name]
			LEFT JOIN AspNetUsers sortProductionUsers ON sortProductionUsers.Id = qd809.[Production Test Name]
		WHERE qd809.isactive = 1
		 AND (@custom  != 'qd809serial'  OR ([QD809 Serial] = @qd809serial))
		 AND (@custom != 'search' OR (@datefromengineering_from IS NULL OR  [Date from engineering] >= convert(DATETIME, @datefromengineering_from, 103))
			AND (@datefromengineering_to IS NULL OR [Date from engineering] <= convert(DATETIME, @datefromengineering_to, 103))
			AND (@datefromintegrationtest_from IS NULL OR  [Date from Integration Test] >= convert(DATETIME, @datefromintegrationtest_from, 103))
			AND (@datefromintegrationtest_to IS NULL OR [Date from Integration Test] <= convert(DATETIME, @datefromintegrationtest_to, 103))
			AND (@productiontestsignoffdate_from IS NULL OR  [Production Test Signoff Date] >= convert(DATETIME, @productiontestsignoffdate_from, 103))
			AND (@productiontestsignoffdate_to IS NULL OR [Production Test Signoff Date] <=convert(DATETIME, @productiontestsignoffdate_to, 103) )
			AND (@integrationtestname IS NULL OR [Integration Test Name] = @integrationtestname )
			AND (@emptyintegrationtestname IS NULL OR [Integration Test Name] IS NULL )
			AND (@productiontestname IS NULL OR [Production Test Name] = @productiontestname )
			AND (@emptyproductiontestname IS NULL OR [Production Test Name] IS NULL )
			AND (@integrationtestresult IS NULL OR [Integration Test Result] = @integrationtestresult )
			AND (@emptyintegrationtestresult IS NULL OR [Integration Test Result] IS NULL )
			AND (@productiontestresult IS NULL OR [Production Test Result] = @productiontestresult )
			AND (@emptyproductiontestresult IS NULL OR [Production Test Result] IS NULL )
			AND (@productionpcr IS NULL OR CAST([Production PCR] AS VARCHAR(20)) LIKE '%' + CAST(@productionpcr AS VARCHAR(20)) + '%' )
			AND (@partnumber IS NULL OR [Part Number] LIKE '%' + @partnumber + '%' )
			AND (@revision IS NULL OR [Revision] LIKE '%' + @revision + '%' )
			)
	)t
WHERE
      (RowNum >(@pageNumber * @itemsPerPage) - @itemsPerPage )
      AND 
      (RowNum <= (@pageNumber * @itemsPerPage))
