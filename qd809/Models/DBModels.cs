﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Runtime.Serialization;


using HME.SQLSERVER.DAL;
using QD809.Models;
using System.Reflection;
namespace QD809.Models
{
    public class DBModels
    {

        #region Constructors

        public DBModels()
        {
        }

        #endregion

        #region Properties

        #region public class responseDTO
        [Serializable]
        public class responseDTO
        {
            public string key { get; set; }
            public string status { get; set; }
            public string type { get; set; }
            public string message { get; set; }
            public Dictionary<string, string> updatedto { get; set; }
            public DropdownTransfer transfer { get; set; }
        }

        public class productrangeDTO
        {
            public string key { get; set; }
            public string status { get; set; }
            public string updatetype { get; set; }
            public string message { get; set; }
            public string isactive { get; set; }
            public string productrange { get; set; }
            public string previousproductrange { get; set; }
        }

        public class resultsDTO
        {
            public string key { get; set; }
            public string status { get; set; }
            public string updatetype { get; set; }
            public string message { get; set; }
            public string isactive { get; set; }
            public string results { get; set; }
            public string previousresults { get; set; }
         }
        public class DropdownTransfer
        {
            public string fromid { get; set; }
            public string status { get; set; }
            public string message { get; set; }
            public List<DropdownTransferList> list { get; set; }
            public List<emailsTransfered> emailsTransferedList { get; set; }
    }

        public class DropdownTransferList
        {
            public string toList { get; set; }
            public string toid { get; set; }
        }

        public class emailsTransfered
        {
            public string dropdown { get; set; }
            public int qd809serial { get; set; }
            public string id { get; set; }
            public string productrange { get; set; }
            public string firstname { get; set; }
            public string lastname { get; set; }
            public string email { get; set; }
        }
        #endregion

        #endregion

        #region Methods

        #region Update
        public responseDTO Update(Dictionary<string, string> dto)
        {
            List<SqlParameter> sp = new List<SqlParameter>();

            #region foreach SqlParameter
            foreach (var item in dto)
            {
                if (item.Key != "status")
                {
                    sp.Add(new SqlParameter(item.Key, item.Value));
                }
            }
            #endregion

            //SqlParameter id = new SqlParameter("id", SqlDbType.NVarChar);
            //id.Direction = ParameterDirection.Output;
            //sp.Add(id);

            dbSQL db = new dbSQL();
            NonQueryMessage nqm = db.execNonQuery(CommandType.StoredProcedure, "update_email", sp);
            db.Dispose();

            responseDTO responseDTO = new responseDTO();
            responseDTO.type = dto["type"];
            responseDTO.key = dto["key"];
            responseDTO.status = nqm.Message == "" ? "OK" : "Failed";
            responseDTO.message = nqm.Message == "" ? "" : nqm.Message;
            return responseDTO;
        }
        #endregion

        #region Update
        public productrangeDTO UpdateProductRange(productrangeDTO dto)
        {
            List<SqlParameter> sp = new List<SqlParameter>();


            sp.Add(new SqlParameter("key", dto.key));
            sp.Add(new SqlParameter("updatetype", dto.updatetype));
            sp.Add(new SqlParameter("productrange", dto.productrange));
            sp.Add(new SqlParameter("isactive", dto.isactive));

            SqlParameter id = new SqlParameter("id", SqlDbType.Int);
            id.Direction = ParameterDirection.Output;
            sp.Add(id);

            SqlParameter count = new SqlParameter("count", SqlDbType.Int);
            count.Direction = ParameterDirection.Output;
            sp.Add(count);

            dbSQL db = new dbSQL();
            NonQueryMessage nqm = db.execNonQuery(CommandType.StoredProcedure, "update_productrange", sp);
            db.Dispose();

            if (dto.updatetype == "delete" && Convert.ToInt32(count.Value) > 0)
            {
                dto.key = id.Value.ToString();
                dto.status = nqm.Message == "" ? "Assigned" : "Failed";
                dto.message = nqm.Message == "" ? "" : nqm.Message;
            }else
            {
                dto.key = id.Value.ToString();
                dto.status = nqm.Message == "" ? "OK" : "Failed";
                dto.message = nqm.Message == "" ? "" : nqm.Message;
            }
            return dto;
        }

        public resultsDTO UpdateResults(resultsDTO dto)
        {
            List<SqlParameter> sp = new List<SqlParameter>();


            sp.Add(new SqlParameter("key", dto.key));
            sp.Add(new SqlParameter("updatetype", dto.updatetype));
            sp.Add(new SqlParameter("results", dto.results));
            sp.Add(new SqlParameter("isactive", dto.isactive));

            SqlParameter id = new SqlParameter("id", SqlDbType.Int);
            id.Direction = ParameterDirection.Output;
            sp.Add(id);

            SqlParameter count = new SqlParameter("count", SqlDbType.Int);
            count.Direction = ParameterDirection.Output;
            sp.Add(count);

            dbSQL db = new dbSQL();
            NonQueryMessage nqm = db.execNonQuery(CommandType.StoredProcedure, "update_results", sp);
            db.Dispose();


            if (dto.updatetype == "delete" && Convert.ToInt32(count.Value) > 0)
            {
                dto.key = id.Value.ToString();
                dto.status = nqm.Message == "" ? "Assigned" : "Failed";
                dto.message = nqm.Message == "" ? "" : nqm.Message;
            }
            else
            {
                dto.key = id.Value.ToString();
                dto.status = nqm.Message == "" ? "OK" : "Failed";
                dto.message = nqm.Message == "" ? "" : nqm.Message;
            }
            return dto;
        }

        public resultsDTO UpdateResultsDefault(resultsDTO dto)
        {
            List<SqlParameter> sp = new List<SqlParameter>();

            sp.Add(new SqlParameter("key", dto.key));
            sp.Add(new SqlParameter("results", dto.results));

            dbSQL db = new dbSQL();
            NonQueryMessage nqm = db.execNonQuery(CommandType.StoredProcedure, "update_results_default", sp);
            db.Dispose();

            dto.status = nqm.Message == "" ? "OK" : "Failed";
            dto.message = nqm.Message == "" ? "" : nqm.Message;

            return dto;
        }

        #endregion

        #region public responseDTO Transfer(DropdownTransfer dto)
        public responseDTO Transfer(DropdownTransfer dto)
        {
            responseDTO responseDTO = new responseDTO();
            List<SqlParameter> sp = new List<SqlParameter>();

            sp.Add(new SqlParameter("fromid", dto.fromid));
            
            #region foreach SqlParameter
            foreach (DropdownTransferList item in dto.list)
            {
                sp.Add(new SqlParameter(item.toList, item.toid));
            }
            #endregion

            dbSQL db = new dbSQL();
            NonQueryMessage nqm = db.execNonQuery(CommandType.StoredProcedure, "update_dropdown", sp);
            db.Dispose();

            responseDTO.transfer = dto;
            responseDTO.status = nqm.Message == "" ? "OK" : "Failed";
            responseDTO.message = nqm.Message == "" ? "" : nqm.Message;
            responseDTO.transfer.message = responseDTO.message;
            responseDTO.transfer.status = responseDTO.status;
            return responseDTO;

        }
        #endregion

        #region public List<emailsTransfered> emails_Transfered(DropdownTransfer dto)
        public List<emailsTransfered> emails_Transfered(DropdownTransfer dto)
        {
            emailsTransfered emailTransfered = new emailsTransfered();
            List<emailsTransfered> emailsTransferedList = new List<emailsTransfered>();
            string msg = string.Empty;
            List<SqlParameter> sp = new List<SqlParameter>();

            sp.Add(new SqlParameter("fromid", dto.fromid));

            #region foreach SqlParameter
            foreach (DropdownTransferList item in dto.list)
            {
                sp.Add(new SqlParameter(item.toList, item.toid));
            }
            #endregion

            dbSQL db = new dbSQL();
            DataSet ds = db.executeStoredProcedure("dropdown_email_changes", sp, out msg);
            db.Dispose();

            foreach(DataRow row in ds.Tables[0].Rows)
            {
                emailTransfered = new emailsTransfered();
                emailTransfered.dropdown = row["dropdown"].ToString();
                emailTransfered.id = row["id"].ToString();
                emailTransfered.qd809serial = Convert.ToInt32(row["qd809serial"]);
                emailTransfered.firstname = row["firstname"].ToString();
                emailTransfered.lastname = row["lastname"].ToString();
                emailTransfered.productrange = row["productrange"].ToString();
                emailTransfered.email = row["email"].ToString();
                emailsTransferedList.Add(emailTransfered);
            }

            return emailsTransferedList;

        }
        #endregion

        #endregion
    }
}