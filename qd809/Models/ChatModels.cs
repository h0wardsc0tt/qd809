﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace QD809.Models
{
    public class ChatModels
    {
        #region Constructors

        public ChatModels()
        {
        }

        #endregion

        #region Properties  

        [Serializable]
        public class ChatDTO
        {
            public string request { get; set; }
            public string id { get; set; }
            public int qd809serialnumber { get; set; }
            public DateTime date { get; set; }
            public string status { get; set; }
            public string firstname { get; set; }
            public string lastname { get; set; }
            public string updatedto { get; set; }
        }

        #endregion

    }
}