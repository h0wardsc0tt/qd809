﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace QD809.Models
{
    public class AjaxModels
    {
        [Serializable]
        public class QD809SearchResults
        {
            public QD809SearchFilters searchfilters { get; set; }
            public string count { get; set; }
            public string itemsperpage { get; set; }
            public string pageend { get; set; }
            public string pagestart { get; set; }
            public string pagenumber { get; set; }
            public HomeModels.testrecord records { get; set; }
        }

        [Serializable]
        public class QD809SearchFilters
        {
            public string custom { get; set; }
            public string sortcolumn { get; set; }
            public string sortorder { get; set; }
        }
    }
}