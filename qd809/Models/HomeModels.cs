﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Runtime.Serialization;


using HME.SQLSERVER.DAL;
using QD809.Models;
using System.Reflection;

namespace QD809.Models
{
    public class HomeModels
    {

        #region Constructors

        public HomeModels()
        {
        }

        #endregion

        #region Properties
        [Serializable]
        public class Qd809BaseObj
        {
            public testrecord record { get; set; }
            public filterDTO dto { get; set; }
            public dropdowns dropdowns { get; set; }
            public string userfirstname { get; set; }
            public string userlastname { get; set; }
            public string useremailaddress { get; set; }
            public bool isadmin { get; set; }
        }

        [Serializable]
        public class updateDTO
        {
            public string status { get; set; }
            public string updatetype { get; set; }
            public testrecord record { get; set; }
        }

        [Serializable]
        public class filterDTO
        {
            public string status { get; set; }
            public string custom { get; set; }
            public string storedprocedure { get; set; }
            public string sortcolumn { get; set; }
            public string sortorder { get; set; }
            public int count { get; set; }
            public int pagenumber { get; set; }
            public int itemsperpage { get; set; }
            public string message { get; set; }
            public List<testrecord> records { get; set; }
            public Filters filters { get; set; }

        }

        [Serializable]
        public class Filters
        {
            public string qd809serial { get; set; }
            public string datefromengineering_from { get; set; }
            public string datefromengineering_to { get; set; }
            public string datefromintegrationtest_from { get; set; }
            public string datefromintegrationtest_to { get; set; }
            public string productiontestsignoffdate_from { get; set; }
            public string productiontestsignoffdate_to { get; set; }
            public string integrationtestname { get; set; }
            public string productiontestname { get; set; }
            public string integrationtestresult { get; set; }
            public string partnumber { get; set; }
            public string productiontestresult { get; set; }
            public string productionpcr { get; set; }
            public string revision { get; set; }
        }

        [Serializable]
        public class testrecord
        {
            //[DataMember(IsRequired = false)]
            public string qd809serial { get; set; }
            public string productrange { get; set; }
            public string partnumber { get; set; }
            public string description { get; set; }
            public string softwarerelease { get; set; }
            public string firmwarerelease { get; set; }
            public string hardwarerelease { get; set; }
            public string datefromengineering { get; set; }
            public string engineeringtest { get; set; }
            public string smoketest { get; set; }
            public string customerbetatest { get; set; }
            public string targettedregressiontest { get; set; }
            public string scenariotest { get; set; }
            public string fullregressiontest { get; set; }
            public string integrationtestresult { get; set; }
            public string integrationtestname { get; set; }
            public string datefromintegrationtest { get; set; }
            public string notes { get; set; }
            public string productiontestsignoff { get; set; }
            public string productiontestdaterecevied { get; set; }
            public string productiontestresult { get; set; }
            public string productiontestname { get; set; }
            public string productionpcr { get; set; }
            public string released { get; set; }
            public string revision { get; set; }
            public string areastotest { get; set; }
            public string engineeringtestname { get; set; }
            public string dateintegrationtest { get; set; }
            public string productiontestsignoffdate { get; set; }
            public string isactive { get; set; }
            public string previousintegrationtestname { get; set; }
            public string previousproductiontestname { get; set; }
            public string previousengineeringtestname { get; set; }
        }

        // public class testrecordobj
        // {
        //     public string qd809serial;
        //     public string productrange;
        //     public string partnumber;
        //     public string description;
        //     public string softwarerelease;
        //     public string firmwarerelease;
        //     public string hardwarerelease;
        //     public string datefromengineering;
        //     public string engineeringtest;
        //     public string smoketest;
        //     public string customerbetatest;
        //     public string targettedregressiontest;
        //     public string scenariotest;
        //     public string fullregressiontest;
        //     public string integrationtestresult;
        //     public string integrationtestname;
        //     public string datefromintegrationtest;
        //     public string notes;
        //     public string productiontestsignoff;
        //     public string productiontestdaterecevied;
        //     public string productiontestresult;
        //     public string productiontestname;
        //     public string productionpcr;
        //     public string released;
        //     public string revision;
        //     public string areastotest;
        // }

        [Serializable]
        public class email
        {
            public string key { get; set; }
            public string integration { get; set; }
            public string production { get; set; }
            public string engineering { get; set; }
            public string firstname { get; set; }
            public string lastname { get; set; }
            public string emailaddress { get; set; }
            public string address { get; set; }
            public string isactive { get; set; }
        }

        [Serializable]
        public class emailname
        {
            public string key { get; set; }
            public string firstname { get; set; }
            public string lastname { get; set; }
            public string isactive { get; set; }
        }

        [Serializable]
        public class integrationtestname
        {
            public int key { get; set; }
             public string firstname { get; set; }
            public string lastname { get; set; }
        }

        [Serializable]
        public class productiontestname
        {
            public int key { get; set; }
            public string firstname { get; set; }
            public string lastname { get; set; }
        }

        [Serializable]
        public class productrange
        {
            public int key { get; set; }
            public string product_range { get; set; }
            public string isactive { get; set; }
        }

        [Serializable]
        public class dropdowns
        {
            public List<email> emails { get; set; }
            public List<emailname> integrationtestnames { get; set; }
            public List<emailname> productiontestnames { get; set; }
            public List<emailname> engineeringtestnames { get; set; }
            public List<productrange> productranges { get; set; }
            public List<result> results { get; set; }
        }

        [Serializable]
        public class result
        {
            public int key { get; set; }
            public string status { get; set; }
            public string isactive { get; set; }
            public string integrationdefaultvalue { get; set; }
            public string productiondefaultvalue { get; set; }
        }

        [Serializable]
        public class responseDTO
        {
            public string qd809serial { get; set; }
            public string status { get; set; }
            public string updatetype { get; set; }
            public string message { get; set; }
            public testrecord record { get; set; }
            public Dictionary<string, string> updatedto { get; set; }
        }

        #endregion

        #region Methods

        #region GetDropDowns
        public Qd809BaseObj GetDropDowns()
        {
            List<email> listofemails = new List<email>();
            List<emailname> listofintegrationtestnames = new List<emailname>();
            List<emailname> listofproductiontestnames = new List<emailname>();
            List<emailname> listofengineeringtestname = new List<emailname>();
            List<productrange> listofproductranges = new List<productrange>();
            List<result> listofresults = new List<result>();

            dropdowns dropdowns = new dropdowns();
            Qd809BaseObj trilogy = new Qd809BaseObj();

            dbSQL db = new dbSQL();
            string sqlMessage = string.Empty;
            DataSet ds;
            ds = db.executeStoredProcedure("get_dropdowns", out sqlMessage);

            listofemails = ds.Tables[0].AsEnumerable().Select(m => new email()
            {
                key = Convert.ToString(m["key"]),
                integration = Convert.ToString(m["integration"]),
                production = Convert.ToString(m["production"]),
                engineering = Convert.ToString(m["engineering"]),
                firstname = Convert.ToString(m["firstname"]),
                lastname = Convert.ToString(m["lastname"]),
                emailaddress = Convert.ToString(m["emailaddress"]),
                address = Convert.ToString(m["address"]),
                isactive = Convert.ToString(m["isactive"])
                //id = Convert.ToString(m["id"])
            }).ToList();

            listofproductiontestnames = ds.Tables[1].AsEnumerable().Select(m => new emailname()
            {
                key = Convert.ToString(m["key"]),
                firstname = Convert.ToString(m["firstname"]),
                lastname = Convert.ToString(m["lastname"]),
                isactive = Convert.ToString(m["isactive"])
            }).ToList();

            listofintegrationtestnames = ds.Tables[2].AsEnumerable().Select(m => new emailname()
            {
                key = Convert.ToString(m["key"]),
                firstname = Convert.ToString(m["firstname"]),
                lastname = Convert.ToString(m["lastname"]),
                isactive = Convert.ToString(m["isactive"])
            }).ToList();

            listofengineeringtestname = ds.Tables[3].AsEnumerable().Select(m => new emailname()
            {
                key = Convert.ToString(m["key"]),
                firstname = Convert.ToString(m["firstname"]),
                lastname = Convert.ToString(m["lastname"]),
                isactive = Convert.ToString(m["isactive"])
            }).ToList();

            listofproductranges = ds.Tables[4].AsEnumerable().Select(m => new productrange()
            {
                key = Convert.ToInt32(m["key"]),
                product_range = Convert.ToString(m["product_range"]),
                isactive = Convert.ToString(m["isactive"]),
            }).ToList();

            listofresults = ds.Tables[5].AsEnumerable().Select(m => new result()
            {
                key = Convert.ToInt32(m["key"]),
                status = Convert.ToString(m["status"]),
                isactive = Convert.ToString(m["isactive"]),
                integrationdefaultvalue = Convert.ToString(m["integrationdefaultvalue"]),
                productiondefaultvalue = Convert.ToString(m["productiondefaultvalue"])
            }).ToList();

            dropdowns.emails = listofemails;
            dropdowns.integrationtestnames = listofintegrationtestnames;
            dropdowns.productiontestnames = listofproductiontestnames;
            dropdowns.engineeringtestnames = listofengineeringtestname;
            dropdowns.productranges = listofproductranges;
            dropdowns.results = listofresults;

            trilogy.dropdowns = dropdowns;
            return trilogy;
        }

        #endregion

        #region Update
        public responseDTO Update(Dictionary<string, string> dto)
        {
            List<SqlParameter> sp = new List<SqlParameter>();

            #region foreach SqlParameter
            foreach (var item in dto)
            {
                if (item.Key != "status")
                {
                    sp.Add(new SqlParameter(item.Key, item.Value));
                }
            }
            #endregion

            SqlParameter serialNumber = new SqlParameter("serialnumber", SqlDbType.Int);
            serialNumber.Direction = ParameterDirection.Output;
            sp.Add(serialNumber);

            dbSQL db = new dbSQL();
            NonQueryMessage nqm = db.execNonQuery(CommandType.StoredProcedure, "update_qd809", sp);
            db.Dispose();

            responseDTO responseDTO = new responseDTO();
            responseDTO.updatetype = dto["updatetype"];
            responseDTO.qd809serial = serialNumber.Value.ToString();
            responseDTO.status = nqm.Message == "" ? "OK" : "Failed";
            responseDTO.message = nqm.Message == "" ? "" : nqm.Message;
            return responseDTO;
        }

        #endregion

        #region Select
        public filterDTO Select(filterDTO dto)
        {
            List<SqlParameter> sp = new List<SqlParameter>();
            sp.Add(new SqlParameter("sortcolumn", dto.sortcolumn));
            sp.Add(new SqlParameter("pagenumber", dto.pagenumber));
            sp.Add(new SqlParameter("itemsperpage", dto.itemsperpage));
            sp.Add(new SqlParameter("sortorder", dto.sortorder));
            sp.Add(new SqlParameter("custom", dto.custom));

            Type myClassType = dto.filters.GetType();
            PropertyInfo[] properties = myClassType.GetProperties();
            object value = string.Empty;

            #region foreach property
            foreach (PropertyInfo property in properties as IEnumerable)
            {
                value = property.GetValue(dto.filters, null);
                if (value != null)
                {
                    //if (value.ToString() == "0")
                    //{
                    //    value = null;
                    //}
                }
                sp.Add(new SqlParameter(property.Name, value));
            }
            #endregion

            SqlParameter count = new SqlParameter("count", SqlDbType.Int);
            count.Direction = ParameterDirection.Output;
            sp.Add(count);

            dbSQL db = new dbSQL();
            DataSet ds = new DataSet();
            string sqlMessage = string.Empty;
            ds = db.executeStoredProcedure("custom_select", sp, out sqlMessage);
            db.Dispose();

            List<testrecord> listofrecords = new List<testrecord>();

            #region listofrecords
            listofrecords = ds.Tables[0].AsEnumerable().Select(m => new testrecord()
            {
                qd809serial = Convert.ToString(m["qd809serial"]),
                productrange = Convert.ToString(m["productrange"]),
                partnumber = Convert.ToString(m["partnumber"]),
                description = Convert.ToString(m["description"]),
                softwarerelease = Convert.ToString(m["softwarerelease"]) == "True" ? "1" : "0",
                firmwarerelease = Convert.ToString(m["firmwarerelease"]) == "True" ? "1" : "0",
                hardwarerelease = Convert.ToString(m["hardwarerelease"]) == "True" ? "1" : "0",
                datefromengineering = Convert.ToString(m["datefromengineering"]),
                engineeringtest = Convert.ToString(m["engineeringtest"]),
                smoketest = Convert.ToString(m["smoketest"]),
                customerbetatest = Convert.ToString(m["customerbetatest"]),
                targettedregressiontest = Convert.ToString(m["targettedregressiontest"]),
                scenariotest = Convert.ToString(m["scenariotest"]),
                fullregressiontest = Convert.ToString(m["fullregressiontest"]),
                integrationtestresult = Convert.ToString(m["integrationtestresult"]),
                integrationtestname = Convert.ToString(m["integrationtestname"]),
                datefromintegrationtest = Convert.ToString(m["datefromintegrationtest"]),
                dateintegrationtest = Convert.ToString(m["dateintegrationtest"]),
                notes = Convert.ToString(m["notes"]),
                productiontestsignoff = Convert.ToString(m["productiontestsignoff"]),
                productiontestsignoffdate = Convert.ToString(m["productiontestsignoffdate"]),
                productiontestdaterecevied = Convert.ToString(m["productiontestdaterecevied"]),
                productiontestresult = Convert.ToString(m["productiontestresult"]),
                productiontestname = Convert.ToString(m["productiontestname"]),
                productionpcr = Convert.ToString(m["productionpcr"]),
                released = Convert.ToString(m["released"]),
                revision = Convert.ToString(m["revision"]),
                areastotest = Convert.ToString(m["areastotest"]),
                engineeringtestname = Convert.ToString(m["engineeringtestname"]),
                isactive = Convert.ToString(m["isactive"]) == "True" ? "1" : "0",
                previousintegrationtestname = Convert.ToString(m["previousintegrationtestname"]),
                previousproductiontestname = Convert.ToString(m["previousproductiontestname"]),
                previousengineeringtestname = Convert.ToString(m["previousengineeringtestname"])


            }).ToList();
            #endregion

            dto.count = (int)count.Value; //listofrecords.Count;
            dto.records = listofrecords;
            dto.status = sqlMessage == "OK" ? "OK" : "Failed";
            dto.message = sqlMessage == "OK" ? "" : sqlMessage;
            return dto;
        }

        #endregion

       #endregion

    }
}