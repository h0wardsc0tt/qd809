CREATE DATABASE [db_qd809]
GO
USE [db_qd809]
GO
/****** Object:  User [DB_WEB_USER]    Script Date: 11/10/2017 4:33:16 PM ******/
CREATE USER [DB_WEB_USER] FOR LOGIN [DB_WEB_USER] WITH DEFAULT_SCHEMA=[dbo]
GO
/****** Object:  Table [dbo].[__MigrationHistory]    Script Date: 11/10/2017 4:33:16 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[__MigrationHistory](
	[MigrationId] [nvarchar](150) NOT NULL,
	[ContextKey] [nvarchar](300) NOT NULL,
	[Model] [varbinary](max) NOT NULL,
	[ProductVersion] [nvarchar](32) NOT NULL,
 CONSTRAINT [PK_dbo.__MigrationHistory] PRIMARY KEY CLUSTERED 
(
	[MigrationId] ASC,
	[ContextKey] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[AspNetRoles]    Script Date: 11/10/2017 4:33:16 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AspNetRoles](
	[Id] [nvarchar](128) NOT NULL,
	[Name] [nvarchar](256) NOT NULL,
 CONSTRAINT [PK_dbo.AspNetRoles] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[AspNetUserClaims]    Script Date: 11/10/2017 4:33:16 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AspNetUserClaims](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[UserId] [nvarchar](128) NOT NULL,
	[ClaimType] [nvarchar](max) NULL,
	[ClaimValue] [nvarchar](max) NULL,
 CONSTRAINT [PK_dbo.AspNetUserClaims] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[AspNetUserLogins]    Script Date: 11/10/2017 4:33:16 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AspNetUserLogins](
	[LoginProvider] [nvarchar](128) NOT NULL,
	[ProviderKey] [nvarchar](128) NOT NULL,
	[UserId] [nvarchar](128) NOT NULL,
 CONSTRAINT [PK_dbo.AspNetUserLogins] PRIMARY KEY CLUSTERED 
(
	[LoginProvider] ASC,
	[ProviderKey] ASC,
	[UserId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[AspNetUserRoles]    Script Date: 11/10/2017 4:33:16 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AspNetUserRoles](
	[UserId] [nvarchar](128) NOT NULL,
	[RoleId] [nvarchar](128) NOT NULL,
 CONSTRAINT [PK_dbo.AspNetUserRoles] PRIMARY KEY CLUSTERED 
(
	[UserId] ASC,
	[RoleId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[AspNetUsers]    Script Date: 11/10/2017 4:33:16 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AspNetUsers](
	[Id] [nvarchar](128) NOT NULL,
	[Email] [nvarchar](256) NULL,
	[EmailConfirmed] [bit] NOT NULL,
	[PasswordHash] [nvarchar](max) NULL,
	[SecurityStamp] [nvarchar](max) NULL,
	[PhoneNumber] [nvarchar](max) NULL,
	[PhoneNumberConfirmed] [bit] NOT NULL,
	[TwoFactorEnabled] [bit] NOT NULL,
	[LockoutEndDateUtc] [datetime] NULL,
	[LockoutEnabled] [bit] NOT NULL,
	[AccessFailedCount] [int] NOT NULL,
	[UserName] [nvarchar](256) NOT NULL,
	[FirstName] [nvarchar](256) NULL,
	[LastName] [nvarchar](256) NULL,
	[Department] [nvarchar](256) NULL,
	[Discriminator] [nvarchar](128) NULL,
	[Isactive] [int] NOT NULL,
 CONSTRAINT [PK_dbo.AspNetUsers] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Emails]    Script Date: 11/10/2017 4:33:16 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Emails](
	[key] [int] IDENTITY(1,1) NOT NULL,
	[id] [nvarchar](150) NULL,
	[firstname] [nvarchar](50) NULL,
	[lastname] [nvarchar](50) NULL,
	[emailaddress] [nvarchar](150) NOT NULL,
	[address] [nvarchar](150) NOT NULL,
	[integration] [bit] NOT NULL,
	[production] [bit] NOT NULL,
	[engineering] [bit] NOT NULL,
	[isactive] [bit] NOT NULL,
 CONSTRAINT [PK_Emails] PRIMARY KEY CLUSTERED 
(
	[key] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Identity]    Script Date: 11/10/2017 4:33:16 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Identity](
	[key] [int] NOT NULL,
	[username] [nvarchar](250) NULL,
	[emailaddress] [nvarchar](250) NULL,
	[firstname] [nvarchar](250) NULL,
	[lastname] [nvarchar](250) NULL,
	[password] [binary](64) NULL,
	[guid] [uniqueidentifier] NULL,
	[role] [int] NULL
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[ProductRange]    Script Date: 11/10/2017 4:33:16 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ProductRange](
	[key] [int] IDENTITY(1,1) NOT NULL,
	[ProductRange] [nvarchar](50) NULL,
	[isactive] [bit] NOT NULL,
 CONSTRAINT [PK_ProductRange] PRIMARY KEY CLUSTERED 
(
	[key] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Results]    Script Date: 11/10/2017 4:33:16 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Results](
	[key] [int] IDENTITY(0,1) NOT NULL,
	[Results] [nvarchar](50) NOT NULL,
	[isactive] [bit] NOT NULL,
 CONSTRAINT [PK_Results] PRIMARY KEY CLUSTERED 
(
	[key] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[TrilogyTestData]    Script Date: 11/10/2017 4:33:16 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TrilogyTestData](
	[QD809 Serial] [int] IDENTITY(1000,1) NOT NULL,
	[Product Range] [int] NULL,
	[Part Number] [nvarchar](150) NULL,
	[Description] [nvarchar](max) NULL,
	[Software Release] [bit] NULL,
	[Firmware Release] [bit] NULL,
	[Hardware Release] [bit] NULL,
	[Date from engineering] [datetime] NULL,
	[Engineering Test] [nvarchar](150) NULL,
	[Smoke Test] [nvarchar](150) NULL,
	[Customer Beta Test] [nvarchar](150) NULL,
	[Targetted Regression Test] [nvarchar](150) NULL,
	[Scenario Test] [nvarchar](150) NULL,
	[Full Regression Test] [nvarchar](150) NULL,
	[Integration Test Result] [nvarchar](150) NULL,
	[Integration Test Name] [nvarchar](150) NULL,
	[Date from Integration Test] [datetime] NULL,
	[Notes] [nvarchar](max) NULL,
	[Production Test Signoff Date] [datetime] NULL,
	[Production Test Date Recevied] [datetime] NULL,
	[Production Test Result] [nvarchar](150) NULL,
	[Production Test Name] [nvarchar](150) NULL,
	[Production PCR] [int] NULL,
	[Released] [bit] NULL,
	[Revision] [nvarchar](50) NULL,
	[Areas To Test] [nvarchar](max) NULL,
	[Engineering Test Name] [nvarchar](150) NULL,
	[Date Integration Test] [datetime] NULL,
	[Production Test Signoff] [int] NULL,
	[isactive] [bit] NOT NULL,
	[Previous Integration Test Name] [nvarchar](150) NULL,
	[Previous Production Test Name] [nvarchar](150) NULL,
	[Previous Engineering Test Name] [nvarchar](150) NULL,
 CONSTRAINT [PK_TrilogyTestData] PRIMARY KEY CLUSTERED 
(
	[QD809 Serial] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
ALTER TABLE [dbo].[AspNetUsers] ADD  CONSTRAINT [DF_AspNetUsers_isactive]  DEFAULT ((1)) FOR [Isactive]
GO
ALTER TABLE [dbo].[Emails] ADD  CONSTRAINT [DF_Emails_integration]  DEFAULT ((0)) FOR [integration]
GO
ALTER TABLE [dbo].[Emails] ADD  CONSTRAINT [DF_Emails_production]  DEFAULT ((0)) FOR [production]
GO
ALTER TABLE [dbo].[Emails] ADD  CONSTRAINT [DF_Emails_engineering]  DEFAULT ((0)) FOR [engineering]
GO
ALTER TABLE [dbo].[Emails] ADD  CONSTRAINT [DF_Emails_isactive]  DEFAULT ((1)) FOR [isactive]
GO
ALTER TABLE [dbo].[ProductRange] ADD  CONSTRAINT [DF_ProductRange_isactive]  DEFAULT ((1)) FOR [isactive]
GO
ALTER TABLE [dbo].[Results] ADD  CONSTRAINT [DF_Results_isactive]  DEFAULT ((1)) FOR [isactive]
GO
ALTER TABLE [dbo].[TrilogyTestData] ADD  CONSTRAINT [DF_TrilogyTestData_Date from engineering]  DEFAULT (getdate()) FOR [Date from engineering]
GO
ALTER TABLE [dbo].[TrilogyTestData] ADD  CONSTRAINT [DF_TrilogyTestData_Integration Test Result]  DEFAULT ((0)) FOR [Integration Test Result]
GO
ALTER TABLE [dbo].[TrilogyTestData] ADD  CONSTRAINT [DF_TrilogyTestData_Production Test Result]  DEFAULT ((0)) FOR [Production Test Result]
GO
ALTER TABLE [dbo].[TrilogyTestData] ADD  CONSTRAINT [DF_TrilogyTestData_isactive]  DEFAULT ((1)) FOR [isactive]
GO
ALTER TABLE [dbo].[AspNetUserClaims]  WITH CHECK ADD  CONSTRAINT [FK_dbo.AspNetUserClaims_dbo.AspNetUsers_UserId] FOREIGN KEY([UserId])
REFERENCES [dbo].[AspNetUsers] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[AspNetUserClaims] CHECK CONSTRAINT [FK_dbo.AspNetUserClaims_dbo.AspNetUsers_UserId]
GO
ALTER TABLE [dbo].[AspNetUserLogins]  WITH CHECK ADD  CONSTRAINT [FK_dbo.AspNetUserLogins_dbo.AspNetUsers_UserId] FOREIGN KEY([UserId])
REFERENCES [dbo].[AspNetUsers] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[AspNetUserLogins] CHECK CONSTRAINT [FK_dbo.AspNetUserLogins_dbo.AspNetUsers_UserId]
GO
ALTER TABLE [dbo].[AspNetUserRoles]  WITH CHECK ADD  CONSTRAINT [FK_dbo.AspNetUserRoles_dbo.AspNetRoles_RoleId] FOREIGN KEY([RoleId])
REFERENCES [dbo].[AspNetRoles] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[AspNetUserRoles] CHECK CONSTRAINT [FK_dbo.AspNetUserRoles_dbo.AspNetRoles_RoleId]
GO
ALTER TABLE [dbo].[AspNetUserRoles]  WITH CHECK ADD  CONSTRAINT [FK_dbo.AspNetUserRoles_dbo.AspNetUsers_UserId] FOREIGN KEY([UserId])
REFERENCES [dbo].[AspNetUsers] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[AspNetUserRoles] CHECK CONSTRAINT [FK_dbo.AspNetUserRoles_dbo.AspNetUsers_UserId]
GO
/****** Object:  StoredProcedure [dbo].[custom_select]    Script Date: 11/10/2017 4:33:16 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[custom_select](
	@pageNumber INT = NULL,
	@itemsPerPage INT = NULL,
	@count INT OUTPUT,
	@qd809serial INT = NULL, 
	@productrange NVARCHAR(150) = NULL, 
	@partnumber NVARCHAR(150) = NULL, 
	@description NVARCHAR(MAX) = NULL, 
	@softwarerelease INT = NULL, 
	@firmwarerelease INT = NULL, 
	@hardwarerelease INT = NULL, 
	@datefromengineering NVARCHAR(15) = NULL, --date
	@engineeringtest NVARCHAR(150) = NULL, 
	@smoketest NVARCHAR(150) = NULL, 
	@customerbetatest NVARCHAR(150) = NULL, 
	@targettedregressiontest NVARCHAR(150) = NULL, 
	@scenariotest NVARCHAR(150) = NULL, 
	@fullregressiontest NVARCHAR(150) = NULL, 
	@integrationtestresult NVARCHAR(150) = NULL, 
	@integrationtestname NVARCHAR(150) = NULL, 
	@datefromintegrationtest NVARCHAR(15) = NULL,  --date
	@notes NVARCHAR(MAX) = NULL, 
	@productiontestsignoff NVARCHAR(150) = NULL, 
	@productiontestsignoffdate NVARCHAR(15) = NULL,  --date
	@productiontestdaterecevied NVARCHAR(15) = NULL,  --date
	@productiontestresult NVARCHAR(150) = NULL, 
	@productiontestname NVARCHAR(150) = NULL, 
	@engineeringtestname NVARCHAR(150) = NULL, 
	@productionpcr INT = NULL, 
	@released INT = NULL, 
	@revision NVARCHAR(150) = NULL, 
	@areastotest NVARCHAR(MAX) = NULL,
	@dateintegrationtest NVARCHAR(15) = NULL, --date
	@datefromengineering_from  NVARCHAR(15) = NULL, --date
    @datefromengineering_to  NVARCHAR(15) = NULL, --date
    @datefromintegrationtest_from  NVARCHAR(15) = NULL, --date
    @datefromintegrationtest_to  NVARCHAR(15) = NULL, --date
    @productiontestsignoffdate_from  NVARCHAR(15) = NULL, --date
    @productiontestsignoffdate_to  NVARCHAR(15) = NULL, --date
	@custom NVARCHAR(50) = NULL,
	@sortcolumn NVARCHAR(50) = NULL,
	@sortorder NVARCHAR(5) = NULL,
	@isactive INT = NULL
) AS

DECLARE
	@rowCount   NVARCHAR(100),
	@pageFetch  NVARCHAR(300),
	@cnt INT,
	@recordSet INT,
	@currentDate DateTime,
	@culture nvarchar(10)

SET @culture = 'en-GB'
SET @currentDate = GETDATE()

IF @pageNumber IS NOT NULL AND  @itemsPerPage IS NOT NULL
	BEGIN
		SET @recordSet = (@pageNumber * @itemsPerPage) - @itemsPerPage
		SET @pageFetch = ' OFFSET ' + CAST(@recordSet as NVARCHAR(5)) + ' ROWS FETCH NEXT ' + CAST(@itemsPerPage as NVARCHAR(5))  + ' ROWS ONLY'
	END

SELECT @cnt = count(*) 
FROM [TrilogyTestData]
WHERE isactive = 1
 AND (@custom  != 'qd809serial'  OR ([QD809 Serial] = @qd809serial))
 AND (@custom != 'search' OR (@datefromengineering_from IS NULL OR  [Date from engineering] >= convert(DATETIME, @datefromengineering_from, 103))
	AND (@datefromengineering_to IS NULL OR [Date from engineering] <= convert(DATETIME, @datefromengineering_to, 103))
	AND (@datefromintegrationtest_from IS NULL OR  [Date from Integration Test] >= convert(DATETIME, @datefromintegrationtest_from, 103))
	AND (@datefromintegrationtest_to IS NULL OR [Date from Integration Test] <= convert(DATETIME, @datefromintegrationtest_to, 103))
	AND (@productiontestsignoffdate_from IS NULL OR  [Production Test Signoff Date] >= convert(DATETIME, @productiontestsignoffdate_from, 103))
	AND (@productiontestsignoffdate_to IS NULL OR [Production Test Signoff Date] <=convert(DATETIME, @productiontestsignoffdate_to, 103) )
	AND (@integrationtestname IS NULL OR [Integration Test Name] = @integrationtestname )
	AND (@productiontestname IS NULL OR [Production Test Name] = @productiontestname )
	AND (@integrationtestresult IS NULL OR [Integration Test Result] = @integrationtestresult )
	AND (@productiontestresult IS NULL OR [Production Test Result] = @productiontestresult )
	AND (@productionpcr IS NULL OR CAST([Production PCR] AS VARCHAR(20)) LIKE '%' + CAST(@productionpcr AS VARCHAR(20)) + '%' )
	AND (@partnumber IS NULL OR [Part Number] LIKE '%' + @partnumber + '%' )
	)

SET @count = @cnt
SELECT 
	--@cnt as count
	 [QD809 Serial] AS qd809serial
	,[Product Range] AS productrange
	,[Part Number] AS partnumber
	,[Description] AS description
	,[Software Release] AS softwarerelease
	,[Firmware Release] AS firmwarerelease
	,[Hardware Release] AS hardwarerelease
	,FORMAT([Date from engineering],'d', @culture) AS datefromengineering	
	,[Engineering Test] AS engineeringtest
	,[Smoke Test] AS smoketest
	,[Customer Beta Test] AS customerbetatest
	,[Targetted Regression Test] AS targettedregressiontest
	,[Scenario Test] AS scenariotest
	,[Full Regression Test] AS fullregressiontest
	,[Integration Test Result] AS integrationtestresult
	,[Integration Test Name] AS integrationtestname
	,FORMAT([Date from Integration Test],'d', @culture) AS datefromintegrationtest	
	,[Notes] AS notes
	,[Production Test Signoff]  AS productiontestsignoff	
	,FORMAT([Production Test Signoff Date],'d', @culture) AS productiontestsignoffdate	
	,FORMAT([Production Test Date Recevied],'d', @culture) AS productiontestdaterecevied	
	,[Production Test Result] AS productiontestresult
	,[Production Test Name] AS productiontestname
	,[Production PCR] AS productionpcr
	,[Released] AS released
	,[Revision] AS revision
	,[Areas To Test] AS areastotest	
	,[Engineering Test Name] AS engineeringtestname	
	,FORMAT([Date Integration Test],'d', @culture) AS dateintegrationtest	
	,qd809.isactive
	,[Previous Integration Test Name] AS previousintegrationtestname
	,[Previous Production Test Name] AS previousproductiontestname
	,[Previous Engineering Test Name] AS previousengineeringtestname
FROM [dbo].[TrilogyTestData] qd809
LEFT JOIN Results sortIntegrationresult  ON sortIntegrationresult.[key] = qd809.[Integration Test Result]
LEFT JOIN Results sortProductionresult  ON sortProductionresult.[key] = qd809.[Production Test Result]
LEFT JOIN ProductRange sortProductionrange  ON sortProductionrange.[key] = qd809.[Product Range]
LEFT JOIN AspNetUsers sortUsers ON sortUsers.Id = qd809.[Integration Test Name]
LEFT JOIN AspNetUsers sortProductionUsers ON sortProductionUsers.Id = qd809.[Production Test Name]
WHERE qd809.isactive = 1
  AND (@custom  != 'qd809serial'  OR ([QD809 Serial] = @qd809serial))
AND (@custom != 'search' OR (@datefromengineering_from IS NULL OR  [Date from engineering] >= convert(DATETIME, @datefromengineering_from, 103))
	AND (@datefromengineering_to IS NULL OR [Date from engineering] <= convert(DATETIME, @datefromengineering_to, 103))
	AND (@datefromintegrationtest_from IS NULL OR  [Date from Integration Test] >= convert(DATETIME, @datefromintegrationtest_from, 103))
	AND (@datefromintegrationtest_to IS NULL OR [Date from Integration Test] <= convert(DATETIME, @datefromintegrationtest_to, 103))
	AND (@productiontestsignoffdate_from IS NULL OR  [Production Test Signoff Date] >= convert(DATETIME, @productiontestsignoffdate_from, 103))
	AND (@productiontestsignoffdate_to IS NULL OR [Production Test Signoff Date] <=convert(DATETIME, @productiontestsignoffdate_to, 103) )
	AND (@integrationtestname IS NULL OR [Integration Test Name] = @integrationtestname )
	AND (@productiontestname IS NULL OR [Production Test Name] = @productiontestname )
	AND (@integrationtestresult IS NULL OR [Integration Test Result] = @integrationtestresult )
	AND (@productiontestresult IS NULL OR [Production Test Result] = @productiontestresult )
	AND (@productionpcr IS NULL OR CAST([Production PCR] AS VARCHAR(20)) LIKE '%' + CAST(@productionpcr AS VARCHAR(20)) + '%' )
	AND (@partnumber IS NULL OR [Part Number] LIKE '%' + @partnumber + '%' )
	)
ORDER BY 
		 (case when @sortcolumn = '[QD809 Serial]' and @sortorder = 'asc' then [QD809 Serial] end)  asc,
         (case when @sortcolumn = '[QD809 Serial]' and @sortorder = 'desc' then [QD809 Serial] end) desc,
         (case when @sortcolumn = '[Product Range]' and @sortorder = 'asc' then sortProductionrange.ProductRange end) asc,
         (case when @sortcolumn = '[Product Range]' and @sortorder = 'desc' then sortProductionrange.ProductRange end) desc,
         (case when @sortcolumn = '[Part Number]' and @sortorder = 'asc' then LOWER([Part Number]) end) asc,
         (case when @sortcolumn = '[Part Number]' and @sortorder = 'desc' then LOWER([Part Number]) end) desc,
         (case when @sortcolumn = '[Date from engineering]' and @sortorder = 'asc' then [Date from engineering] end) asc,
         (case when @sortcolumn = '[Date from engineering]' and @sortorder = 'desc' then [Date from engineering] end) desc,
         (case when @sortcolumn = '[Integration Test Result]' and @sortorder = 'asc' then sortIntegrationresult.Results end) asc,
         (case when @sortcolumn = '[Integration Test Result]' and @sortorder = 'desc' then sortIntegrationresult.Results end) desc,
         (case when @sortcolumn = '[Production Test Result]' and @sortorder = 'asc' then sortProductionresult.Results end) asc,
         (case when @sortcolumn = '[Production Test Result]' and @sortorder = 'desc' then sortProductionresult.Results end) desc,
         (case when @sortcolumn = '[Production Test Signoff Date]' and @sortorder = 'asc' then [Production Test Signoff Date] end) asc,
         (case when @sortcolumn = '[Production Test Signoff Date]' and @sortorder = 'desc' then [Production Test Signoff Date] end) desc,
         (case when @sortcolumn = '[Date from Integration Test]' and @sortorder = 'asc' then [Date from Integration Test] end) asc,
         (case when @sortcolumn = '[Date from Integration Test]' and @sortorder = 'desc' then [Date from Integration Test] end) desc,
         (case when @sortcolumn = '[Production Test Date Recevied]' and @sortorder = 'asc' then [Production Test Date Recevied] end) asc,
         (case when @sortcolumn = '[Production Test Date Recevied]' and @sortorder = 'desc' then [Production Test Date Recevied] end) desc,
         (case when @sortcolumn = '[Production PCR]' and @sortorder = 'asc' then LOWER([Production PCR]) end) asc,
         (case when @sortcolumn = '[Production PCR]' and @sortorder = 'desc' then LOWER([Production PCR]) end) desc,
         (case when @sortcolumn = '[Integration Test Name]' and @sortorder = 'asc' then LOWER(sortUsers.FirstName) end) asc,
         (case when @sortcolumn = '[Integration Test Name]' and @sortorder = 'desc' then LOWER(sortUsers.FirstName) end) desc,
         (case when @sortcolumn = '[Production Test Name]' and @sortorder = 'asc' then LOWER(sortProductionUsers.FirstName) end) asc,
         (case when @sortcolumn = '[Production Test Name]' and @sortorder = 'desc' then LOWER(sortProductionUsers.FirstName) end) desc

OFFSET @recordSet ROWS FETCH NEXT @itemsPerPage ROWS ONLY

GO
/****** Object:  StoredProcedure [dbo].[dropdown_email_changes]    Script Date: 11/10/2017 4:33:16 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[dropdown_email_changes]
	@fromid NVARCHAR(150) = NULL, 
	@integration NVARCHAR(150) = NULL, 
	@production NVARCHAR(150) = NULL, 
	@engineering NVARCHAR(150) = NULL
AS

DECLARE @emails_transfered AS TABLE(dropdown NVARCHAR(150), id NVARCHAR(150), qd809serial INT, productrange NVARCHAR(150), firstname NVARCHAR(150), lastname NVARCHAR(150), email NVARCHAR(150))

SET NOCOUNT ON

INSERT INTO  @emails_transfered
SELECT 'integration' AS dropdown, [Integration Test Name] AS id, [QD809 Serial] AS qd809serial, pr.ProductRange AS productrange, em.firstname, em.lastname, em.emailaddress 
FROM [dbo].[TrilogyTestData] j
INNER JOIN ProductRange pr ON pr.[key] = j.[Product Range]
INNER JOIN emails em ON em.id = @integration
WHERE [Integration Test Name] = @fromid

INSERT INTO  @emails_transfered
SELECT 'engineering' AS dropdown, [Engineering Test Name] AS id, [QD809 Serial] AS qd809serial, pr.ProductRange AS productrange , em.firstname, em.lastname , em.emailaddress 
FROM [dbo].[TrilogyTestData] j
INNER JOIN ProductRange pr ON pr.[key] = j.[Product Range]
INNER JOIN emails em ON em.id = @engineering
WHERE [Engineering Test Name] = @fromid

INSERT INTO  @emails_transfered
SELECT 'production' AS dropdown, [Production Test Name] AS id, [QD809 Serial] AS qd809serial, pr.ProductRange AS productrange, em.firstname, em.lastname , em.emailaddress 
FROM [dbo].[TrilogyTestData] j
INNER JOIN ProductRange pr ON pr.[key] = j.[Product Range]
INNER JOIN emails em ON em.id = @production
WHERE [Production Test Name] = @fromid

SELECT * from @emails_transfered
GO
/****** Object:  StoredProcedure [dbo].[get_dropdowns]    Script Date: 11/10/2017 4:33:16 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[get_dropdowns]
AS
	SELECT
		 users.id AS [key]
		,users.[firstname] AS firstname
		,users.[lastname] AS lastname
		,users.[firstname] + ' ' + users.[lastname] + '<' + users.[email] + '>' AS emailaddress
		,users.[email]  AS address
		,[integration] AS integration
		,[production] AS production
		,[engineering] AS engineering
		,CASE WHEN users.isactive = 1 THEN 'True' ELSE 'False' END  AS isactive
	FROM AspNetUsers users
	INNER JOIN Emails role ON role.id = users.Id
	ORDER BY [firstname]

	SELECT
		 users.id AS [key]
		,users.[firstname] AS firstname
		,users.[lastname] AS lastname
		,users.[firstname] + ' ' + users.[lastname] + '<' + users.[email] + '>' AS emailaddress
		,users.[email]  AS address
		,[integration] AS integration
		,[production] AS production
		,[engineering] AS engineering
		,CASE WHEN users.isactive = 1 THEN 'True' ELSE 'False' END  AS isactive
	FROM AspNetUsers users
	INNER JOIN Emails role ON role.id = users.Id and role.production = 1
	ORDER BY [firstname]

	SELECT
		 users.id AS [key]
		,users.[firstname] AS firstname
		,users.[lastname] AS lastname
		,users.[firstname] + ' ' + users.[lastname] + '<' + users.[email] + '>' AS emailaddress
		,users.[email]  AS address
		,[integration] AS integration
		,[production] AS production
		,[engineering] AS engineering
		,CASE WHEN users.isactive = 1 THEN 'True' ELSE 'False' END  AS isactive
	FROM AspNetUsers users
	INNER JOIN Emails role ON role.id = users.Id and role.integration = 1
	ORDER BY [firstname]

	SELECT
		 users.id AS [key]
		,users.[firstname] AS firstname
		,users.[lastname] AS lastname
		,users.[firstname] + ' ' + users.[lastname] + '<' + users.[email] + '>' AS emailaddress
		,users.[email]  AS address
		,[integration] AS integration
		,[production] AS production
		,[engineering] AS engineering
		,CASE WHEN users.isactive = 1 THEN 'True' ELSE 'False' END  AS isactive
	FROM AspNetUsers users
	INNER JOIN Emails role ON role.id = users.Id and role.engineering = 1
	ORDER BY [firstname]

	SELECT [key]
		  ,[productrange] AS product_range
		  ,CASE WHEN isactive = 1 THEN 'True' ELSE 'False' END  AS isactive
	 FROM [dbo].[ProductRange]
	 --WHERE isactive = 1
	 ORDER BY [productrange] ASC

	 SELECT [key]
		  ,[results] AS status
		  ,CASE WHEN isactive = 1 THEN 'True' ELSE 'False' END  AS isactive
	 FROM [dbo].[results]
	 --WHERE isactive = 1
	 ORDER BY [results] ASC


GO
/****** Object:  StoredProcedure [dbo].[insert_user]    Script Date: 11/10/2017 4:33:16 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[insert_user]
    @username NVARCHAR(50), 
    @password NVARCHAR(50),
    @firstname NVARCHAR(40) = NULL, 
    @lastname NVARCHAR(40) = NULL,
    @role int = 0,
	@emailaddress NVARCHAR(40) = NULL,
    @responseMessage NVARCHAR(250) OUTPUT
AS
BEGIN
    SET NOCOUNT ON

    DECLARE @guid UNIQUEIDENTIFIER = NEWID()
    BEGIN TRY

        INSERT INTO dbo.[Identity] (username, password, guid, firstname, lastname, emailaddress, role)
        VALUES(@username, HASHBYTES('SHA2_512', @password+CAST(@guid AS NVARCHAR(36))), @guid, @firstname, @lastname, @emailaddress, @role)

       SET @responseMessage='Success'

    END TRY
    BEGIN CATCH
        SET @responseMessage=ERROR_MESSAGE() 
    END CATCH

END

GO
/****** Object:  StoredProcedure [dbo].[Login]    Script Date: 11/10/2017 4:33:16 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[Login]
	@username NVARCHAR(250),
	@password NVARCHAR(50)--,
AS
	SELECT 	username, firstname, lastname, role, emailaddress
	FROM [dbo].[Identity] WHERE  username = @username AND [password]=HASHBYTES('SHA2_512', @password+CAST([guid] AS NVARCHAR(36)))

GO
/****** Object:  StoredProcedure [dbo].[update_dropdown]    Script Date: 11/10/2017 4:33:16 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[update_dropdown]
	@fromid NVARCHAR(150) = NULL, 
	@integration NVARCHAR(150) = NULL, 
	@production NVARCHAR(150) = NULL, 
	@engineering NVARCHAR(150) = NULL
AS

SET NOCOUNT ON

IF(@integration IS NOT NULL)
	BEGIN
		UPDATE [dbo].[TrilogyTestData]
		SET [Integration Test Name] = @integration
		WHERE [Integration Test Name] = @fromid
	END

IF(@engineering IS NOT NULL)
	BEGIN
		UPDATE [dbo].[TrilogyTestData]
		SET [Engineering Test Name] = @engineering
		WHERE [Engineering Test Name] = @fromid
	END

IF(@production IS NOT NULL)
	BEGIN
		UPDATE [dbo].[TrilogyTestData]
		SET [Production Test Name] = @production
		WHERE [Production Test Name] = @fromid
	END
GO
/****** Object:  StoredProcedure [dbo].[update_email]    Script Date: 11/10/2017 4:33:16 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[update_email]
	@key NVARCHAR(150) = NULL, 
	@type NVARCHAR(150) = NULL, 
	@firstname NVARCHAR(150) = NULL, 
	@lastname NVARCHAR(150) = NULL, 
	@emailaddress NVARCHAR(MAX) = NULL, 
	@integration INT = NULL, 
	@production INT = NULL, 
	@engineering INT = NULL,  
	@isactive  INT = NULL,
	@id NVARCHAR(150) = NULL
AS

SET NOCOUNT ON
DECLARE @culture nvarchar(10)
SET @culture = 'en-US'

IF @type = 'delete'
	BEGIN
		UPDATE [dbo].TrilogyTestData
		SET 
			[Previous Integration Test Name] = [Integration Test Name]
			,[Integration Test Name] = NULL
		WHERE [Integration Test Name] = @id
		UPDATE [dbo].TrilogyTestData
		SET 
			[Previous Engineering Test Name] = [Engineering Test Name]
			,[Engineering Test Name] = NULL
		WHERE [Engineering Test Name] =  @id
		UPDATE [dbo].TrilogyTestData
		SET 
				[Previous Production Test Name] = [Production Test Name]
			,[Production Test Name] = NULL
		WHERE [Production Test Name] = @id

		DELETE FROM [Emails] WHERE id = @id
	END

IF @type = 'insert'
	BEGIN
		INSERT INTO [dbo].[Emails]
		(
		   [firstname]
		  ,[lastname]
		  ,[emailaddress]
		  ,[address]
		  ,[integration]
		  ,[production]
		  ,[engineering]
		  ,[isactive]
		  ,[id]
		)
		VALUES
		(
			 @firstname
			,@lastname 
			,@firstname + ' ' + @lastname +'<' + @emailaddress + '>'
			,@emailaddress
			,ISNULL(@integration, 0)
			,ISNULL(@production, 0)
			,ISNULL(@engineering, 0)
			,ISNULL(@isactive, 1)
			,@id
		)
	END
ELSE
	BEGIN
		UPDATE [dbo].[Emails]
		SET
			 [firstname] = ISNULL(@firstname, [firstname])
			,[lastname] = ISNULL(@lastname, [lastname])
			,[address] = ISNULL(@emailaddress, [address])
			,[emailaddress] = CASE WHEN @emailaddress IS NULL AND (@firstname IS NOT NULL OR @lastname IS NOT NULL) THEN
				ISNULL(@firstname, [firstname]) + ' ' + ISNULL(@lastname, [lastname]) + '<' + [address] + '>'
			ELSE
				CASE WHEN @emailaddress IS NULL THEN 
					[emailaddress]
				ELSE
					ISNULL(@firstname, [firstname]) + ' ' + ISNULL(@lastname, [lastname]) +'<' + @emailaddress + '>'
				END
			END
			,[integration] = ISNULL(@integration, [integration])
			,[production] = ISNULL(@production, [production])
			,[engineering] = ISNULL(@engineering, [engineering])
			,[isactive] = ISNULL(@isactive, [isactive])
		WHERE [id] = @key
		--SET @id =  @key

		if(@integration = '0' AND (@isactive != '0'OR @isactive IS NULL))
			BEGIN
				UPDATE [dbo].TrilogyTestData
				SET 
					[Previous Integration Test Name] = [Integration Test Name]
					,[Integration Test Name] = NULL
				WHERE [Integration Test Name] = @key
			END
		if(@engineering = '0' AND (@isactive != '0'OR @isactive IS NULL))
			BEGIN
				UPDATE [dbo].TrilogyTestData
				SET 
					[Previous Engineering Test Name] = [Engineering Test Name]
					,[Engineering Test Name] = NULL
				WHERE [Engineering Test Name] = @key
			END
		if(@production = '0' AND(@isactive != '0'OR @isactive IS NULL))
			BEGIN
				UPDATE [dbo].TrilogyTestData
				SET 
					[Previous Production Test Name] = [Production Test Name]
					,[Production Test Name] = NULL
				WHERE [Production Test Name] = @key
			END

		if(@isactive = '0' AND (@integration != '0'OR @integration IS NULL))
			BEGIN
				UPDATE [dbo].TrilogyTestData
				SET 
					 [Previous Integration Test Name] = [Integration Test Name]
					,[Integration Test Name] = NULL
				WHERE [Integration Test Name] = @key
			END

		if(@isactive = '0' AND (@engineering != '0'OR @engineering IS NULL))
			BEGIN
				UPDATE [dbo].TrilogyTestData
				SET 
					 [Previous Engineering Test Name] = [Engineering Test Name]
					,[Engineering Test Name] = NULL

				WHERE [Engineering Test Name] = @key
			END

		if(@isactive = '0' AND (@production != '0'OR @production IS NULL))
			BEGIN
				UPDATE [dbo].TrilogyTestData
				SET 
					 [Previous Production Test Name] = [Production Test Name]
					,[Production Test Name] = NULL
				WHERE [Production Test Name] = @key
			END
	END

GO
/****** Object:  StoredProcedure [dbo].[update_productrange]    Script Date: 11/10/2017 4:33:16 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[update_productrange]
	@updatetype NVARCHAR(150) = NULL, 
	@productrange NVARCHAR(150) = NULL, 
	@key INT = NULL, 
	@isactive INT = NULL,
	@id INT OUTPUT,
	@count INT OUTPUT
AS

SET NOCOUNT ON
DECLARE @culture nvarchar(10)
SET @culture = 'en-US'
SET @id = @key
SET @count = 0

IF @updatetype = 'insert'
	BEGIN
		INSERT INTO [dbo].[ProductRange]
		(
			 [ProductRange]
			,[isactive]
		)
		VALUES
		(
			 @productrange
			,@isactive
		)
		SET @id = @@IDENTITY
	END
IF @updatetype = 'update'
	BEGIN
		UPDATE [dbo].[ProductRange]
		SET
			  [ProductRange] = ISNULL(@productrange,[ProductRange])
			 ,[isactive] = ISNULL(@isactive,[isactive])
		WHERE [key] = @key
	END
IF @updatetype = 'delete'
	BEGIN
		SELECT @count = count(*) FROM [dbo].TrilogyTestData WHERE  [Product Range] = @key
		IF @count = 0
		BEGIN
			DELETE FROM [dbo].[ProductRange]
			WHERE [key] = @key
		END
	END
GO
/****** Object:  StoredProcedure [dbo].[update_qd809]    Script Date: 11/10/2017 4:33:16 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[update_qd809]
	@updatetype NVARCHAR(150) = NULL, 
	@qd809serial INT = NULL, 
	@productrange NVARCHAR(150) = NULL, 
	@partnumber NVARCHAR(150) = NULL, 
	@description NVARCHAR(MAX) = NULL, 
	@softwarerelease INT = NULL, 
	@firmwarerelease INT = NULL, 
	@hardwarerelease INT = NULL, 
	@datefromengineering NVARCHAR(15) = NULL, 
	@engineeringtest NVARCHAR(150) = NULL, 
	@smoketest NVARCHAR(150) = NULL, 
	@customerbetatest NVARCHAR(150) = NULL, 
	@targettedregressiontest NVARCHAR(150) = NULL, 
	@scenariotest NVARCHAR(150) = NULL, 
	@fullregressiontest NVARCHAR(150) = NULL, 
	@integrationtestresult NVARCHAR(150) = NULL, 
	@integrationtestname NVARCHAR(150) = NULL, 
	@datefromintegrationtest NVARCHAR(10) = NULL, 
	@notes NVARCHAR(MAX) = NULL, 
	@productiontestsignoff NVARCHAR(150) = NULL, 
	@productiontestsignoffdate NVARCHAR(15) = NULL, 
	@productiontestdaterecevied NVARCHAR(15) = NULL, 
	@productiontestresult NVARCHAR(150) = NULL, 
	@productiontestname NVARCHAR(150) = NULL, 
	@productionpcr NVARCHAR(150) = NULL, 
	@released INT = NULL, 
	@revision NVARCHAR(150) = NULL, 
	@areastotest NVARCHAR(MAX) = NULL,
	@dateintegrationtest NVARCHAR(15) = NULL,
	@engineeringtestname NVARCHAR(150) = NULL,
	@isactive INT = NULL,
	@serialnumber INT OUTPUT
AS

SET NOCOUNT ON
DECLARE @culture nvarchar(10)
SET @culture = 'en-US'

IF @updatetype = 'insert'
	BEGIN
		INSERT INTO [dbo].[TrilogyTestData]
		(
			 [Product Range]
			,[Part Number]
			,[Description]
			,[Software Release]
			,[Firmware Release]
			,[Hardware Release]
			,[Date from engineering]
			,[Engineering Test]
			,[Smoke Test]
			,[Customer Beta Test]
			,[Targetted Regression Test]
			,[Scenario Test]
			,[Full Regression Test]
			,[Integration Test Result]
			,[Integration Test Name]
			,[Date from Integration Test]
			,[Notes]
			,[Production Test Signoff]
			,[Production Test Signoff Date]
			,[Production Test Date Recevied]
			,[Production Test Result]
			,[Production Test Name]
			,[Production PCR]
			,[Released]
			,[Revision]
			,[Areas To Test]
			,[Date Integration Test]
			,[Engineering Test Name]
		)
		VALUES
		(
			 @productrange
			,@partnumber
			,@description
			,@softwarerelease
			,@firmwarerelease
			,@hardwarerelease
			,convert(datetime, @datefromengineering, 103)
			,@engineeringtest
			,@smoketest
			,@customerbetatest
			,@targettedregressiontest
			,@scenariotest
			,@fullregressiontest
			,@integrationtestresult
			,@integrationtestname
			,convert(datetime, @datefromintegrationtest, 103) 
			,@notes
			,@productiontestsignoff			
			,convert(datetime, @productiontestsignoffdate, 103) 
			,convert(datetime, @productiontestdaterecevied, 103) 
			,@productiontestresult
			,@productiontestname
			,@productionpcr
			,@released
			,@revision
			,@areastotest
			,convert(datetime, @dateintegrationtest, 103) 
			,@engineeringtestname
		)
		SET @serialnumber = @@IDENTITY
	END
ELSE
	BEGIN
	UPDATE [dbo].[TrilogyTestData]
	SET
		 [Product Range] = CASE WHEN @productrange IS NULL THEN [Product Range] ELSE CASE WHEN @productrange = '' THEN NULL ELSE @productrange END END
		,[Part Number] = CASE WHEN @partnumber IS NULL THEN [Part Number] ELSE CASE WHEN @partnumber = '' THEN NULL ELSE @partnumber END END
		,[Description] = CASE WHEN @description IS NULL THEN [Description] ELSE CASE WHEN @description = '' THEN NULL ELSE @description END END
		,[Software Release] = CASE WHEN @softwarerelease IS NULL THEN [Software Release] ELSE CASE WHEN @softwarerelease = '' THEN NULL ELSE @softwarerelease END END
		,[Firmware Release] = CASE WHEN @firmwarerelease IS NULL THEN [Firmware Release] ELSE CASE WHEN @firmwarerelease = '' THEN NULL ELSE @firmwarerelease END END
		,[Hardware Release] = CASE WHEN @hardwarerelease IS NULL THEN [Hardware Release] ELSE CASE WHEN @hardwarerelease = '' THEN NULL ELSE @hardwarerelease END END
		,[Date from engineering] = CASE WHEN @datefromengineering IS NULL THEN [Date from engineering] ELSE CASE WHEN @datefromengineering = '' THEN NULL ELSE convert(DATETIME, @datefromengineering, 103) END END
		,[Engineering Test] = CASE WHEN @engineeringtest IS NULL THEN [Engineering Test] ELSE CASE WHEN @engineeringtest = '' THEN NULL ELSE @engineeringtest END END
		,[Smoke Test] = CASE WHEN @smoketest IS NULL THEN [Smoke Test] ELSE CASE WHEN @smoketest = '' THEN NULL ELSE @smoketest END END
		,[Customer Beta Test] = CASE WHEN @customerbetatest IS NULL THEN [Customer Beta Test] ELSE CASE WHEN @customerbetatest = '' THEN NULL ELSE @customerbetatest END END
		,[Targetted Regression Test] = CASE WHEN @targettedregressiontest IS NULL THEN [Targetted Regression Test] ELSE CASE WHEN @targettedregressiontest = '' THEN NULL ELSE @targettedregressiontest END END
		,[Scenario Test] = CASE WHEN @scenariotest IS NULL THEN [Scenario Test] ELSE CASE WHEN @scenariotest = '' THEN NULL ELSE @scenariotest END END
		,[Full Regression Test] = CASE WHEN @fullregressiontest IS NULL THEN [Full Regression Test] ELSE CASE WHEN @fullregressiontest = '' THEN NULL ELSE @fullregressiontest END END
		,[Integration Test Result] = CASE WHEN @integrationtestresult IS NULL THEN [Integration Test Result] ELSE CASE WHEN @integrationtestresult = '' THEN NULL ELSE @integrationtestresult END END
		,[Integration Test Name] = CASE WHEN @integrationtestname IS NULL THEN [Integration Test Name] ELSE CASE WHEN @integrationtestname = '' THEN NULL ELSE @integrationtestname END END
		,[Date from Integration Test] = CASE WHEN @datefromintegrationtest IS NULL THEN [Date from Integration Test] ELSE CASE WHEN @datefromintegrationtest = '' THEN NULL ELSE convert(DATETIME, @datefromintegrationtest, 103) END END
		,[Notes] = CASE WHEN @notes IS NULL THEN [Notes] ELSE CASE WHEN @notes = '' THEN NULL ELSE @notes END END
		,[Production Test Signoff] = CASE WHEN @productiontestsignoff IS NULL THEN [Production Test Signoff] ELSE CASE WHEN @productiontestsignoff = '' THEN NULL ELSE @productiontestsignoff END END
		,[Production Test Signoff Date] = CASE WHEN @productiontestsignoffdate IS NULL THEN [Production Test Signoff Date] ELSE CASE WHEN @productiontestsignoffdate = '' THEN NULL ELSE convert(DATETIME, @productiontestsignoffdate, 103) END END
		,[Production Test Date Recevied] = CASE WHEN @productiontestdaterecevied IS NULL THEN [Production Test Date Recevied] ELSE CASE WHEN @productiontestdaterecevied = '' THEN NULL ELSE convert(DATETIME, @productiontestdaterecevied, 103) END END
		,[Production Test Result] = CASE WHEN @productiontestresult IS NULL THEN [Production Test Result] ELSE CASE WHEN @productiontestresult = '' THEN NULL ELSE @productiontestresult END END
		,[Production Test Name] = CASE WHEN @productiontestname IS NULL THEN [Production Test Name] ELSE CASE WHEN @productiontestname = '' THEN NULL ELSE @productiontestname END END
		,[Production PCR] = CASE WHEN @productionpcr IS NULL THEN [Production PCR] ELSE CASE WHEN @productionpcr = '' THEN NULL ELSE @productionpcr END END
		,[Released] = CASE WHEN @released IS NULL THEN [Released] ELSE CASE WHEN @released = '' THEN NULL ELSE @released END END
		,[Revision] = CASE WHEN @revision IS NULL THEN [Revision] ELSE CASE WHEN @revision = '' THEN NULL ELSE @revision END END
		,[Areas To Test] = CASE WHEN @areastotest IS NULL THEN [Areas To Test] ELSE CASE WHEN @areastotest = '' THEN NULL ELSE @areastotest END END
		,[Date Integration Test] = CASE WHEN @dateintegrationtest IS NULL THEN [Date Integration Test] ELSE CASE WHEN @dateintegrationtest = '' THEN NULL ELSE convert(DATETIME, @dateintegrationtest, 103) END END
		,[Engineering Test Name] = CASE WHEN @engineeringtestname IS NULL THEN [Engineering Test Name] ELSE CASE WHEN @engineeringtestname = '' THEN NULL ELSE @engineeringtestname END END
		,[isactive] = isnull(@isactive,[isactive])
		,[Previous Integration Test Name] = CASE WHEN @integrationtestname IS NULL THEN [Previous Integration Test Name] ELSE NULL END
		,[Previous Production Test Name] =  CASE WHEN @productiontestname IS NULL THEN [Previous Production Test Name] ELSE NULL END
		,[Previous Engineering Test Name] = CASE WHEN @engineeringtestname IS NULL THEN [Previous Engineering Test Name] ELSE NULL END
	WHERE [QD809 Serial] = @qd809serial
		SET @serialnumber =  @qd809serial
	END

GO
/****** Object:  StoredProcedure [dbo].[update_results]    Script Date: 11/10/2017 4:33:16 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[update_results]
	@updatetype NVARCHAR(150) = NULL, 
	@results NVARCHAR(150) = NULL, 
	@key INT = NULL, 
	@isactive INT = NULL,
	@id INT OUTPUT,
	@count INT OUTPUT
AS

SET NOCOUNT ON
DECLARE @culture nvarchar(10)
SET @culture = 'en-US'
SET @id = @key
SET @count = 0

IF @updatetype = 'insert'
	BEGIN
		INSERT INTO [dbo].Results
		(
			 [Results]
			,[isactive]
		)
		VALUES
		(
			 @results
			,@isactive
		)
		SET @id = @@IDENTITY
	END
IF @updatetype = 'update'
	BEGIN
		UPDATE [dbo].Results
		SET
			  [Results] = ISNULL(@results,[Results])
			 ,[isactive] = ISNULL(@isactive,[isactive])
		WHERE [key] = @key
	END
IF @updatetype = 'delete'
	BEGIN
		SELECT @count = count(*) FROM [dbo].TrilogyTestData WHERE [Production Test Result] = @key OR [Integration Test Result]  = @key
		IF @count = 0
		BEGIN
			DELETE FROM [dbo].Results
			WHERE [key] = @key
		END
	END
GO


insert into AspNetRoles values('fcddb6f5-bb8a-4630-8e22-aa48486e4a85', 'Administrator')
insert into AspNetRoles values('cbcc207c-f480-4bd4-a55c-6d580857252b', 'Manager')
insert into AspNetRoles values('d04a941d-8864-41a5-ab5b-c5dfc3a6ce72', 'User')

insert into [ProductRange] ([ProductRange]) values('Mercury')
insert into [ProductRange] ([ProductRange]) values('Mentor RG')
insert into [ProductRange] ([ProductRange]) values('Gemini')
insert into [ProductRange] ([ProductRange]) values('MasterMind')
insert into [ProductRange] ([ProductRange]) values('16 Key HW Panel')
insert into [ProductRange] ([ProductRange]) values('8 Key Desktop Panel')
insert into [ProductRange] ([ProductRange]) values('30 Key Touch Panel')
insert into [ProductRange] ([ProductRange]) values('IP Panel')
insert into [ProductRange] ([ProductRange]) values('Desktop touch Panel')
insert into [ProductRange] ([ProductRange]) values('10 key 1U panel')
insert into [ProductRange] ([ProductRange]) values('32 Key Panel')
insert into [ProductRange] ([ProductRange]) values('Mentor XL')



insert into [Results] ([Results]) values('In Progress')
insert into [Results] ([Results]) values('Passed')
insert into [Results] ([Results]) values('Failed')


GRANT ALTER ON [dbo].Emails TO DB_WEB_USER
GRANT DELETE ON [dbo].Emails TO DB_WEB_USER
GRANT INSERT ON [dbo].Emails TO DB_WEB_USER
GRANT SELECT ON [dbo].Emails TO DB_WEB_USER
GRANT UPDATE ON [dbo].Emails TO DB_WEB_USER

GRANT ALTER ON [dbo].ProductRange TO DB_WEB_USER
GRANT DELETE ON [dbo].ProductRange TO DB_WEB_USER
GRANT INSERT ON [dbo].ProductRange TO DB_WEB_USER
GRANT SELECT ON [dbo].ProductRange TO DB_WEB_USER
GRANT UPDATE ON [dbo].ProductRange TO DB_WEB_USER


GRANT ALTER ON [dbo].Results TO DB_WEB_USER
GRANT DELETE ON [dbo].Results TO DB_WEB_USER
GRANT INSERT ON [dbo].Results TO DB_WEB_USER
GRANT SELECT ON [dbo].Results TO DB_WEB_USER
GRANT UPDATE ON [dbo].Results TO DB_WEB_USER


GRANT ALTER ON [dbo].TrilogyTestData TO DB_WEB_USER
GRANT DELETE ON [dbo].TrilogyTestData TO DB_WEB_USER
GRANT INSERT ON [dbo].TrilogyTestData TO DB_WEB_USER
GRANT SELECT ON [dbo].TrilogyTestData TO DB_WEB_USER
GRANT UPDATE ON [dbo].TrilogyTestData TO DB_WEB_USER


GRANT ALTER ON [dbo].[Identity] TO DB_WEB_USER
GRANT DELETE ON [dbo].[Identity] TO DB_WEB_USER
GRANT INSERT ON [dbo].[Identity] TO DB_WEB_USER
GRANT SELECT ON [dbo].[Identity] TO DB_WEB_USER
GRANT UPDATE ON [dbo].[Identity] TO DB_WEB_USER


GRANT ALTER ON [dbo].__MigrationHistory TO DB_WEB_USER
GRANT DELETE ON [dbo].__MigrationHistory TO DB_WEB_USER
GRANT INSERT ON [dbo].__MigrationHistory TO DB_WEB_USER
GRANT SELECT ON [dbo].__MigrationHistory TO DB_WEB_USER
GRANT UPDATE ON [dbo].__MigrationHistory TO DB_WEB_USER


GRANT ALTER ON [dbo].AspNetRoles TO DB_WEB_USER
GRANT DELETE ON [dbo].AspNetRoles TO DB_WEB_USER
GRANT INSERT ON [dbo].AspNetRoles TO DB_WEB_USER
GRANT SELECT ON [dbo].AspNetRoles TO DB_WEB_USER
GRANT UPDATE ON [dbo].AspNetRoles TO DB_WEB_USER


GRANT ALTER ON [dbo].AspNetUserClaims TO DB_WEB_USER
GRANT DELETE ON [dbo].AspNetUserClaims TO DB_WEB_USER
GRANT INSERT ON [dbo].AspNetUserClaims TO DB_WEB_USER
GRANT SELECT ON [dbo].AspNetUserClaims TO DB_WEB_USER
GRANT UPDATE ON [dbo].AspNetUserClaims TO DB_WEB_USER


GRANT ALTER ON [dbo].AspNetUserLogins TO DB_WEB_USER
GRANT DELETE ON [dbo].AspNetUserLogins TO DB_WEB_USER
GRANT INSERT ON [dbo].AspNetUserLogins TO DB_WEB_USER
GRANT SELECT ON [dbo].AspNetUserLogins TO DB_WEB_USER
GRANT UPDATE ON [dbo].AspNetUserLogins TO DB_WEB_USER


GRANT ALTER ON [dbo].AspNetUserRoles TO DB_WEB_USER
GRANT DELETE ON [dbo].AspNetUserRoles TO DB_WEB_USER
GRANT INSERT ON [dbo].AspNetUserRoles TO DB_WEB_USER
GRANT SELECT ON [dbo].AspNetUserRoles TO DB_WEB_USER
GRANT UPDATE ON [dbo].AspNetUserRoles TO DB_WEB_USER


GRANT ALTER ON [dbo].AspNetUsers TO DB_WEB_USER
GRANT DELETE ON [dbo].AspNetUsers TO DB_WEB_USER
GRANT INSERT ON [dbo].AspNetUsers TO DB_WEB_USER
GRANT SELECT ON [dbo].AspNetUsers TO DB_WEB_USER
GRANT UPDATE ON [dbo].AspNetUsers TO DB_WEB_USER

GRANT EXECUTE ON custom_select TO DB_WEB_USER
GRANT EXECUTE ON dropdown_email_changes TO DB_WEB_USER
GRANT EXECUTE ON get_dropdowns TO DB_WEB_USER
GRANT EXECUTE ON insert_user TO DB_WEB_USER
GRANT EXECUTE ON [Login] TO DB_WEB_USER
GRANT EXECUTE ON update_dropdown TO DB_WEB_USER
GRANT EXECUTE ON update_email TO DB_WEB_USER
GRANT EXECUTE ON update_productrange TO DB_WEB_USER
GRANT EXECUTE ON update_qd809 TO DB_WEB_USER
GRANT EXECUTE ON update_results TO DB_WEB_USER

CREATE INDEX idx_ProductRange ON [dbo].TrilogyTestData ([Product Range]);
CREATE INDEX idx_PartNumber ON [dbo].TrilogyTestData ([Part Number]);
CREATE INDEX idx_ProductionPCR ON [dbo].TrilogyTestData ([Production PCR]);
 
CREATE INDEX idx_IntegrationTestName ON [dbo].TrilogyTestData ([Integration Test Name]);
CREATE INDEX idx_ProductionTestName ON [dbo].TrilogyTestData ([Production Test Name]);

CREATE INDEX idx_DateFromEngineering ON [dbo].TrilogyTestData ([Date from engineering]);
CREATE INDEX idx_DateFromIntegrationTest ON [dbo].TrilogyTestData ([Date from Integration Test]);
CREATE INDEX idx_ProductionTestSignofDate ON [dbo].TrilogyTestData ([Production Test Signoff Date]);

CREATE INDEX idx_ProductionTestResult ON [dbo].TrilogyTestData ([Production Test Result]);
CREATE INDEX idx_IntegrationTestResult ON [dbo].TrilogyTestData ([Integration Test Result]);

CREATE INDEX idx_Emails_ID ON [dbo].Emails ([ID]);
CREATE INDEX idx_Emails_Integration ON [dbo].Emails ([Integration]);
CREATE INDEX idx_Emails_Production ON [dbo].Emails ([Production]);
CREATE INDEX idx_Emails_Engineering ON [dbo].Emails ([Engineering]);


CREATE INDEX idx_AspNetUsers_Email ON [dbo].AspNetUsers ([Email]);
CREATE INDEX idx_AspNetUsers_UserName ON [dbo].AspNetUsers ([UserName]);
CREATE INDEX idx_AspNetUsers_Isactive ON [dbo].AspNetUsers ([Isactive]);
GO