﻿using Microsoft.AspNet.SignalR;
using Microsoft.AspNet.SignalR.Hubs;
using QD809.Models;
using System.Web.Script.Serialization;

namespace QD809.Hubs
{
    public class PeerToPeer
    {
        [HubName("chatHub")]
        public class ChatHub : Hub
        {
            public void Send(Models.ChatModels.ChatDTO chatdto)
            {
                Clients.All.broadcastMessage(chatdto);
            }
        }
    }
}