﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(QD809.Startup))]
namespace QD809
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
            app.MapSignalR();
        }
    }
}
