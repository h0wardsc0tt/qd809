﻿using System.Web;
using System.Web.Optimization;

namespace QD809
{
    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-1.10.2.js",
                        "~/Scripts/1.11.4_jquery-ui.min.js",
                        "~/Scripts/qd809.js",
                        "~/Scripts/alasql.min.js",
                        "~/Scripts/qd809-functions.js",
                        "~/Scripts/jquery.signalR-2.2.2.min.js",
                        "~/Scripts/ajax.js"));

            bundles.Add(new ScriptBundle("~/bundles/admin").Include(
            "~/Scripts/jquery-1.10.2.js",
            "~/Scripts/1.11.4_jquery-ui.min.js",
            //"~/Scripts/qd809.js",
            "~/Scripts/alasql.min.js",
            "~/Scripts/qd809-functions.js",
            //"~/Scripts/jquery.signalR-2.2.2.min.js",
            "~/Scripts/ajax.js"));

            bundles.Add(new ScriptBundle("~/bundles/jquerylogin").Include(
                        "~/Scripts/jquery-1.10.2.js",
                        "~/Scripts/1.11.4_jquery-ui.min.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.validate*"));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at http://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/Scripts/bootstrap.js",
                      "~/Scripts/respond.js"));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                     "~/Content/bootstrap.css",
                     "~/Content/site.css",
                     "~/Content/4.7.0-font-awesome.min.css",
                     "~/Content/trilogy.css",
                     "~/Content/nav-bar.css",
                     "~/Content/qd809.css",
                     //"~/Content/main.css",
                     "~/Content/jquery-ui.css"
                    ));
            bundles.Add(new StyleBundle("~/Content/font-awesome").Include(
                      "~/Content/4.7.0-font-awesome.min.css"
                     ));

            bundles.Add(new StyleBundle("~/Content/hmeLogin").Include(
           "~/Content/media.css",
           "~/Content/3.3.5_bootstrap.min.css",
           "~/Content/hmeLogin.css",
           "~/Content/animate.min.css"));

        }
    }
}
