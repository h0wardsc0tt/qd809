﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using Microsoft.AspNet.Identity.EntityFramework;
using QD809.Models;

using hmeUtil;
using System.Web.Security;

namespace QD809.Controllers
{
    [Authorize(Roles = "Administrator")]
    public class dbController : Controller
    {
        private ApplicationUserManager _userManager;
        private ApplicationRoleManager _roleManager;

        [Authorize(Roles = "Administrator")]
        public ActionResult Index()
        {
            var userManager = Request.GetOwinContext().GetUserManager<ApplicationUserManager>();
            var roles = userManager.GetRoles(User.Identity.GetUserId());
            var result = UserManager.FindByName(User.Identity.Name);

            HomeModels startup = new HomeModels();
            JavaScriptSerializer json = new JavaScriptSerializer();
            HomeModels.Qd809BaseObj modelObj = startup.GetDropDowns();
            modelObj.userfirstname = result.FirstName;
            modelObj.userlastname = result.LastName;
            modelObj.useremailaddress = result.Email;
            modelObj.isadmin = roles.Contains("Administrator");

            ViewBag.json = json.Serialize(modelObj);
            ViewBag.ineditnotification = ConfigurationManager.AppSettings["ineditnotification"];
            ViewBag.ineditnotificationinterval = ConfigurationManager.AppSettings["ineditnotificationinterval"];
            ViewBag.ischecked = "checked";
            ViewBag.strikethrough = "strikethrough";


            return View(modelObj);

        }

        [HttpPost]
        public JsonResult EmailTransfer(DBModels.DropdownTransfer dto)
        {
            DBModels.responseDTO responseDTO = new DBModels.responseDTO();
            DBModels dbModels = new DBModels();
            var emailsTransfered = dbModels.emails_Transfered(dto);
            var response = dbModels.Transfer(dto);
            response.transfer.emailsTransferedList = emailsTransfered;

            foreach(var item in response.transfer.emailsTransferedList)
            {
                switch (item.dropdown)
                {
                    case "integration":
                    case "production":
                    sendenotifymail(item.qd809serial.ToString(), item.email, item.firstname + " " + item.lastname, item.productrange);
                    break;
                }

            }

            return Json(response);
        }

        [HttpPost]
        public JsonResult Update()
        {
            DBModels.responseDTO responseDTO = new DBModels.responseDTO();
            DBModels dbModels = new DBModels();
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            if (User.Identity.Name == "")
            {
                responseDTO.status = "login";
                return Json(responseDTO);
            }

            var userManager = Request.GetOwinContext().GetUserManager<ApplicationUserManager>();
            var roles = userManager.GetRoles(User.Identity.GetUserId());

            if (User != null && User.Identity != null && User.Identity.IsAuthenticated && roles.Contains("Administrator"))
            {
                var resolveRequest = HttpContext.Request;
                resolveRequest.InputStream.Seek(0, SeekOrigin.Begin);
                string jsonString = new StreamReader(resolveRequest.InputStream).ReadToEnd();
                Dictionary<string, string> json_dict;
                if (jsonString != null)
                {
                    json_dict = serializer.Deserialize<Dictionary<string, string>>(jsonString);
                    responseDTO = dbModels.Update(json_dict);
                    responseDTO.updatedto = json_dict;
                    return Json(responseDTO);
                }
                else
                {
                    responseDTO.status = "Failed:No Data";
                    return Json(responseDTO);
                }
            }
            else
            {
                responseDTO.status = "login";
                return Json(responseDTO);
            }
        }

        [HttpPost]
        public JsonResult UpdateProjectRange(DBModels.productrangeDTO dto)
        {
            DBModels dbModels = new DBModels();
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            if (User.Identity.Name == "")
            {
                dto.status = "login";
                return Json(dto);
            }

            var userManager = Request.GetOwinContext().GetUserManager<ApplicationUserManager>();
            var roles = userManager.GetRoles(User.Identity.GetUserId());

            if (User != null && User.Identity != null && User.Identity.IsAuthenticated && roles.Contains("Administrator"))
            {
                //var resolveRequest = HttpContext.Request;
                //resolveRequest.InputStream.Seek(0, SeekOrigin.Begin);
                //string jsonString = new StreamReader(resolveRequest.InputStream).ReadToEnd();
                //if (jsonString != null)
                //{
                    dto = dbModels.UpdateProductRange(dto);
                    return Json(dto);
                //}
                //else
                //{
                //    dto.status = "Failed:No Data";
                //    return Json(dto);
                //}
            }
            else
            {
                dto.status = "login";
                return Json(dto);
            }
        }

        [HttpPost]
        public JsonResult UpdateResults(DBModels.resultsDTO dto)
        {
            DBModels dbModels = new DBModels();
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            if (User.Identity.Name == "")
            {
                dto.status = "login";
                return Json(dto);
            }

            var userManager = Request.GetOwinContext().GetUserManager<ApplicationUserManager>();
            var roles = userManager.GetRoles(User.Identity.GetUserId());

            if (User != null && User.Identity != null && User.Identity.IsAuthenticated && roles.Contains("Administrator"))
            {
                dto = dbModels.UpdateResults(dto);
                return Json(dto);
            }
            else
            {
                dto.status = "login";
                return Json(dto);
            }
        }

        [HttpPost]
        public JsonResult UpdateResultsDefault(DBModels.resultsDTO dto)
        {
            DBModels dbModels = new DBModels();
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            if (User.Identity.Name == "")
            {
                dto.status = "login";
                return Json(dto);
            }

            var userManager = Request.GetOwinContext().GetUserManager<ApplicationUserManager>();
            var roles = userManager.GetRoles(User.Identity.GetUserId());

            if (User != null && User.Identity != null && User.Identity.IsAuthenticated && roles.Contains("Administrator"))
            {
                dto = dbModels.UpdateResultsDefault(dto);
                return Json(dto);
            }
            else
            {
                dto.status = "login";
                return Json(dto);
            }
        }
        #region public ApplicationUserManager UserManager
        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ??
                    HttpContext.GetOwinContext()
                    .GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }
        #endregion

        #region public ApplicationRoleManager RoleManager
        public ApplicationRoleManager RoleManager
        {
            get
            {
                return _roleManager ??
                    HttpContext.GetOwinContext()
                    .GetUserManager<ApplicationRoleManager>();
            }
            private set
            {
                _roleManager = value;
            }
        }
        #endregion

        private void sendenotifymail(string qdid, string emailaddr, string name, string prodrange)
        {
            EmailTemplateModels.renderEmail model = new EmailTemplateModels.renderEmail();
            model.header1 = "A QD809 record has been assigned to you.";
            model.footer1 = "";// ConfigurationManager.AppSettings["FooterPhoneNumber"];
            model.footer2 = "";// ConfigurationManager.AppSettings["FooterrURL"];
            model.maincontent = new List<EmailTemplateModels.content>();
            EmailTemplateModels.content content = new EmailTemplateModels.content();
            content.row = "A QD809 record " + qdid + " has been assigned to you " + name + " regarding the " + prodrange + " release.";
            content.order = 0;
            model.maincontent.Add(content);
            content = new EmailTemplateModels.content();
            content.row = "Please check the QD809 system on the Intranet";
            content.order = 1;
            model.maincontent.Add(content);
            content = new EmailTemplateModels.content();
            content.row = "Thank you";
            content.order = 2;
            model.maincontent.Add(content);
            content = new EmailTemplateModels.content();
            content.row = "The QD809 System.";
            content.order = 3;
            model.maincontent.Add(content);

            var emailbody = RenderRazorViewToString(@"emailtemplate", model);

            email email = new email();
            email.emailaddress emailaddress;
            email.attachment attachment;

            email.from = ConfigurationManager.AppSettings["SupportEmailAddr"];
            email.subject = "QD809 record  " + qdid + " has been assigned to you";

            email.to = new List<email.emailaddress>();
            emailaddress = new email.emailaddress();
            if (ConfigurationManager.AppSettings["isDebug"] == "true")
            {
                emailaddress.address = "hscott@hme.com";
            }
            else
            {
                emailaddress.address = emailaddr;
            }
            email.to.Add(emailaddress);

            email.attachments = new List<email.attachment>();
            attachment = new email.attachment();
            attachment.type = email.AttachmentType.filePath;
            attachment.filepath = Server.MapPath(@"~\images\trilogy-logo.png");
            attachment.inline = true;
            attachment.inlineid = "~attachment1~";
            email.attachments.Add(attachment);
            email.body = emailbody;// "<img src='cid:~attachment1~'><br> this is my test email body";
            bool ret = email.sendemail();
            email.Dispose();
        }

        private string RenderRazorViewToString(string viewName, object model)
        {
            ViewData.Model = model;

            using (var stringWriter = new System.IO.StringWriter())
            {
                var viewResult = ViewEngines.Engines.FindPartialView(ControllerContext, viewName);
                var viewContext = new ViewContext(ControllerContext, viewResult.View, ViewData, TempData, stringWriter);
                viewResult.View.Render(viewContext, stringWriter);
                viewResult.ViewEngine.ReleaseView(ControllerContext, viewResult.View);

                return stringWriter.GetStringBuilder().ToString();
            }
        }
    }
}
