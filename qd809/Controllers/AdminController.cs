﻿#region Includes
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.AspNet.Identity.EntityFramework;
using QD809.Models;
using PagedList;

using HME.SQLSERVER.DAL;

using hmeUtil;
using System.Configuration;
using System.Data.SqlClient;
using System.Data;

#endregion Includes

namespace QD809.Controllers
{
    public class AdminController : Controller
    {
        private ApplicationUserManager _userManager;
        private ApplicationRoleManager _roleManager;

        // Controllers

        // GET: /Admin/
        [Authorize(Roles = "Administrator")]
        #region public ActionResult Index(string searchStringUserNameOrEmail)
        public ActionResult Index(string searchStringUserNameOrEmail, string currentFilter, int? page)
        {
            try
            {
                int intPage = 1;
                int intPageSize = 5;
                int intTotalPageCount = 0;

                if (searchStringUserNameOrEmail != null)
                {
                    intPage = 1;
                }
                else
                {
                    if (currentFilter != null)
                    {
                        searchStringUserNameOrEmail = currentFilter;
                        intPage = page ?? 1;
                    }
                    else
                    {
                        searchStringUserNameOrEmail = "";
                        intPage = page ?? 1;
                    }
                }

                ViewBag.CurrentFilter = searchStringUserNameOrEmail;

                List<ExpandedUserDTO> col_UserDTO = new List<ExpandedUserDTO>();
                int intSkip = (intPage - 1) * intPageSize;

                intTotalPageCount = UserManager.Users
                    .Where(x => (x.UserName.Contains(searchStringUserNameOrEmail) || (x.Email.Contains(searchStringUserNameOrEmail))))
                    .Count();

                var result = UserManager.Users
                     .Where(x => (x.UserName.Contains(searchStringUserNameOrEmail) || (x.Email.Contains(searchStringUserNameOrEmail))))
                   //.Where(x => x.UserName.Contains(searchStringUserNameOrEmail))
                    .OrderBy(x => x.UserName)
                    .Skip(intSkip)
                    .Take(intPageSize)
                    .ToList();

                foreach (var item in result)
                {
                    ExpandedUserDTO objUserDTO = new ExpandedUserDTO();

                    objUserDTO.UserName = item.UserName;
                    objUserDTO.Email = item.Email;
                    objUserDTO.Isactive = item.Isactive;
                    objUserDTO.id = item.Id;
                    objUserDTO.LockoutEndDateUtc = item.LockoutEndDateUtc;
                    //var user = UserManager.FindByName(item.UserName);
                    col_UserDTO.Add(objUserDTO);
                }

                // Set the number of pages
                var _UserDTOAsIPagedList =
                    new StaticPagedList<ExpandedUserDTO>
                    (
                        col_UserDTO, intPage, intPageSize, intTotalPageCount
                    );
               
                ViewBag.EditUser = "";
                if (Session["editeuser"] != null)
                {
                    ViewBag.EditUser = Session["editeuser"].ToString();
                    Session["editeuser"] = null;
                }

                ViewBag.NewUser = "";
                if (Session["newuser"] != null)
                {
                    ViewBag.NewUser = Session["newuser"].ToString();
                    Session["newuser"] = null;
                }

                ViewBag.DeleteUser = "";
                if (Session["deleteuser"] != null)
                {
                    ViewBag.DeleteUser = Session["deleteuser"].ToString();
                    Session["deleteuser"] = null;
                }
                ViewBag.NoDelete = "";
                if (Session["nodelete"] != null)
                {
                    ViewBag.NoDelete = Session["nodelete"].ToString();
                    Session["nodelete"] = null;
                }
                return View(_UserDTOAsIPagedList);
            }
            catch (Exception ex)
            {
                ModelState.AddModelError(string.Empty, "Error: " + ex);
                List<ExpandedUserDTO> col_UserDTO = new List<ExpandedUserDTO>();

                return View(col_UserDTO.ToPagedList(1, 25));
            }
        }
        #endregion

        // Users *****************************

        // GET: /Admin/Edit/Create 
        [Authorize(Roles = "Administrator")]
        #region public ActionResult Create()
        public ActionResult Create()
        {
            ExpandedUserDTO objExpandedUserDTO = new ExpandedUserDTO();

            ViewBag.Roles = GetAllRolesAsSelectList();

            return View(objExpandedUserDTO);
        }
        #endregion

        // PUT: /Admin/Create
        [Authorize(Roles = "Administrator")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        #region public ActionResult Create(ExpandedUserDTO paramExpandedUserDTO)
        public async System.Threading.Tasks.Task<ActionResult> Create(ExpandedUserDTO paramExpandedUserDTO)
        {
            bool iserror = false;
            try
            {
                if (paramExpandedUserDTO == null)
                {
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                }
                var Email = string.Empty;
                var UserName = string.Empty;
                var Password = string.Empty;
                var FirstName = string.Empty;
                var LastName = string.Empty;
                var role = string.Empty;

                if (paramExpandedUserDTO.Email != null)
                    Email = paramExpandedUserDTO.Email.Trim();
                if (paramExpandedUserDTO.UserName != null)
                    UserName = paramExpandedUserDTO.UserName.Trim();
                if (paramExpandedUserDTO.Password != null)
                    Password = paramExpandedUserDTO.Password.Trim();
                if (paramExpandedUserDTO.FirstName != null)
                    FirstName = paramExpandedUserDTO.FirstName.Trim();
                if (paramExpandedUserDTO.LastName != null)
                    LastName = paramExpandedUserDTO.LastName.Trim();
                if (Request.Form["Roles"] != null)
                    role = Convert.ToString(Request.Form["Roles"]);

                if (UserName == string.Empty)
                {
                    iserror = true;
                    ModelState.AddModelError(string.Empty, "Error: No UserName");
                }

                if (FirstName == string.Empty)
                {
                    iserror = true;
                    ModelState.AddModelError(string.Empty, "Error: No FirstName");
                }

                if (LastName == string.Empty)
                {
                    iserror = true;
                    ModelState.AddModelError(string.Empty, "Error: No LastName");
                }

                if (Email == string.Empty)
                {
                    iserror = true;
                    ModelState.AddModelError(string.Empty, "Error: No Email");
                }
 
                if (Password == string.Empty)
                {
                    iserror = true;
                    ModelState.AddModelError(string.Empty, "Error: No Password");
                }

                if (role == "0")
                {
                    iserror = true;
                    ModelState.AddModelError(string.Empty, "Error: No Role");
                }

                if (iserror)
                {
                    ViewBag.Roles = GetAllRolesAsSelectList();
                    return View(paramExpandedUserDTO);
                }

                // UserName is LowerCase of the Email
                UserName = UserName.ToLower();

                // Create user

                var user = new ApplicationUser { UserName = UserName, Email = Email, FirstName = FirstName, LastName = LastName, Isactive = 1 };
                var AdminUserCreateResult = UserManager.Create(user, Password);

                if (AdminUserCreateResult.Succeeded == true)
                {
                    string strNewRole = Convert.ToString(Request.Form["Roles"]);

                    if (strNewRole != "0")
                    {
                        // Put user in role
                        UserManager.AddToRole(user.Id, strNewRole);
                    }
                    // Send an email with this link
                    string code = await UserManager.GenerateEmailConfirmationTokenAsync(user.Id);
                    var callbackUrl = Url.Action("ConfirmEmail", "Account", new { userId = user.Id, code = code }, protocol: Request.Url.Scheme);
                    //await UserManager.SendEmailAsync(user.Id, "Confirm your account", "Please confirm your account by clicking <a href=\"" + callbackUrl + "\">here</a>");
                    //bool ret = sendenotifymail("Confirm Email", "Please confirm your account by clicking", callbackUrl, "here.", Email, "Confirm Email");
                    bool ret = sendenotifymail("A user account " + UserName + " has been created for you on the QD809 System", "Please confirm your account by clicking", callbackUrl, "here.", Email, "New User Account " + UserName + " Created on QD809 System");
                    Session["newuser"] = FirstName + ',' + LastName + ',' + Email + ',' + user.Id;

                    List<SqlParameter> sp = new List<SqlParameter>();
                    sp.Add(new SqlParameter("firstname", FirstName));
                    sp.Add(new SqlParameter("lastname", LastName));
                    sp.Add(new SqlParameter("emailaddress", Email));
                    sp.Add(new SqlParameter("id", user.Id));
                    sp.Add(new SqlParameter("type", "insert"));
                    dbSQL db = new dbSQL();
                    NonQueryMessage nqm = db.execNonQuery(CommandType.StoredProcedure, "update_email", sp);
                    db.Dispose();

                    return Redirect("~/Admin");
                }
                else
                {
                    var xx = AdminUserCreateResult.Errors.ToList();
                    ViewBag.Roles = GetAllRolesAsSelectList();
                    ModelState.AddModelError(string.Empty,
                        xx[0]);
                    return View(paramExpandedUserDTO);
                }
            }
            catch (Exception ex)
            {
                ViewBag.Roles = GetAllRolesAsSelectList();
                ModelState.AddModelError(string.Empty, "Error: " + ex);
                return View("Create");
            }
        }
        #endregion

        // GET: /Admin/Edit/TestUser 
        [Authorize(Roles = "Administrator")]
        #region public ActionResult EditUser(string UserName)
        public ActionResult EditUser(string UserName)
        {
            if (UserName == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ExpandedUserDTO objExpandedUserDTO = GetUser(UserName);
            if (objExpandedUserDTO == null)
            {
                return HttpNotFound();
            }
            return View(objExpandedUserDTO);
        }
        #endregion

        // PUT: /Admin/EditUser
        [Authorize(Roles = "Administrator")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        #region public ActionResult EditUser(ExpandedUserDTO paramExpandedUserDTO)
        public ActionResult EditUser(ExpandedUserDTO paramExpandedUserDTO)
        {
            try
            {
                if (paramExpandedUserDTO == null)
                {
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                }

                ExpandedUserDTO objExpandedUserDTO = UpdateDTOUser(paramExpandedUserDTO);

                if (objExpandedUserDTO == null)
                {
                    return HttpNotFound();
                }
                var result = UserManager.FindByName(objExpandedUserDTO.UserName);

                Session["editeuser"] = result.FirstName + ',' + result.LastName + ',' + result.Email + ',' + result.Id;

                return Redirect("~/Admin");
            }
            catch (Exception ex)
            {
                ModelState.AddModelError(string.Empty, ex.Message);
                return View("EditUser", GetUser(paramExpandedUserDTO.UserName));
            }
        }
        #endregion

        // DELETE: /Admin/DeleteUser
        [Authorize(Roles = "Administrator")]
        #region public ActionResult DeleteUser(string UserName)
        public ActionResult DeleteUser(string UserName)
        {
            try
            {
                if (UserName == null)
                {
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                }

                if (UserName.ToLower() == this.User.Identity.Name.ToLower())
                {
                    ModelState.AddModelError(
                        string.Empty, "Error: Cannot delete the current user");

                    return View("EditUser");
                }

                ExpandedUserDTO objExpandedUserDTO = GetUser(UserName);

                if (objExpandedUserDTO == null)
                {
                    return HttpNotFound();
                }
                else
                {
                    var result = UserManager.FindByName(UserName);
                    string msg = string.Empty;
                    dbSQL db = new dbSQL();
                    DataSet ds = db.executeSqlString("select count(*) as count from TrilogyTestData where [Integration Test Name] = '" + result.Id + "' or [Production Test Name] = '" + result.Id + "' or [Engineering Test Name] = '" + result.Id + "'", out msg);
                    if ((int)ds.Tables[0].Rows[0]["count"] > 0)
                    {
                        Session["nodelete"] = result.Email;
                        return Redirect("~/Admin");
                    }

                    List<SqlParameter> sp = new List<SqlParameter>();
                    sp.Add(new SqlParameter("id", result.Id));
                    sp.Add(new SqlParameter("type", "delete"));
                    NonQueryMessage nqm = db.execNonQuery(CommandType.StoredProcedure, "update_email", sp);
                    db.Dispose();
                    DeleteUser(objExpandedUserDTO);
                    Session["deleteuser"] = result.FirstName + ',' + result.LastName + ',' + result.Email + ',' + result.Id;

                }

                return Redirect("~/Admin");
            }
            catch (Exception ex)
            {
                ModelState.AddModelError(string.Empty, "Error: " + ex);
                return View("EditUser", GetUser(UserName));
            }
        }
        #endregion
        // unlock: /Admin/unlockuser
        [Authorize(Roles = "Administrator")]
        #region public ActionResult UnlockUser(string UserName)
        public async System.Threading.Tasks.Task<ActionResult> UnlockUser(string UserName)
        {
            try
            {
                if (UserName == null)
                {
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                }

                if (UserName.ToLower() == this.User.Identity.Name.ToLower())
                {
                    ModelState.AddModelError(
                        string.Empty, "Error: Cannot unlock the current user");

                    return View("~/Admin");
                }

                var result = UserManager.FindByName(UserName);
                //await UserManager.SetLockoutEndDateAsync(result.Id, new DateTimeOffset(DateTime.UtcNow));
                dbSQL db = new dbSQL();
                db.execNonQueryString("update [AspNetUsers] set [LockoutEndDateUtc] = null where [Id] = '" + result.Id + "'");
                await UserManager.ResetAccessFailedCountAsync(result.Id);

                //var lockout = await UserManager.SetLockoutEnabledAsync(result.Id, false);
                //await UserManager.ResetAccessFailedCountAsync(result.Id);
                //if (lockout.Succeeded)
                //{
                //await UserManager.ResetAccessFailedCountAsync(result.Id);
                //lockout = await UserManager.SetLockoutEnabledAsync(result.Id, true);
                //}

                return Redirect("~/Admin");
            }
            catch (Exception ex)
            {
                ModelState.AddModelError(string.Empty, "Error: " + ex);
                return View("~/Admin");
            }
        }
        #endregion


        // IsActiveUser: /Admin/IsActiveUser
        [Authorize(Roles = "Administrator")]
        #region public ActionResult IsActiveUser(string UserName)
        public ActionResult IsActiveUser(string UserName)
        {
            try
            {
                if (UserName == null)
                {
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                }

                if (UserName.ToLower() == this.User.Identity.Name.ToLower())
                {
                    ModelState.AddModelError(
                        string.Empty, "Error: Cannot delete the current user");

                    return View("EditUser");
                }
 
                var result = UserManager.FindByName(UserName);
  
                ExpandedUserDTO objExpandedUserDTO = GetUser(UserName);

                if (objExpandedUserDTO == null)
                {
                    return HttpNotFound();
                }
                else
                {
                    result.Isactive = result.Isactive == 1 ? 0 : 1;
                    UserManager.Update(result);
                }

                return Redirect("~/Admin");
            }
            catch (Exception ex)
            {
                ModelState.AddModelError(string.Empty, "Error: " + ex);
                return View("EditUser", GetUser(UserName));
            }
        }
        #endregion

        // GET: /Admin/EditRoles/TestUser 
        [Authorize(Roles = "Administrator")]
        #region ActionResult EditRoles(string UserName)
        public ActionResult EditRoles(string UserName)
        {
            if (UserName == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            UserName = UserName.ToLower();

            // Check that we have an actual user
            ExpandedUserDTO objExpandedUserDTO = GetUser(UserName);

            if (objExpandedUserDTO == null)
            {
                return HttpNotFound();
            }

            UserAndRolesDTO objUserAndRolesDTO =
                GetUserAndRoles(UserName);

            return View(objUserAndRolesDTO);
        }
        #endregion

        // PUT: /Admin/EditRoles/TestUser 
        [Authorize(Roles = "Administrator")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        #region public ActionResult EditRoles(UserAndRolesDTO paramUserAndRolesDTO)
        public ActionResult EditRoles(UserAndRolesDTO paramUserAndRolesDTO)
        {
            try
            {
                if (paramUserAndRolesDTO == null)
                {
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                }

                string UserName = paramUserAndRolesDTO.UserName;
                string strNewRole = Convert.ToString(Request.Form["AddRole"]);

                if (strNewRole != "No Roles Found")
                {
                    // Go get the User
                    ApplicationUser user = UserManager.FindByName(UserName);

                    // Put user in role
                    UserManager.AddToRole(user.Id, strNewRole);
                }

                ViewBag.AddRole = new SelectList(RolesUserIsNotIn(UserName));

                UserAndRolesDTO objUserAndRolesDTO =
                    GetUserAndRoles(UserName);

                return View(objUserAndRolesDTO);
            }
            catch (Exception ex)
            {
                ModelState.AddModelError(string.Empty, "Error: " + ex);
                return View("EditRoles");
            }
        }
        #endregion

        // DELETE: /Admin/DeleteRole?UserName="TestUser&RoleName=Administrator
        [Authorize(Roles = "Administrator")]
        #region public ActionResult DeleteRole(string UserName, string RoleName)
        public ActionResult DeleteRole(string UserName, string RoleName)
        {
            try
            {
                if ((UserName == null) || (RoleName == null))
                {
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                }

                UserName = UserName.ToLower();

                // Check that we have an actual user
                ExpandedUserDTO objExpandedUserDTO = GetUser(UserName);

                if (objExpandedUserDTO == null)
                {
                    return HttpNotFound();
                }

                if (UserName.ToLower() ==
                    this.User.Identity.Name.ToLower() && RoleName == "Administrator")
                {
                    ModelState.AddModelError(string.Empty,
                        "Error: Cannot delete Administrator Role for the current user");
                }

                // Go get the User
                ApplicationUser user = UserManager.FindByName(UserName);
                // Remove User from role
                UserManager.RemoveFromRoles(user.Id, RoleName);
                UserManager.Update(user);

                ViewBag.AddRole = new SelectList(RolesUserIsNotIn(UserName));

                return RedirectToAction("EditRoles", new { UserName = UserName });
            }
            catch (Exception ex)
            {
                ModelState.AddModelError(string.Empty, "Error: " + ex);

                ViewBag.AddRole = new SelectList(RolesUserIsNotIn(UserName));

                UserAndRolesDTO objUserAndRolesDTO =
                    GetUserAndRoles(UserName);

                return View("EditRoles", objUserAndRolesDTO);
            }
        }
        #endregion

        // Roles *****************************

        // GET: /Admin/ViewAllRoles
        [Authorize(Roles = "Administrator")]
        #region public ActionResult ViewAllRoles()
        public ActionResult ViewAllRoles()
        {
            var roleManager =
                new RoleManager<IdentityRole>
                (
                    new RoleStore<IdentityRole>(new ApplicationDbContext())
                    );

            List<RoleDTO> colRoleDTO = (from objRole in roleManager.Roles
                                        select new RoleDTO
                                        {
                                            Id = objRole.Id,
                                            RoleName = objRole.Name
                                        }).ToList();

            return View(colRoleDTO);
        }
        #endregion

        // GET: /Admin/AddRole
        [Authorize(Roles = "Administrator")]
        #region public ActionResult AddRole()
        public ActionResult AddRole()
        {
            RoleDTO objRoleDTO = new RoleDTO();

            return View(objRoleDTO);
        }
        #endregion

        // PUT: /Admin/AddRole
        [Authorize(Roles = "Administrator")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        #region public ActionResult AddRole(RoleDTO paramRoleDTO)
        public ActionResult AddRole(RoleDTO paramRoleDTO)
        {
            try
            {
                if (paramRoleDTO == null)
                {
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                }

                var RoleName = paramRoleDTO.RoleName.Trim();

                if (RoleName == "")
                {
                    throw new Exception("No RoleName");
                }

                // Create Role
                var roleManager =
                    new RoleManager<IdentityRole>(
                        new RoleStore<IdentityRole>(new ApplicationDbContext())
                        );

                if (!roleManager.RoleExists(RoleName))
                {
                    roleManager.Create(new IdentityRole(RoleName));
                }

                return Redirect("~/Admin/ViewAllRoles");
            }
            catch (Exception ex)
            {
                ModelState.AddModelError(string.Empty, "Error: " + ex);
                return View("AddRole");
            }
        }
        #endregion

        // DELETE: /Admin/DeleteUserRole?RoleName=TestRole
        [Authorize(Roles = "Administrator")]
        #region public ActionResult DeleteUserRole(string RoleName)
        public ActionResult DeleteUserRole(string RoleName)
        {
            try
            {
                if (RoleName == null)
                {
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                }

                if (RoleName.ToLower() == "administrator")
                {
                    throw new Exception(String.Format("Cannot delete {0} Role.", RoleName));
                }

                var roleManager =
                    new RoleManager<IdentityRole>(
                        new RoleStore<IdentityRole>(new ApplicationDbContext()));

                var UsersInRole = roleManager.FindByName(RoleName).Users.Count();
                if (UsersInRole > 0)
                {
                    throw new Exception(
                        String.Format(
                            "Canot delete {0} Role because it still has users.",
                            RoleName)
                            );
                }

                var objRoleToDelete = (from objRole in roleManager.Roles
                                       where objRole.Name == RoleName
                                       select objRole).FirstOrDefault();
                if (objRoleToDelete != null)
                {
                    roleManager.Delete(objRoleToDelete);
                }
                else
                {
                    throw new Exception(
                        String.Format(
                            "Canot delete {0} Role does not exist.",
                            RoleName)
                            );
                }

                List<RoleDTO> colRoleDTO = (from objRole in roleManager.Roles
                                            select new RoleDTO
                                            {
                                                Id = objRole.Id,
                                                RoleName = objRole.Name
                                            }).ToList();

                return View("ViewAllRoles", colRoleDTO);
            }
            catch (Exception ex)
            {
                ModelState.AddModelError(string.Empty, "Error: " + ex.Message);

                var roleManager =
                    new RoleManager<IdentityRole>(
                        new RoleStore<IdentityRole>(new ApplicationDbContext()));

                List<RoleDTO> colRoleDTO = (from objRole in roleManager.Roles
                                            select new RoleDTO
                                            {
                                                Id = objRole.Id,
                                                RoleName = objRole.Name
                                            }).ToList();

                return View("ViewAllRoles", colRoleDTO);
            }
        }
        #endregion


        // Utility

        #region public ApplicationUserManager UserManager
        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ??
                    HttpContext.GetOwinContext()
                    .GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }
        #endregion

        #region public ApplicationRoleManager RoleManager
        public ApplicationRoleManager RoleManager
        {
            get
            {
                return _roleManager ??
                    HttpContext.GetOwinContext()
                    .GetUserManager<ApplicationRoleManager>();
            }
            private set
            {
                _roleManager = value;
            }
        }

        //public string FirstName { get; private set; }
        //public string LastName { get; private set; }
        #endregion

        #region private List<SelectListItem> GetAllRolesAsSelectList()
        private List<SelectListItem> GetAllRolesAsSelectList()
        {
            List<SelectListItem> SelectRoleListItems =
                new List<SelectListItem>();

            var roleManager =
                new RoleManager<IdentityRole>(
                    new RoleStore<IdentityRole>(new ApplicationDbContext()));

            var colRoleSelectList = roleManager.Roles.OrderBy(x => x.Name).ToList();

            SelectRoleListItems.Add(
                new SelectListItem
                {
                    Text = "Select",
                    Value = "0"
                });

            foreach (var item in colRoleSelectList)
            {
                SelectRoleListItems.Add(
                    new SelectListItem
                    {
                        Text = item.Name.ToString(),
                        Value = item.Name.ToString()
                    });
            }

            return SelectRoleListItems;
        }
        #endregion

        #region private ExpandedUserDTO GetUser(string paramUserName)
        private ExpandedUserDTO GetUser(string paramUserName)
        {
            ExpandedUserDTO objExpandedUserDTO = new ExpandedUserDTO();

            var result = UserManager.FindByName(paramUserName);

            // If we could not find the user, throw an exception
            if (result == null) throw new Exception("Could not find the User");

            objExpandedUserDTO.UserName = result.UserName;
            objExpandedUserDTO.Email = result.Email;
            objExpandedUserDTO.LockoutEndDateUtc = result.LockoutEndDateUtc;
            objExpandedUserDTO.AccessFailedCount = result.AccessFailedCount;
            objExpandedUserDTO.PhoneNumber = result.PhoneNumber;
            objExpandedUserDTO.FirstName = result.FirstName;
            objExpandedUserDTO.LastName = result.LastName;
            objExpandedUserDTO.Isactive = result.Isactive;
            objExpandedUserDTO.id = result.Id;

            return objExpandedUserDTO;
        }
        #endregion

        #region private ExpandedUserDTO UpdateDTOUser(ExpandedUserDTO objExpandedUserDTO)
        private ExpandedUserDTO UpdateDTOUser(ExpandedUserDTO paramExpandedUserDTO)
        {
            ApplicationUser result =
                UserManager.FindByName(paramExpandedUserDTO.UserName);

            // If we could not find the user, throw an exception
            if (result == null)
            {
                throw new Exception("Could not find the User");
            }

            result.Email = paramExpandedUserDTO.Email;
            result.FirstName = paramExpandedUserDTO.FirstName;
            result.LastName = paramExpandedUserDTO.LastName;
            if (result.FirstName == null)
            {
                throw new Exception("No First Name");
            }
            if (result.LastName == null)
            {
                throw new Exception("No Last Name");
            }

            // Lets check if the account needs to be unlocked
            if (UserManager.IsLockedOut(result.Id))
            {
                // Unlock user
                UserManager.ResetAccessFailedCountAsync(result.Id);
            }

            var results = UserManager.Update(result);
            if (results.Errors.Count() > 0)
            {
                throw new Exception(results.Errors.FirstOrDefault());
            }

            // Was a password sent across?
            if (!string.IsNullOrEmpty(paramExpandedUserDTO.Password))
            {
                // Remove current password
                var removePassword = UserManager.RemovePassword(result.Id);
                if (removePassword.Succeeded)
                {
                    // Add new password
                    var AddPassword =
                        UserManager.AddPassword(
                            result.Id,
                            paramExpandedUserDTO.Password
                            );

                    if (AddPassword.Errors.Count() > 0)
                    {
                        throw new Exception(AddPassword.Errors.FirstOrDefault());
                    }
                }
            }

            return paramExpandedUserDTO;
        }
        #endregion

        #region private void DeleteUser(ExpandedUserDTO paramExpandedUserDTO)
        private void DeleteUser(ExpandedUserDTO paramExpandedUserDTO)
        {
            ApplicationUser user =
                UserManager.FindByName(paramExpandedUserDTO.UserName);

            // If we could not find the user, throw an exception
            if (user == null)
            {
                throw new Exception("Could not find the User");
            }

            UserManager.RemoveFromRoles(user.Id, UserManager.GetRoles(user.Id).ToArray());
            UserManager.Update(user);
            UserManager.Delete(user);
        }
        #endregion

        #region private UserAndRolesDTO GetUserAndRoles(string UserName)
        private UserAndRolesDTO GetUserAndRoles(string UserName)
        {
            // Go get the User
            ApplicationUser user = UserManager.FindByName(UserName);

            List<UserRoleDTO> colUserRoleDTO =
                (from objRole in UserManager.GetRoles(user.Id)
                 select new UserRoleDTO
                 {
                     RoleName = objRole,
                     UserName = UserName
                 }).ToList();

            if (colUserRoleDTO.Count() == 0)
            {
                colUserRoleDTO.Add(new UserRoleDTO { RoleName = "No Roles Found" });
            }

            ViewBag.AddRole = new SelectList(RolesUserIsNotIn(UserName));

            // Create UserRolesAndPermissionsDTO
            UserAndRolesDTO objUserAndRolesDTO =
                new UserAndRolesDTO();
            objUserAndRolesDTO.UserName = UserName;
            objUserAndRolesDTO.colUserRoleDTO = colUserRoleDTO;
            return objUserAndRolesDTO;
        }
        #endregion

        #region private List<string> RolesUserIsNotIn(string UserName)
        private List<string> RolesUserIsNotIn(string UserName)
        {
            // Get roles the user is not in
            var colAllRoles = RoleManager.Roles.Select(x => x.Name).ToList();

            // Go get the roles for an individual
            ApplicationUser user = UserManager.FindByName(UserName);

            // If we could not find the user, throw an exception
            if (user == null)
            {
                throw new Exception("Could not find the User");
            }

            var colRolesForUser = UserManager.GetRoles(user.Id).ToList();
            var colRolesUserInNotIn = (from objRole in colAllRoles
                                       where !colRolesForUser.Contains(objRole)
                                       select objRole).ToList();

            if (colRolesUserInNotIn.Count() == 0)
            {
                colRolesUserInNotIn.Add("No Roles Found");
            }

            return colRolesUserInNotIn;
        }
        #endregion

        #region private void sendenotifymail(string header, string message, string url, string tag, string subject)
        private bool sendenotifymail(string header, string message, string url, string linktag, string to, string subject)
        {
            EmailTemplateModels.renderEmail model = new EmailTemplateModels.renderEmail();
            model.header1 = header;
            model.footer1 = "";// ConfigurationManager.AppSettings["FooterPhoneNumber"];
            model.footer2 = "";// ConfigurationManager.AppSettings["FooterrURL"];
            model.message = message;
            model.url = url;
            model.linktag = linktag;
            model.maincontent = new List<EmailTemplateModels.content>();
            EmailTemplateModels.content content = new EmailTemplateModels.content();
            content.row = message;
            content.order = 0;
            model.maincontent.Add(content);
            var emailbody = RenderRazorViewToString(@"emailtemplate", model);

            email email = new email();
            email.emailaddress emailaddress;
            email.attachment attachment;

            email.from = ConfigurationManager.AppSettings["SupportEmailAddr"];
            email.subject = subject;

            email.to = new List<email.emailaddress>();
            emailaddress = new email.emailaddress();
            //emailaddress.address = "hscott@hme.com";
            emailaddress.address = to;
            email.to.Add(emailaddress);

            email.attachments = new List<email.attachment>();
            attachment = new email.attachment();
            attachment.type = email.AttachmentType.filePath;
            attachment.filepath = Server.MapPath(@"~\images\trilogy-logo.png");
            attachment.inline = true;
            attachment.inlineid = "~attachment1~";
            email.attachments.Add(attachment);
            email.body = emailbody;
            bool ret = email.sendemail();
            email.Dispose();
            return ret;
        }
        #endregion

        #region private string RenderRazorViewToString(string viewName, object model)
        private string RenderRazorViewToString(string viewName, object model)
        {
            ViewData.Model = model;

            using (var stringWriter = new System.IO.StringWriter())
            {
                var viewResult = ViewEngines.Engines.FindPartialView(ControllerContext, viewName);
                var viewContext = new ViewContext(ControllerContext, viewResult.View, ViewData, TempData, stringWriter);
                viewResult.View.Render(viewContext, stringWriter);
                viewResult.ViewEngine.ReleaseView(ControllerContext, viewResult.View);

                return stringWriter.GetStringBuilder().ToString();
            }
        }
        #endregion

    }
}