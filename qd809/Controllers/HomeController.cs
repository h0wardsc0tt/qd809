﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using Microsoft.AspNet.Identity.EntityFramework;
using QD809.Models;

using hmeUtil;
using System.Web.Security;

namespace QD809.Controllers
{
    public class HomeController : Controller
    {
        private ApplicationUserManager _userManager;
        private ApplicationRoleManager _roleManager;

        public ActionResult Index()
        {
            if (User.Identity.Name == "")
            {
                return RedirectToAction("Login", "Account", new { @returnUrl = Request.QueryString.ToString() });
            }
            var userManager = Request.GetOwinContext().GetUserManager<ApplicationUserManager>();
            var roles = userManager.GetRoles(User.Identity.GetUserId());
            //ExpandedUserDTO objExpandedUserDTO = new ExpandedUserDTO();
            var result = UserManager.FindByName(User.Identity.Name);
            ViewBag.log_id = string.Empty;
            if (TempData["logid"] != null)
            {
                ViewBag.log_id = TempData["logid"];
            }
            else
            {
                try
                {
                    TempData["logid"] = Request["id"];
                    if(TempData["logid"] != null)
                    {
                        return Redirect("/");
                    }
                }
                catch { }
            }

            HomeModels startup = new HomeModels();
            JavaScriptSerializer json = new JavaScriptSerializer();
            HomeModels.Qd809BaseObj modelObj = startup.GetDropDowns();
            HomeModels.filterDTO filters = new HomeModels.filterDTO();
            modelObj.dto = new HomeModels.filterDTO();
            modelObj.dto.custom = ConfigurationManager.AppSettings["filter"]; ;
            modelObj.dto.itemsperpage = Convert.ToInt32(ConfigurationManager.AppSettings["itemsperpage"]);
            modelObj.dto.pagenumber = 1;
            modelObj.dto.sortcolumn = ConfigurationManager.AppSettings["sortcolumn"];
            modelObj.dto.sortorder = ConfigurationManager.AppSettings["sortorder"];
            modelObj.dto.filters = new HomeModels.Filters();
            modelObj.userfirstname = result.FirstName;
            modelObj.userlastname = result.LastName;
            modelObj.useremailaddress = result.Email;
            modelObj.isadmin = roles.Contains("Administrator");


            modelObj.dto = startup.Select(modelObj.dto);
            ViewBag.json = json.Serialize(modelObj);
            ViewBag.currentdisplay = modelObj.dto.count % modelObj.dto.itemsperpage;
            ViewBag.nextpage = modelObj.dto.count > modelObj.dto.itemsperpage ? "" : "disabled=\"disabled\"";
            ViewBag.sortcolumn = modelObj.dto.sortcolumn;
            ViewBag.sortorder = modelObj.dto.sortorder;
            ViewBag.sort = "sort";
            ViewBag.ineditnotification = ConfigurationManager.AppSettings["ineditnotification"];
            ViewBag.ineditnotificationinterval = ConfigurationManager.AppSettings["ineditnotificationinterval"];
            ViewBag.disabledcolor = "style=color:graytext;";
            ViewBag.notactive = "disabled";


            return View(modelObj);
        }

        [HttpPost]
        public JsonResult Search(HomeModels.filterDTO dto)
        {
            if (User != null && User.Identity != null && User.Identity.IsAuthenticated)
            {
                HomeModels homeModels = new HomeModels();
                var response = homeModels.Select(dto);
                string json = new JavaScriptSerializer().Serialize(response);
                return Json(response);
            }
            else
            {
                dto.status = "login";
                return Json(dto);
            }

        }

        [HttpPost]
        public JsonResult Update()
        {
            HomeModels.responseDTO responseDTO = new HomeModels.responseDTO();
            if (User != null && User.Identity != null && User.Identity.IsAuthenticated)
            {
                JavaScriptSerializer serializer = new JavaScriptSerializer();
                var resolveRequest = HttpContext.Request;
                resolveRequest.InputStream.Seek(0, SeekOrigin.Begin);
                string jsonString = new StreamReader(resolveRequest.InputStream).ReadToEnd();
                Dictionary<string, string> json_dict;
                if (jsonString != null)
                {
                    json_dict = serializer.Deserialize<Dictionary<string, string>>(jsonString);
                    HomeModels homeModels = new HomeModels();
                    HomeModels.responseDTO response = new HomeModels.responseDTO();
                    if (json_dict["updatetype"] == "insert")
                    {
                        response = homeModels.Update(json_dict);
                        HomeModels.filterDTO dto = new HomeModels.filterDTO();
                        dto.filters = new HomeModels.Filters();
                        dto.custom = "qd809serial";
                        dto.itemsperpage = 1;
                        dto.pagenumber = 1;
                        dto.filters.qd809serial = response.qd809serial;
                        response.record = (homeModels.Select(dto)).records[0];
                       
                        json_dict["qd809serial"] = response.qd809serial;
                        sendNotifications(json_dict);
                    }
                    else
                    {
                        sendNotifications(json_dict);
                        response = homeModels.Update(json_dict);
                    }

                    response.updatedto = json_dict;
                    return Json(response);
                    //HomeModels.updateDTO model = new HomeModels.updateDTO();
                    //model = (HomeModels.updateDTO)serializer.Deserialize(jsonString, typeof(HomeModels.updateDTO));
                }
                else
                {
                    responseDTO.status = "Failed:No Data";
                    return Json(responseDTO);
                }
            }
            else
            {
                responseDTO.status = "login";
                return Json(responseDTO);
            }
        }

        #region public ApplicationUserManager UserManager
        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ??
                    HttpContext.GetOwinContext()
                    .GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }
        #endregion

        #region public ApplicationRoleManager RoleManager
        public ApplicationRoleManager RoleManager
        {
            get
            {
                return _roleManager ??
                    HttpContext.GetOwinContext()
                    .GetUserManager<ApplicationRoleManager>();
            }
            private set
            {
                _roleManager = value;
            }
        }

        #endregion

        #region private List<SelectListItem> GetAllRolesAsSelectList()
        private List<SelectListItem> GetAllRolesAsSelectList()
        {
            List<SelectListItem> SelectRoleListItems =
                new List<SelectListItem>();

            var roleManager =
                new RoleManager<IdentityRole>(
                    new RoleStore<IdentityRole>(new ApplicationDbContext()));

            var colRoleSelectList = roleManager.Roles.OrderBy(x => x.Name).ToList();

            SelectRoleListItems.Add(
                new SelectListItem
                {
                    Text = "Select",
                    Value = "0"
                });

            foreach (var item in colRoleSelectList)
            {
                SelectRoleListItems.Add(
                    new SelectListItem
                    {
                        Text = item.Name.ToString(),
                        Value = item.Name.ToString()
                    });
            }

            return SelectRoleListItems;
        }
        #endregion

        #region private ExpandedUserDTO GetUser(string paramUserName)
        private ExpandedUserDTO GetUser(string paramUserName)
        {
            ExpandedUserDTO objExpandedUserDTO = new ExpandedUserDTO();

            var result = UserManager.FindByName(paramUserName);

            // If we could not find the user, throw an exception
            if (result == null) throw new Exception("Could not find the User");

            objExpandedUserDTO.UserName = result.UserName;
            objExpandedUserDTO.Email = result.Email;
            objExpandedUserDTO.LockoutEndDateUtc = result.LockoutEndDateUtc;
            objExpandedUserDTO.AccessFailedCount = result.AccessFailedCount;
            objExpandedUserDTO.PhoneNumber = result.PhoneNumber;

            return objExpandedUserDTO;
        }
        #endregion

        #region private sendNotifications(Dictionary<string, string> json_dict)
        private void sendNotifications(Dictionary<string, string> json_dict)
        {
            string updatetype = json_dict["updatetype"];
            HomeModels homeModels = new HomeModels();
            HomeModels.Qd809BaseObj modelObj = homeModels.GetDropDowns();

            HomeModels.filterDTO dto = new HomeModels.filterDTO();
            dto.filters = new HomeModels.Filters();
            dto.custom = "qd809serial";
            dto.itemsperpage = 1;
            dto.pagenumber = 1;
            dto.filters.qd809serial = json_dict["qd809serial"];
            HomeModels.filterDTO currentrecord = homeModels.Select(dto);
            string emailaddress = string.Empty;
            string firstname = string.Empty;
            string lastname = string.Empty;
            string prod_range = string.Empty;

            var integrationtestname = json_dict.Where(key => key.Key.Contains("integrationtestname")).ToList();
            var productiontestname = json_dict.Where(key => key.Key.Contains("productiontestname")).ToList();
            var engineeringtestname = json_dict.Where(key => key.Key.Contains("engineeringtestname")).ToList();

            if (updatetype == "insert")
            {
                if (integrationtestname.Count > 0)
                {
                    if (integrationtestname[0].Value != null)
                    {
                        var emailInfo = modelObj.dropdowns.emails.Single(s => s.key == Convert.ToString(integrationtestname[0].Value));
                        var prodrange = json_dict.Where(key => key.Key.Contains("productrange")).ToList();
                        var prodInfo = modelObj.dropdowns.productranges.Single(s => s.key == Convert.ToInt32(prodrange[0].Value));
                        prod_range = prodInfo.product_range;

                        emailaddress = emailInfo.emailaddress;
                        firstname = emailInfo.firstname;
                        lastname = emailInfo.lastname;
                        sendenotifymail(json_dict["qd809serial"], emailaddress, emailInfo.firstname + " " + emailInfo.lastname, prod_range, currentrecord);
                    }
                }
                if (productiontestname.Count > 0)
                {
                    if (productiontestname[0].Value != null)
                    {
                        var emailInfo = modelObj.dropdowns.emails.Single(s => s.key == Convert.ToString(productiontestname[0].Value));
                        var prodrange = json_dict.Where(key => key.Key.Contains("productrange")).ToList();
                        var prodInfo = modelObj.dropdowns.productranges.Single(s => s.key == Convert.ToInt32(prodrange[0].Value));
                        prod_range = prodInfo.product_range;

                        emailaddress = emailInfo.emailaddress;
                        firstname = emailInfo.firstname;
                        lastname = emailInfo.lastname;
                        sendenotifymail(json_dict["qd809serial"], emailaddress, emailInfo.firstname + " " + emailInfo.lastname, prod_range, currentrecord);                       
                    }
                }
            }
            else
            {
                if (integrationtestname.Count > 0)
                {
                    if (integrationtestname[0].Value != null)
                    {
                        if (integrationtestname[0].Value != currentrecord.records[0].integrationtestname)
                        {
                            var emailInfo = modelObj.dropdowns.emails.Single(s => s.key == Convert.ToString(integrationtestname[0].Value));
                            var prodrange = json_dict.Where(key => key.Key.Contains("productrange")).ToList();
                            if (prodrange.Count > 0)
                            {
                                var prodInfo = modelObj.dropdowns.productranges.Single(s => s.key == Convert.ToInt32(prodrange[0].Value));
                                prod_range = prodInfo.product_range;
                            }
                            else
                            {
                                var prodInfo = modelObj.dropdowns.productranges.Single(s => s.key == Convert.ToInt32(currentrecord.records[0].productrange));
                                prod_range = prodInfo.product_range;
                            }
                            emailaddress = emailInfo.emailaddress;
                            firstname = emailInfo.firstname;
                            lastname = emailInfo.lastname;
                            sendenotifymail(json_dict["qd809serial"], emailaddress, emailInfo.firstname + " " + emailInfo.lastname, prod_range, currentrecord);
                        }
                    }
                }
                if (productiontestname.Count > 0)
                {
                    if (productiontestname[0].Value != null)
                    {
                        if (productiontestname[0].Value != currentrecord.records[0].productiontestname)
                        {
                            var emailInfo = modelObj.dropdowns.emails.Single(s => s.key == Convert.ToString(productiontestname[0].Value));
                            var prodrange = json_dict.Where(key => key.Key.Contains("productrange")).ToList();
                            if (prodrange.Count > 0)
                            {
                                var prodInfo = modelObj.dropdowns.productranges.Single(s => s.key == Convert.ToInt32(prodrange[0].Value));
                                prod_range = prodInfo.product_range;
                            }
                            else
                            {
                                var prodInfo = modelObj.dropdowns.productranges.Single(s => s.key == Convert.ToInt32(currentrecord.records[0].productrange));
                                prod_range = prodInfo.product_range;
                            }
                            emailaddress = emailInfo.emailaddress;
                            firstname = emailInfo.firstname;
                            lastname = emailInfo.lastname;
                            sendenotifymail(json_dict["qd809serial"], emailaddress, emailInfo.firstname + " " + emailInfo.lastname, prod_range, currentrecord);
                        }
                    }
                }
            }
        }
        #endregion

        private string RenderRazorViewToString(string viewName, object model)
        {
            ViewData.Model = model;

            using (var stringWriter = new System.IO.StringWriter())
            {
                var viewResult = ViewEngines.Engines.FindPartialView(ControllerContext, viewName);
                var viewContext = new ViewContext(ControllerContext, viewResult.View, ViewData, TempData, stringWriter);
                viewResult.View.Render(viewContext, stringWriter);
                viewResult.ViewEngine.ReleaseView(ControllerContext, viewResult.View);

                return stringWriter.GetStringBuilder().ToString();
            }
        }

        private void sendenotifymail(string qdid, string emailaddr, string name, string prodrange, HomeModels.filterDTO record)
        {
            string url = ConfigurationManager.AppSettings["QD809URL"];
            EmailTemplateModels.renderEmail model = new EmailTemplateModels.renderEmail();
            model.header1 = "A QD809 record has been assigned to you.";
            model.footer1 = "";// ConfigurationManager.AppSettings["FooterPhoneNumber"];
            model.footer2 = "";// ConfigurationManager.AppSettings["FooterrURL"];
            model.maincontent = new List<EmailTemplateModels.content>();
            EmailTemplateModels.content content = new EmailTemplateModels.content();
            content.row = "A QD809 record " + qdid + " has been assigned to you " + name + " regarding the "+ prodrange + " release.";
            content.order = 0;
            model.maincontent.Add(content);
            content = new EmailTemplateModels.content();
            content.row = "Please check the <a href='" + url + "/?id=" + qdid + "'>QD809</a> system on the Intranet";
            content.order = 1;
            model.maincontent.Add(content);

            content = new EmailTemplateModels.content();
            content.row = "Serial Number : " + record.records[0].qd809serial;
            content.order = 2;
            model.maincontent.Add(content);

            content = new EmailTemplateModels.content();
            content.row = "Product Range : " + prodrange;
            content.order = 3;
            model.maincontent.Add(content);

            content = new EmailTemplateModels.content();
            content.row = "Part Number : " + record.records[0].partnumber;
            content.order = 4;
            model.maincontent.Add(content);

            content = new EmailTemplateModels.content();
            content.row = "Description  : " + record.records[0].description;
            content.order = 5;
            model.maincontent.Add(content);


            content = new EmailTemplateModels.content();
            content.row = "Thank you";
            content.order = 6;
            model.maincontent.Add(content);

            var emailbody = RenderRazorViewToString(@"emailtemplate", model);

            email email = new email();
            email.emailaddress emailaddress;
            email.attachment attachment;

            email.from = ConfigurationManager.AppSettings["SupportEmailAddr"];
            email.subject = "QD809 record  " + qdid + " has been assigned to you";

            email.to = new List<email.emailaddress>();
            emailaddress = new email.emailaddress();
            if(ConfigurationManager.AppSettings["isDebug"] == "true")
            {
                emailaddress.address = "hscott@hme.com";
            }
            else
            {
                emailaddress.address = emailaddr;
            }
            email.to.Add(emailaddress);

            //email.cc = new List<email.emailaddress>();
            //emailaddress = new email.emailaddress();
            //emailaddress.address = "hscott@currentevent.com";
            //email.cc.Add(emailaddress);
            //email.bcc = new List<email.emailaddress>();
            //emailaddress = new email.emailaddress();
            //emailaddress.address = "hscott@hme.com";
            //email.bcc.Add(emailaddress);

            email.attachments = new List<email.attachment>();
            attachment = new email.attachment();
            attachment.type = email.AttachmentType.filePath;
            attachment.filepath = Server.MapPath(@"~\images\trilogy-logo.png");
            attachment.inline = true;
            attachment.inlineid = "~attachment1~";
            email.body = emailbody;// "<img src='cid:~attachment1~'><br> this is my test email body";
            bool ret = email.sendemail();
            email.Dispose();
        }
        private void sendemail()
        {
            EmailTemplateModels.renderEmail model = new EmailTemplateModels.renderEmail();
            model.header1 = "Password Reset Confirmation";
            model.footer1 = ConfigurationManager.AppSettings["FooterPhoneNumber"];
            model.footer2 = ConfigurationManager.AppSettings["FooterrURL"];
            model.maincontent = new List<EmailTemplateModels.content>();
            EmailTemplateModels.content content = new EmailTemplateModels.content();
            content.row = "This email confirms your recent password change.";
            content.order = 0;
            content.row = "If your password was changed without your knowledge, then please click the link below to change it again:";
            content.order = 1;
            content.row = "http://www.trilogy.com/?pg=ManageAccount&st=rq";
            content.order = 2;
            content.row = "Thank you.";
            content.order = 3;
            var emailbody = RenderRazorViewToString(@"emailtemplate", model);

            email email = new email();
            email.emailaddress emailaddress;
            email.attachment attachment;

            email.from = ConfigurationManager.AppSettings["SupportEmailAddr"];
            email.subject = "test message";

            email.to = new List<email.emailaddress>();
            emailaddress = new email.emailaddress();
            emailaddress.address = "hscott@hme.com";
            email.to.Add(emailaddress);

            //email.cc = new List<email.emailaddress>();
            //emailaddress = new email.emailaddress();
            //emailaddress.address = "hscott@currentevent.com";
            //email.cc.Add(emailaddress);

            email.attachments = new List<email.attachment>();
            attachment = new email.attachment();
            attachment.type = email.AttachmentType.filePath;
            attachment.filepath = Server.MapPath(@"~\images\trilogy-logo.png");
            attachment.inline = true;
            attachment.inlineid = "~attachment1~";
            email.attachments.Add(attachment);
            email.body = emailbody;// "<img src='cid:~attachment1~'><br> this is my test email body";
            bool ret = email.sendemail();
            email.Dispose();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}