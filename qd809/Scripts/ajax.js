﻿function qd809Serial_Lookup() {
    var id = $('#Search').val();
    if (id == '') {
        return;
    }
    qd809_Lookup($('#Search').val())
}

function qd809_Lookup(id) {
    if (changed_flag && !override) {
        $('#continue_function').val('qd809Serial_Lookup');
        $('#continue_function_parms').val('');
        $('#edit_active').center();
        //$('#edit_active').css('top', '80px');
        $('#edit_active').slideDown("slow", function () {
            // Animation complete.
        });
        return;
    }
    jsonObject = { "custom": 'qd809serial', "sortcolumn": "", "sortorder": "", "records": "{}", "storedprocedure": "", "pagenumber": 1, "itemsperpage": 1 };
    var filters = {};
    filters.qd809serial = id;
    jsonObject.filters = filters;
    //console.log(jsonObject);
    $.ajax({
        url: "/Home/Search",
        type: "POST",
        data: JSON.stringify(jsonObject),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            //console.log(response);
            if (response.status == 'login') {
                $('#reloginModal_iframe').attr('src', '/ajax/relogin/1');
                $('#reloginModal').modal('show');
            }
            if (response.status == 'OK') {
                if (response.count == 0) {
                    $('#msg').html('QD809 Serial Number ' + response.filters.qd809serial + ' Does not exist in the database.');
                    $('#message').center();
                    //$('#message').css('top', '80px');
                    $('#message').slideDown("slow", function () {
                        setTimeout(function () {
                            $('#message').hide(2000);
                        }, 3000);
                    });
                    return;
                }
                showreport();
                jsonqd809.record = response.records[0];
                updatereportpage(response.records[0]);
            }
        },
        error: function (response) {
            console.log(response.responseText);
        }
    });
}

function search_submit(isfilter) {
    //jsonObject = {"custom": custom, "sortcolumn": sortcolumn, "sortorder": sortorder, "records": "{}", "storedprocedure": "", "pagenumber": 1, "itemsperpage": 5000};
    var clone_dto = $.parseJSON(JSON.stringify(jsonqd809.dto));
    if (isfilter) {
        clone_dto.filters = {};
        var val = '';
        $('.s-form').each(function (i, item) {
            clone_dto.filters[item.id.replace(/s_/g, "")] = $(item).val();
        });
        clone_dto.custom = 'search';
    }
    clone_dto.records = [];
    clone_dto.record = {};
    clone_dto.status = '';
    console.log(clone_dto);
    $.ajax({
        url: "/Home/Search",
        type: "POST",
        data: JSON.stringify(clone_dto),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            //console.log(response);
            if (response.status == 'login') {
                $('#reloginModal_iframe').attr('src', '/ajax/relogin/1');
                $('#reloginModal').modal('show');
            }
            if (response.status == 'OK') {
                jsonqd809.dto = response;
                updategrid()
            }
        },
        error: function (response) {
            console.log(response.responseText);
        }
    });
}

function update_qd809report(dto) {
    //console.log(dto);
    $.ajax({
        url: "/Home/update",
        type: "POST",
        data: JSON.stringify(dto),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            //console.log(response);
            if (response.status == 'login') {
                $('#reloginModal_iframe').attr('src', '/ajax/relogin/2');
                $('#reloginModal').modal('show');
            }
            if (response.status == 'OK') {
                $('#fai_form').find('[changed ="1"]').attr("changed", '0');
                changed_flag = false;
                $('#save_report').hide();
                $('#cancel_report').hide();

                if ($('#fai_report #integrationtestname').val() != null) {
                    $("#fai_report #integrationtestname option[value=-1]").prop('disabled', true);
                }
                if ($('#fai_report #productiontestname').val() != null) {
                    $("#fai_report #productiontestname option[value=-1]").prop('disabled', true);
                }
                if ($('#fai_report #engineeringtestname').val() != null) {
                    $("#fai_report #engineeringtestname option[value=-1]").prop('disabled', true);
                }

                switch (response.updatetype) {
                    case 'insert':
                        bell_update('bell-transparent')
                        $('#qd809serial').val(response.qd809serial);
                        $('#msg').html('qd809 report: ' + response.qd809serial + ' has been Added');
                        $('#message').center();
                        //$('#message').css('top', '80px');
                        $('#message').slideDown("slow", function () {
                            setTimeout(function () {
                                setTimeout(function () {
                                    $('#message').slideUp("slow", function () {
                                    });
                                }, 2000);
                            }, 3000);
                        });
                        jsonqd809.record = response.record;
                        break;
                    case 'update':
                        bell_update('bell-transparent')
                        if (ineditnotification) {
                            signalRNotify('qd809update', JSON.stringify(response), response.qd809serial);
                        }
                        else {
                            updateChanges(response)
                        }

                        notification('saved')

                        $('#msg').html('qd809 report: ' + response.qd809serial + ' has been updated');
                        $('#message').center();
                        //$('#message').css('top', '80px');
                        $('#message').slideDown("slow", function () {
                            // Animation complete.
                            setTimeout(function () {
                                $('#message').slideUp("slow", function () {
                                });
                            }, 2000);
                        });
                        break;
                }
            }
        },
        error: function (response) {
            console.log(response.responseText);
        }
    });
}

function updateChanges(response) {
    var index = jsonqd809.dto.records.map(function (qd809serial) { return qd809serial['qd809serial']; }).indexOf(response.qd809serial);
    var isrecord = false;
    if (jsonqd809.record != null)
        isrecord = jsonqd809.record.qd809serial == response.qd809serial ? true : false;
    var row = $('#Cust_Results tbody').find("tr[id='qd_" + response.qd809serial + "']");
    if (row.is("tr")) {
        var tmp = [], tmpval = '';
        $.each(response.updatedto, function (key, value) {
            //console.log(key+'   '+value);
            tmp = []
            tmpval = '';
            tmpstyle = '';
            switch (key) {
                case 'productrange':
                    tmp = alasql("select * from ? where key = " + value, [jsonqd809.dropdowns.productranges]);
                    tmpval = (tmp.length == 0 ? '' : tmp[0].product_range)
                    tmpstyle = (tmp.length == 0 ? '' : tmp[0].isactive == 'False' ? 'strikethrough' : '')
                    $(row).find("td[qd=" + key + "]").html(tmpval);
                    $(row).find("td[qd=" + key + "]").addClass(tmpstyle);
                    if (index >= 0) {
                        jsonqd809.dto.records[index][key] = value;
                    }
                    if (isrecord) {
                        jsonqd809.record[key] = value;
                    }
                    break;
                case 'integrationtestresult':
                    tmp = alasql("select * from ? where key = " + value, [jsonqd809.dropdowns.results]);
                    tmpval = (tmp.length == 0 ? '' : tmp[0].status)
                    $(row).find("td[qd=" + key + "]").html(tmpval);
                    if (index >= 0) {
                        jsonqd809.dto.records[index][key] = value;
                    }
                    if (isrecord) {
                        jsonqd809.record[key] = value;
                    }
                    break;
                case 'productiontestresult':
                    tmp = alasql("select * from ? where key = " + value , [jsonqd809.dropdowns.results]);
                    tmpval = (tmp.length == 0 ? '' : tmp[0].status)
                    $(row).find("td[qd=" + key + "]").html(tmpval);
                    if (index >= 0) {
                        jsonqd809.dto.records[index][key] = value;
                    }
                    if (isrecord) {
                        jsonqd809.record[key] = value;
                    }
                    break;
                case 'integrationtestname':
                    tmp = alasql("select * from ? where key = '" + value + "'", [jsonqd809.dropdowns.emails]);
                    tmpval = (tmp.length == 0 ? '' : tmp[0].firstname + ' ' + tmp[0].lastname)
                    $(row).find("td[qd=" + key + "]").html(tmpval);
                    if (index >= 0) {
                        jsonqd809.dto.records[index][key] = value;
                    }
                    if (isrecord) {
                        jsonqd809.record[key] = value;
                    }
                    break;
                case 'partnumber':
                case 'revision':
                case 'productionpcr':
                case 'datefromengineering':
                case 'productiontestsignoffdate':
                case 'datefromintegrationtest':
                case 'productiontestdaterecevied':
                    $(row).find("td[qd=" + key + "]").html(value);
                    if (index >= 0) {
                        jsonqd809.dto.records[index][key] = value;
                    }
                    if (isrecord) {
                         jsonqd809.record[key] = value;
                    }
                    break;
                default:
                    //if (isrecord) {
                        switch (key) {
                            case 'updatetype':
                            case 'qd809serial':
                            case 'status':
                                break;
                            default:
                                if (index >= 0) {
                                    jsonqd809.dto.records[index][key] = value;
                                }
                                if (isrecord) {
                                    jsonqd809.record[key] = value;
                                }
                                break;
                        }
                    //}
                    break;
            }
        });
        if (isrecord && jsonqd809.record.isactive == 0) {
            row.css('display', 'none');
            if (chatdto.id == $.connection.hub.id) {
                newreportclick();
                showsearchpage();
            } else {
                if (isrecord) {
                    newreportclick();
                    showsearchpage();
                }
            }
        }
    }
}

function closeRelogin(cmd) {
    //console.log('closeRelogin:' + cmd);
    $('#reloginModal').modal('hide');
    switch (cmd) {
        case '1':
            //search_submit(true)
            break;
        case '2':
            //qd809Serial_Lookup();
            break;
    }
}

function update_email_ajax(dto) {
    //console.log(dto);
    $.ajax({
        url: "/DB/update",
        type: "POST",
        data: JSON.stringify(dto),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            //console.log(response);
            if (response.status == 'login') {
                $('#reloginModal_iframe').attr('src', '/ajax/relogin/2');
                $('#reloginModal').modal('show');
            }
            if (response.status == 'OK') {
                 if (ineditnotification) {
                    window.parent.emailUpdateNotify('emailEdit', JSON.stringify(response));
                }
                else {
                     window.parent.adminEmailChanged(response)
                }
                switch (response.type) {
                    case 'update':
                        updateEmailChanges(response);
                        break;
                    case 'insert':
                        insertEmail(response);
                        break;
                }
            }
        },
        error: function (response) {
            console.log(response.responseText);
        }
    });
}

function email_transfer_ajax(dto) {
    //console.log(dto);
    $.ajax({
        url: "/DB/EmailTransfer",
        type: "POST",
        data: JSON.stringify(dto),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            console.log(response);
            if (response.status == 'OK') {
                if (ineditnotification) {
                    window.parent.emailUpdateNotify('transferemail', JSON.stringify(response.transfer));
                }
                else {
                    window.parent.transferemail(response.transfer);
                }
            }
        },
        error: function (response) {
            console.log(response.responseText);
        }
    });
}

function productrangeupdate_ajax(dto) {
    //console.log(dto);
    $.ajax({
        url: "/DB/UpdateProjectRange",
        type: "POST",
        data: JSON.stringify(dto),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            //console.log('productrangeupdate_ajax');
            //console.log(response);
            if (response.status == 'login') {
                $('#reloginModal_iframe').attr('src', '/ajax/relogin/2');
                $('#reloginModal').modal('show');
            }
            if (response.status == 'OK') {
                updateProductRangeScreen(response);
                if (ineditnotification) {
                    window.parent.productrangeUpdateNotify('productrangeUpdate', JSON.stringify(response));
                }
                else {
                    window.parent.updateProductRangeDropdown(response, 'local');
                }
            }
            if (response.status == 'Assigned') {
                var msg = 'Product Range ' + dto.productrange + ' is current assigned.  Unable to delete.';
                displaymessage(msg, 2000);
            }
        },
        error: function (response) {
            console.log(response.responseText);
        }
    });
}

function resultsupdate_ajax(dto) {
    //console.log(dto);
    $.ajax({
        url: "/DB/UpdateResults",
        type: "POST",
        data: JSON.stringify(dto),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            console.log(response);
            if (response.status == 'login') {
                $('#reloginModal_iframe').attr('src', '/ajax/relogin/2');
                $('#reloginModal').modal('show');
            }
            if (response.status == 'OK') {
                updateResultsScreen(response);
                if (ineditnotification) {
                    window.parent.resultsUpdateNotify('resultsUpdate', JSON.stringify(response));
                }
                else {
                    window.parent.updateResultsDropdown(response, 'local');
                }
            }
            if (response.status == 'Assigned') {
                var msg = 'Results ' + dto.productrange + ' is current assigned.  Unable to delete.';
                displaymessage(msg, 2000);
            }
        },
        error: function (response) {
            console.log(response.responseText);
        }
    });
}

function resultsupdatedefault_ajax(dto) {
    console.log(dto);
    $.ajax({
        url: "/DB/UpdateResultsDefault",
        type: "POST",
        data: JSON.stringify(dto),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            console.log(response);
            if (response.status == 'login') {
                $('#reloginModal_iframe').attr('src', '/ajax/relogin/2');
                $('#reloginModal').modal('show');
            }
            if (response.status == 'OK') {
                updateResultsScreenDefault(response);
                if (ineditnotification) {
                    window.parent.resultsUpdateNotify('resultsDefaultUpdate', JSON.stringify(response));
                }
                else {
                    window.parent.updateResultsDefault(response, 'local');
                }
            }
        },
        error: function (response) {
            console.log(response.responseText);
        }
    });
}



