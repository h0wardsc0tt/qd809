﻿var changed_flag = false;
var json_update = {};
var json_update_dto
var jsonObject = {};
var custom = 'custom';
var sortcolumn = 'hardware';
var sortorder = 'asc';
var qd809serial = '';
var override = false;
var chat;
var inedit;
var tryingToReconnect = false;
var pageLeave = false;


jQuery.fn.center = function () {
    this.css("position", "absolute");
    this.css("top", Math.max(0, (($(window).height() - $(this).outerHeight()) / 2) +
                                                $(window).scrollTop()) + "px");
    this.css("left", Math.max(0, (($(window).width() - $(this).outerWidth()) / 2) +
                                                $(window).scrollLeft()) + "px");
    return this;
}
Number.prototype.padLeft = function (base, chr) {
    var len = (String(base || 10).length - String(this).length) + 1;
    return len > 0 ? new Array(len).join(chr || '0') + this : this;
}

$(document).ready(function () {
    jQuery(function ($) {
        $.datepicker.regional['en-GB'] = {
            closeText: 'Done',
            prevText: 'Prev',
            nextText: 'Next',
            currentText: 'Today',
            monthNames: ['January', 'February', 'March', 'April', 'May', 'June',
            'July', 'August', 'September', 'October', 'November', 'December'],
            monthNamesShort: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun',
            'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'],
            dayNames: ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'],
            dayNamesShort: ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'],
            dayNamesMin: ['Su', 'Mo', 'Tu', 'We', 'Th', 'Fr', 'Sa'],
            weekHeader: 'Wk',
            dateFormat: 'dd/mm/yy',
            firstDay: 1,
            isRTL: false,
            showMonthAfterYear: false,
            yearSuffix: ''
        };
        $.datepicker.setDefaults($.datepicker.regional['en-GB']);
    });

    //$(".input-date").datepicker({ dateFormat: 'dd/mm/yy' }).val();
    $(".input-date").datepicker().val();

    $('.modal-dialog').draggable({
        handle: ".modal-header"
    });

    //$('#alertModal').on('hidden.bs.modal', function (e) {
    //    // reposition modal according to window / viewport here
    //});

    //$(".cal-field").click(function (e) {
    //    $(this).datepicker('setDate', null);
    //});

    //$(".cal-field").datepicker({
    //    onSelect: function () { }
    //});

    $('.test-results').change(function (e) {
        if ($(this).val() == '-1')
            $(this).val('-2');
    });

    updategrid();
    monitor_changes();

    $(function () {
        // Declare a proxy to reference the hub.
        chat = $.connection.chatHub;
        chat.client.broadcastMessage = function (chatdto) {
            console.log(chatdto);
            switch (chatdto.request) {
                case 'inedit':
                    if (jsonqd809.record == null)
                        return;
                    if (chatdto.id == $.connection.hub.id) {
                        if (changed_flag && chatdto.qd809serialnumber == jsonqd809.record.qd809serial) {
                            switch (chatdto.status) {
                                case 'inedit':
                                    bell_update('bell-green')
                                    break;
                                case 'unsaved':
                                    bell_update('bell-green')
                                    break;
                                case 'canceled':
                                case 'saved':
                                    bell_update('bell-transparent')
                                    break;
                                default:
                                    bell_update('bell-transparent')
                                    break;
                            }
                        }
                        else {
                            if (chatdto.qd809serialnumber == jsonqd809.record.qd809serial) {
                                switch (chatdto.status) {
                                    case 'inedit':
                                        bell_update('bell-green')
                                        break;
                                    case 'unsaved':
                                        bell_update('bell-yellow')
                                        setTimeout(function () {
                                            if ($('.busy-bell').hasClass('bell-yellow')) {
                                                set_overlay('hide')
                                                bell_update('bell-transparent')
                                            }
                                        }, 3000);

                                        break;
                                    case 'saved':
                                        bell_update('bell-transparent')
                                        break;
                                    default:
                                        bell_update('bell-transparent')
                                        break;
                                }
                            }
                        }
                    }
                    if (chatdto.id != $.connection.hub.id) {
                        if (chatdto.qd809serialnumber == jsonqd809.record.qd809serial) {//$('#fai_report').is(':visible') && 
                            switch (chatdto.status) {
                                case 'inedit':
                                    bell_update('bell-red');
                                    $('.busy-bell').attr('title', chatdto.firstname + ' ' + chatdto.lastname);
                                    set_overlay('show');
                                    break;
                                case 'unsaved':
                                    bell_update('bell-yellow')
                                    setTimeout(function () {
                                        if ($('.busy-bell').hasClass('bell-yellow')) {
                                            set_overlay('hide')
                                            bell_update('bell-transparent')
                                        }
                                    }, 10000);
                                    break;
                                case 'saved':
                                    bell_update('bell-transparent')
                                    if (jsonqd809.record.qd809serial == chatdto.qd809serialnumber) {
                                        qd809_Lookup(jsonqd809.record.qd809serial)
                                        set_overlay('hide')
                                    }
                                    break;
                                default:
                                    bell_update('bell-transparent')
                                    set_overlay('hide')
                                    break;
                            }
                        }
                    }
                    break;
                case 'qd809update':
                    //console.log(JSON.parse(chatdto.updatedto));
                    updateChanges(JSON.parse(chatdto.updatedto))
                    break;
                case 'emailEdit':
                    //console.log(JSON.parse(chatdto.updatedto));
                    adminEmailChanged(JSON.parse(chatdto.updatedto))
                    break;
                case 'iupdate':
                    idenityDropDownUpdate(chatdto.updatedto)
                    break;
                case 'inewuser':
                    idenityNewUser(chatdto.updatedto)
                    break;
                case 'iedituser':
                    idenityEditUser(chatdto)
                    break;
                case 'idetete':
                    idenityDeteteUser(chatdto.updatedto)
                    break;
                case 'transferemail':
                    transferemail(chatdto)
                    break;
                case 'productrangeUpdate':
                    updateProductRangeDropdown(JSON.parse(chatdto.updatedto),'signalR');
                    break;
                case 'resultsUpdate':
                    updateResultsDropdown(JSON.parse(chatdto.updatedto), 'signalR');
                    break;
                case 'resultsDefaultUpdate':
                    updateResultsDefault(JSON.parse(chatdto.updatedto), 'signalR');
                    break;
            }
        };

        $.connection.hub.start().done(function () {
            $('.bolt').removeClass('bell-transparent').addClass('bell-green');
            tryingToReconnect = false;
            console.log('$.connection.hub.start().done');
        });
    });

    if (ineditnotification)
        inedit = new Interval(inedit, ineditnotificationinterval);

    $.connection.hub.reconnecting(function () {
        $('.bolt').removeClass('bell-green').removeClass('bell-red').addClass('bell-yellow');
        console.log('reconnecting');
        tryingToReconnect = true;
    });

    $.connection.hub.reconnected(function () {
        $('.bolt').removeClass('bell-yellow').removeClass('bell-red').addClass('bell-green');
        console.log('reconnected');
        tryingToReconnect = false;
    });

    $.connection.hub.disconnected(function () {
        if ($.connection.hub.lastError)
        { console.log("Disconnected. Reason: " + $.connection.hub.lastError.message); }
        $('.bolt').removeClass('bell-yellow').removeClass('bell-green').addClass('bell-red');
        console.log('disconnected');
        if (tryingToReconnect) {
            notifyUserOfDisconnect(); // Your function to notify user.
            setTimeout(function () {
                console.log('Trying to reconnect');
                $.connection.hub.start();
            }, 5000); // Restart connection after 5 seconds.

        }
        else {
            if (!pageLeave) {
                console.log('$.connection.hub.start');
                setTimeout(function () {
                    console.log('Trying to reconnect');
                    $.connection.hub.start();
                }, 5000); // Restart connection after 5 seconds.
            }
        }

    });

    $.connection.hub.connectionSlow(function () {
        notifyUserOfConnectionProblem(); // Your function to notify user.
    });

    if(log_id != ''){
        $('#Search').val(log_id);
        log_id='';
        qd809Serial_Lookup();
    }
});

function notifyUserOfDisconnect() {
    console.log('notifyUserOfDisconnect');

}

function notifyUserOfConnectionProblem() {
    console.log('notifyUserOfConnectionProblem');

}

function start_inedit_timer(status) {
    bell_update('bell-green')
    if (!ineditnotification)
        return;
    if (!inedit.isRunning()) {
        if (jsonqd809.record == null) {
            return;
        }
        notification(status);
        inedit.start();
    }
}

function inedit() {
    if (changed_flag)
        notification('inedit')
}

function Interval(fn, time) {
    var timer = false;
    this.start = function () {
        if (!this.isRunning())
            timer = setInterval(fn, time);
    };
    this.stop = function () {
        clearInterval(timer);
        timer = false;
    };
    this.isRunning = function () {
        return timer !== false;
    };
}

function notification(status) {
    if (!ineditnotification)
        return;
    if (jsonqd809.record == null) {
        return;
    }
    //console.log(status)
    var chatdto = {};
    chatdto.id = $.connection.hub.id;
    chatdto.qd809serialnumber = jsonqd809.record.qd809serial;
    chatdto.request = 'inedit';
    chatdto.status = status;
    chatdto.date = datetime();
    chatdto.firstname = jsonqd809.userfirstname;
    chatdto.lastname = jsonqd809.userlastname;
    //console.log(chatdto);
    chat.server.send(chatdto);
    switch (status) {
        case 'unsaved':
        case 'canceled':
        case 'saved':
            bell_update('bell-transparent')
            inedit.stop();
            break;
    }
}

function datetime() {
    var d = new Date;//$.now();
    return [(d.getMonth() + 1).padLeft(),
               d.getDate().padLeft(),
               d.getFullYear()].join('/') + ' ' +
              [d.getHours().padLeft(),
               d.getMinutes().padLeft(),
               d.getSeconds().padLeft()].join(':');
}

function shortdatetime() {
    var d = new Date;
    return [(d.getMonth() + 1).padLeft(),
               d.getDate().padLeft(),
               d.getFullYear()].join('/');
}

function set_overlay(toggle) {
    $('.overlay').removeClass('hide show').addClass(toggle);
}

function signalRNotify(request, updatedto, qd809serial) {
    if (ineditnotification) {
        var chatdto = {};
        chatdto.id = $.connection.hub.id;
        chatdto.request = request;
        chatdto.updatedto = updatedto;
        chatdto.date = datetime();
        chatdto.firstname = jsonqd809.userfirstname;
        chatdto.lastname = jsonqd809.userlastname;
        chatdto.qd809serial = qd809serial;
        chat.server.send(chatdto);
    }
}
function emailUpdateNotify(request, updatedto) {
    if (ineditnotification) {
        var chatdto = {};
        chatdto.id = $.connection.hub.id;
        chatdto.request = request;
        chatdto.updatedto = updatedto;
        chatdto.date = datetime();
        chatdto.firstname = jsonqd809.userfirstname;
        chatdto.lastname = jsonqd809.userlastname;
        chat.server.send(chatdto);
    }
}

function resultsUpdateNotify(request, updatedto) {
    if (ineditnotification) {
        var chatdto = {};
        chatdto.id = $.connection.hub.id;
        chatdto.request = request;
        chatdto.updatedto = updatedto;
        chatdto.date = datetime();
        chatdto.firstname = jsonqd809.userfirstname;
        chatdto.lastname = jsonqd809.userlastname;
        chat.server.send(chatdto);
    }
}

function productrangeUpdateNotify(request, updatedto) {
    if (ineditnotification) {
        var chatdto = {};
        chatdto.id = $.connection.hub.id;
        chatdto.request = request;
        chatdto.updatedto = updatedto;
        chatdto.date = datetime();
        chatdto.firstname = jsonqd809.userfirstname;
        chatdto.lastname = jsonqd809.userlastname;
        chat.server.send(chatdto);
    }
}

function idenityUpdateNotify(request, key) {
    if (ineditnotification) {
        var chatdto = {};
        chatdto.id = $.connection.hub.id;
        chatdto.request = request;
        chatdto.updatedto =key
        chatdto.date = datetime();
        chatdto.firstname = jsonqd809.userfirstname;
        chatdto.lastname = jsonqd809.userlastname;
        chat.server.send(chatdto);
    }

}

