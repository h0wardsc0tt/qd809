﻿function monitor_changes() {
    $('#fai_report .input').change(function (e) {
        $(this).attr('changed', '1');
        changed_flag = true;
        start_inedit_timer('inedit');
    });

    var unkeys = [37, 38, 39, 40,16 ,17 ,18];
    var alphaNumericNcontrols = [8, 9, 13, 16, 17, 18, 19, 20, 32, 33, 34, 35, 36, 37, 38, 39, 40, 44, 45, 46, 145, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 65, 66, 67, 68, 69, 70, 71, 72, 73, 74, 75, 76, 77, 78, 79, 80, 81, 82, 83, 84, 85, 86, 87, 88, 89, 90, 96, 97, 98, 99, 100, 101, 102, 103, 104, 105],
    illegal = {
        reg: [106, 111, 191, 220],
        shift: [56, 59, 188, 190, 191, 220, 222]
    }
    $('#fai_report input[type=text]').keyup(function (e) {
        var keyCode = (e.keyCode ? e.keyCode : e.which);
        //if (alphaNumericNcontrols.indexOf(eKey) === -1) return false;
        //if (illegal.reg.indexOf(eKey) > -1) return false;
        //if (e.shiftKey && illegal.shift.indexOf(eKey) > -1) return false;
        if (unkeys.indexOf(keyCode) > -1) return;
        //console.log(window.event.ctrlKey);
        $(this).attr('changed', '1');
        changed_flag = true;
        start_inedit_timer('inedit');
    });

    $('#fai_report .input-textarea').change(function (e) {
        $(this).attr('changed', '1');
        changed_flag = true;
        start_inedit_timer('inedit');
    });

    $('#fai_report .input-select').change(function (e) {
        $(this).attr('changed', '1');
        if ($(this).val() == '-1') {
            $(this).val('-2');
        }
        changed_flag = true;
        start_inedit_timer('inedit');
    });

    $('#fai_report .input-checkbox').change(function (e) {
        $(this).attr('changed', '1');
        changed_flag = true;
        start_inedit_timer('inedit');
    });

    $('#fai_report .toggle-checkbox').change(function (e) {
        var elm = this.id.slice(0, this.id.length - 2)
        $('#' + elm).attr('changed', '1');
        changed_flag = true;
        start_inedit_timer('inedit');
    });

    $('.toggle-checkbox').on('click', function (e) {
        var ext = this.id.substr(this.id.length - 2);
        var elm = this.id.slice(0, this.id.length - 2)
        if (this.checked) {
            var toggle = elm + (ext == '_c' ? '_p' : '_c');
            $('#' + toggle).prop('checked', false);
            $('#' + elm).val(ext == '_c' ? '2' : '1');
        } else {
            $('#' + elm).val('0');
        }
        changed_flag = true;
        start_inedit_timer('inedit');
    });

    $(".sortable").click(function (e) {
        console.log(this)
        sort_results($(this));
    });

    $('.numeric').keypress(function (e) {
        var keyCode = (e.keyCode ? e.keyCode : e.which);
        if (keyCode == 8 || keyCode == 9) return true;
        if (!$.isNumeric(String.fromCharCode(keyCode))) { return false; }
    });

    $('.qd809-lookup').keypress(function (e) {
        var keyCode = (e.keyCode ? e.keyCode : e.which);
        if (keyCode == 13) {
            qd809Serial_Lookup();
        }
    });

    // Paginate to Next page
    $('#CSP_Next_Page').click(function (e) {
        item_per_page(1);
        return false;
    });

    // Paginate to Previous page
    $('#CSP_Prev_Page').click(function (e) {
        item_per_page(-1);
        return false;
    });
    // Toggle items per page
    $('#CSP_Per_Page').change(function (e) {
        item_per_page('');
    });

    window.onbeforeunload = function (e) {
        if (changed_flag) {
            notification('unsaved');
            setTimeout(function () {
                bell_update('bell-yellow')
                changed_flag = false;
                set_overlay('show')
                //notification('inedit');
                //inedit.start();
                setTimeout(function () {
                    if ($('.busy-bell').hasClass('bell-yellow')) {
                        bell_update('bell-green')
                        changed_flag = true;
                        set_overlay('hide')
                        notification('inedit');
                        inedit.start();
                    }
                }, 2000);

            }, 500);

            e.returnValue = 'You have not saves record: ' + jsonqd809.record.qd809serial;
            return 'You have not saves record: ' + jsonqd809.record.qd809serial;
        }
    };

    $(document).keydown(function (e) {
        return;
        var keyCode = (e.keyCode ? e.keyCode : e.which);
        //console.log('key code is: ' + e.which + ' ' + (e.ctrlKey ? 'Ctrl' : '') + ' ' + (e.shiftKey ? 'Shift' : '') + ' ' + (e.altKey ? 'Alt' : ''));
        if (e.ctrlKey && !e.shiftKey) {
            switch (keyCode) {
                case 90: //z
                    newreportclick();
                    break;
                case 88://x
                    showsearchpage();
                    break;
                case 67://c
                    if ($('#fai_report').is(':visible')) {
                        update_report();
                    }
                    break;
                case 81://q
                    if ($('#show_report').is(':visible')) {
                        showreport();
                    }
                    break;
            }
        }
        if (e.ctrlKey && e.shiftKey) {
            switch (keyCode) {
                case 190: //>
                    if (!$('#CSP_Next_Page').prop('disabled')) {
                        $('#CSP_Next_Page').click();
                    }
                    break;
                case 188: //<
                    if (!$('#CSP_Prev_Page').prop('disabled')) {
                        $('#CSP_Prev_Page').click();
                    }
                    break;
                case 90: //z
                    search_submit(true);
                    break;
                case 67://c
                    $('.s-form').val('');
                    break;
                case 88://x
                    $('#Search').focus();
                    $('#Search').val('');
                    break;
            }
        }
    });

    $('#update_email input[type=text]').keyup(function (e) {
        var keyCode = (e.keyCode ? e.keyCode : e.which);
        $(this).attr('changed', '1');
        changed_flag = true;
    });

    $('#update_email input[type=checkbox]').change(function (e) {
        $(this).attr('changed', '1');
        changed_flag = true;
        start_inedit_timer('inedit');
    });

}

function update_report() {
    if ($('.overlay').hasClass('show'))
        return;
    $('.red').removeClass('red');
    var obligatorys = [];
    $('#fai_report .obligatory').each(function (index, item) {
        var label_text = $('label[for=' + this.id + ']').html();
        var label_obj = $('label[for=' + this.id + ']');
        switch ($(this).prop('type')) {
            case 'checkbox':
                break;
            case 'hidden':
                break;
            case 'select-one':
                if ($(this).val() == null) {
                    $(label_obj).addClass('red');
                    obligatorys.push(label_text);
                }
                break;
            case 'text':
            case 'textarea':
                if ($(this).val() == '') {
                    $(label_obj).addClass('red');
                    obligatorys.push(label_text);
                }
                break;
        }
    });
    if (obligatorys.length > 0) {
        $('#obligatory').center();
        //$('#obligatory').css('top', '80px');
        $('#obligatory').slideDown("slow", function () {
            setTimeout(function () {
                $('#obligatory').slideUp("slow", function () {
                });
            }, 2000);
        });
        return;
    }
    if ($("#fai_report [changed ='1']").length == 0) {
        $('#msg').html('No Changes were made.');
        $('#message').center();
        //$('#message').css('top', '80px');
        $('#message').slideDown("slow", function () {
            setTimeout(function () {
                $('#message').slideUp("slow", function () {
                });
            }, 2000);
        });
        return;
    }
 
    qd809serial = $('#qd809serial').val();
    var json_update_dto = { "status": "", "updatetype": "update", "record": {}};
    var json_update = { "status": "", "updatetype": qd809serial == '' ? "insert" : "update" };

    $('#fai_report .input').each(function (index, item) {
        input = $(this);
        if (input.attr('changed') == '1') {
            id = input.prop("id");
            json_update[id] = input.val();
        }
    });

    $('#fai_report .input-select').each(function (index, item) {
        input = $(this);
        if (input.attr('changed') == '1') {
            id = input.prop("id");
            json_update[id] = input.val();
        }
    });

    $('#fai_report .input-textarea').each(function (index, item) {
        input = $(this);
        if (input.attr('changed') == '1') {
            id = input.prop("id");
            json_update[id] = escape(input.val());

        }
    });

    $('#fai_report .input-checkbox').each(function (index, item) {
        input = $(this);
        if (input.attr('changed') == '1') {
            id = input.prop("id");
            if (id == 'isactive')
                json_update[id] = this.checked ? '0' : null;
            else
                json_update[id] = this.checked ? '1' : '0';
        }
    });
    json_update.qd809serial = qd809serial;
    json_update_dto.record = json_update;
    //console.log(json_update);
    update_qd809report(json_update);
}

function continue_function(method, parms){
    override = true;
    window[method](parms.split(','));
}

function newreportclick() {
    if (changed_flag && !override) {
        $('#continue_function').val('newreportclick');       
        $('#continue_function_parms').val('');
        $('#edit_active').center();
        //$('#edit_active').css('top', '80px');
        $('#edit_active').slideDown("slow", function () {
            // Animation complete.
        });
        return;
    }
    notification('canceled');
    override = false;
    $('#fai_search').hide();
    $('#fai_report').show();
    $('#save_report').hide();
    $('#cancel_report').hide();
    $('#show_report').hide();
    $('#search_reports').show();
    jsonqd809.record = null;
    clearreport();
    $('.deleteqd809').hide();
    $('#fai_report #datefromengineering').datepicker("setDate", new Date());
    $('#fai_report #datefromengineering').attr("changed", '1');
    $('#fai_report #integrationtestresult').val(getresultsdefault('integration'));
    $('#fai_report #integrationtestresult').attr("changed", '1');
    $('#fai_report #productiontestresult').val(getresultsdefault('production'));
    $('#fai_report #productiontestresult').attr("changed", '1');
    if ($("#fai_report #integrationtestname option[value=-1]").length == 0) {
        $("<option />", {
            val: '-1',
            text: 'No Name'
        }).appendTo($("#fai_report #integrationtestname"));
    }
    if ($("#fai_report #productiontestname option[value=-1]").length == 0) {
        $("<option />", {
            val: '-1',
            text: 'No Name'
        }).appendTo($("#fai_report #productiontestname"));
    }
    if ($("#fai_report #engineeringtestname option[value=-1]").length == 0) {
        $("<option />", {
            val: '-1',
            text: 'No Name'
        }).appendTo($("#fai_report #engineeringtestname"));
    }

    $('#productrange').focus();
}

function clearreport() {
    set_overlay('hide')
    bell_update('bell-transparent')
    $('#fai_report .input').val('');
    $('#fai_report').find(':checkbox').attr('checked', false);
    $('#fai_report .input-textarea').val('');
    $('#fai_report .input-select').val('');
    $("#fai_report .input-select option[value=-1]").prop('disabled', false);
    $("#fai_report .input-select").val('-2');
    $('#fai_form').find('[changed ="1"]').attr("changed", '0');
    $('.red').removeClass('red');
    changed_flag = false;
}

function showreport() {
    //bell_update('bell-transparent')
    $('#fai_search').hide();
    $('#fai_report').show();
    if (changed_flag) {
        $('#save_report').show();
        $('#cancel_report').show();
    }
    else {
        $('#save_report').hide();
        $('#cancel_report').hide();
    }
    $('#show_report').hide();
    $('#search_reports').show();

    void (0);
}

function showsearchpage() {
    //bell_update('bell-transparent')
    $('#message').hide();
    $('#fai_search').show();
    $('#fai_report').hide();
    $('#save_report').hide();
    $('#cancel_report').hide();
    $('#search_reports').hide();

    if (qd809serial != '' || changed_flag)
    {
        $('#show_report').show();
    } else {
        $('#show_report').hide();
    }
    void (0);
}

function updatereportpage(obj) {
    clearreport();
    qd809serial = obj.qd809serial;
    var type = '';
    $.each(obj, function (key, value) {
        type = $('#' + key).prop('type');
        switch (type) {
            case 'checkbox':
                if (key == 'isactive')
                    $('#' + key).prop('checked', value == '1' ? false : true);
                else
                    $('#' + key).prop('checked', value == '1' ? true : false);
                 break;
            case 'hidden':
                 if ($('#' + key).hasClass('test-type')) {
                    if (value == '1') {
                        $('#' + key + '_p').prop('checked',true );
                    }
                    if (value == '2') {
                        $('#' + key + '_c').prop('checked', true);
                    }
                    $('#' + key).val(value);
                }
                break;
            case 'text':
                if ($('#' + key).hasClass('input-date')) {
                    $('#' + key).datepicker("setDate", value);
                } else {
                    $('#' + key).val(value);
                }
                break;
            case 'select-one':
                //console.log(key+'   '+value);
                switch (key) {
                    case 'integrationtestname':
                        var index = jsonqd809.dropdowns.integrationtestnames.map(function (id) { return id['key']; }).indexOf(value);
                        if (index >= 0) {
                            $('#' + key).val(value)
                        } else {
                            $('#' + key).val('-2');
                        }
                        break;
                    case 'productiontestname':
                        var index = jsonqd809.dropdowns.productiontestnames.map(function (id) { return id['key']; }).indexOf(value);
                        if (index >= 0) {
                            $('#' + key).val(value)
                        } else {
                            $('#' + key).val('-2');
                        }
                        break;
                    case 'engineeringtestname':
                        var index = jsonqd809.dropdowns.engineeringtestnames.map(function (id) { return id['key']; }).indexOf(value);
                        if (index >= 0) {
                            $('#' + key).val(value)
                        } else {
                            $('#' + key).val('-2');
                        }
                        break
                    default:
                        $('#' + key).val(value == '' ? '-2' : value);
                        break;
                }
                break;
            case 'textarea':
                $('#' + key).val(unescape(value));
                break;
        }
    });
    if ($('#fai_report #integrationtestname').val() != null){
        $("#fai_report #integrationtestname option[value=-1]").remove();
        //$("#fai_report #integrationtestname option[value=-1]").prop('disabled', true);
    } else {
        if ($("#fai_report #integrationtestname option[value=-1]").length == 0) {
            $("<option />", {
                val: '-1',
                text: 'No Name'
            }).appendTo($("#fai_report #integrationtestname"));
        }
    }
    if ($('#fai_report #productiontestname').val() != null) {
        $("#fai_report #productiontestname option[value=-1]").remove();
        //$("#fai_report #productiontestname option[value=-1]").prop('disabled', true);
    } else {
        if ($("#fai_report #productiontestname option[value=-1]").length == 0) {
            $("<option />", {
                val: '-1',
                text: 'No Name'
            }).appendTo($("#fai_report #productiontestname"));
        }
    }
    if ($('#fai_report #engineeringtestname').val() != null) {
        $("#fai_report #engineeringtestname option[value=-1]").remove();
        //$("#fai_report #engineeringtestname option[value=-1]").prop('disabled', true);
    } else {
        if ($("#fai_report #engineeringtestname option[value=-1]").length == 0) {
            $("<option />", {
                val: '-1',
                text: 'No Name'
            }).appendTo($("#fai_report #engineeringtestname"));
        }
    } $('.deleteqd809').show();

    $('#productrange').focus();
}

function show_qd809_from_filter(id) {
    if (changed_flag && !override) {
        $('#continue_function').val('show_qd809_from_filter');
        $('#continue_function_parms').val(id);       
        $('#edit_active').center();
        //$('#edit_active').css('top', '80px');
        $('#edit_active').slideDown("slow", function () {
            // Animation complete.
        });
        return;
    }
    override = false;

    var d809 = alasql("select * from ? where qd809serial = '" + id + "'", [jsonqd809.dto.records]);
    if (d809.length > 0) {
        jsonqd809.record = d809[0];
        clearreport();
        showreport();
        updatereportpage(jsonqd809.record);
    }
}

function updategrid() {
    if (jsonqd809.dto == null)
        return;
    //console.log(jsonqd809.dto);
    var tbody = '';
    var tmp = [];
    var tmpval = '';
    var tmpstyle = '';
    $.each(jsonqd809.dto.records, function (index, qd809) {
        tbody += '<tr id="qd_'+qd809.qd809serial+'">';
        tbody += '<td qd="qd809serial"><a href="javascript:void(0);" onclick="show_qd809_from_filter(\'' + qd809.qd809serial + '\');">' + qd809.qd809serial + '</a></td>';
        tmpval = '';
        if (qd809.productrange != '') {
            tmp = alasql("select * from ? where key = " + qd809.productrange, [jsonqd809.dropdowns.productranges]);
            tmpval = (tmp.length == 0 ? '' : tmp[0].product_range)
            tmpstyle = (tmp.length == 0 ? '' : tmp[0].isactive == 'False' ? 'strikethrough' : '')
        }
        tbody += '<td qd="productrange" class="' + tmpstyle + '">' + tmpval + '</td>';
        tbody += '<td class="center" qd="revision">' + qd809.revision + '</td>';
        tbody += '<td qd="partnumber">' + qd809.partnumber + '</td>';
        tbody += '<td class="center" qd="datefromengineering">' + qd809.datefromengineering + '</td>';
        tmpval = '';
        if (qd809.integrationtestresult != '') {
            tmp = alasql("select * from ? where key = " + qd809.integrationtestresult, [jsonqd809.dropdowns.results]);
            tmpval = (tmp.length == 0 ? '' : tmp[0].status)
            tmpstyle = (tmp.length == 0 ? '' : tmp[0].isactive == 'False' ? ' strikethrough' : '')
        }
        tbody += '<td class="center' + tmpstyle + '" qd="integrationtestresult">' + tmpval + '</td>';
        tmpval = '';
        tmpstyle = '';
        if (qd809.productiontestresult != '') {
            tmp = alasql("select * from ? where key = " + qd809.productiontestresult, [jsonqd809.dropdowns.results]);
            tmpval = (tmp.length == 0 ? '' : tmp[0].status)
            tmpstyle = (tmp.length == 0 ? '' : tmp[0].isactive == 'False' ? ' strikethrough' : '')
        }
        tbody += '<td class="center' + tmpstyle + '" qd="productiontestresult">' + tmpval + '</td>';
        tbody += '<td class="center" qd="productiontestsignoffdate">' + qd809.productiontestsignoffdate + '</td>';
        tbody += '<td class="center" qd="datefromintegrationtest">' + qd809.datefromintegrationtest + '</td>';
        tbody += '<td class="center" qd="productiontestdaterecevied">' + qd809.productiontestdaterecevied + '</td>';
        tbody += '<td qd="productionpcr">' + qd809.productionpcr + '</td>';
        tmpval = '';
        if (qd809.integrationtestname != '') {
            tmp = alasql("select * from ? where key = '" + qd809.integrationtestname + "'", [jsonqd809.dropdowns.emails]);
            tmpval = (tmp.length == 0 ? '' : tmp[0].firstname + ' ' + tmp[0].lastname)
            tmpstyle = (tmp.length == 0 ? '' : tmp[0].isactive == 'False' ? 'strikethrough' : '')
        }
        tbody += '<td class="' + tmpstyle + '" qd="integrationtestname">' + tmpval + '</td>';
        tmpval = '';
        if (qd809.productiontestname != '') {
            tmp = alasql("select * from ? where key = '" + qd809.productiontestname + "'", [jsonqd809.dropdowns.emails]);
            tmpval = (tmp.length == 0 ? '' : tmp[0].firstname + ' ' + tmp[0].lastname)
            tmpstyle = (tmp.length == 0 ? '' : tmp[0].isactive == 'False' ? 'strikethrough' : '')
        }
        tbody += '<td class="' + tmpstyle + '" qd="productiontestname">' + tmpval + '</td></tr>';
    });
    $('#Cust_Results tbody').empty();
    $('#Cust_Results tbody').html(tbody);
    $('.csp-page-start').html(((jsonqd809.dto.pagenumber - 1) * jsonqd809.dto.itemsperpage) + 1);
    $('.csp-page-end').html(((jsonqd809.dto.pagenumber - 1) * jsonqd809.dto.itemsperpage) + jsonqd809.dto.records.length);
    $('.total_leads').html(jsonqd809.dto.count);
    if((((jsonqd809.dto.pagenumber - 1) * jsonqd809.dto.itemsperpage) + jsonqd809.dto.records.length) == jsonqd809.dto.count){
        $('#CSP_Next_Page').prop('disabled', true);
    }else{
        $('#CSP_Next_Page').prop('disabled', false);
    }
    if (((jsonqd809.dto.pagenumber - 1) * jsonqd809.dto.itemsperpage) + 1 > jsonqd809.dto.itemsperpage) {
        $('#CSP_Prev_Page').prop('disabled', false);
    } else {
        $('#CSP_Prev_Page').prop('disabled', true);
    }
}

function sort_results(obj) {
    $('.header_title').removeClass('sorted');
    $('#' + obj.prop('id') + ' span:first-child').addClass('sorted');
    if (obj.hasClass('sort') || obj.hasClass('desc')) {
        $(".sortable").removeClass('sort').removeClass('desc').removeClass('asc');
        $(".sortable").addClass('sort');
        obj.removeClass('sort').addClass('asc');
        jsonqd809.dto.sortorder = 'asc';
    } else {
        $('.sortable').removeClass('sort').removeClass('desc').removeClass('asc');
        $('.sortable').addClass('sort');
        obj.removeClass('sort').addClass('desc');
        jsonqd809.dto.sortorder = 'desc';
    }
    jsonqd809.dto.sortcolumn = obj.attr('db');
    jsonqd809.dto.pagenumber = 1;
    //page_fai_ajax(JSON.stringify(leadFilter_update));
    //console.log(jsonqd809.dto);
    search_submit(false);
}

function item_per_page(direction) {
    if (direction != '') {
        jsonqd809.dto.pagenumber = jsonqd809.dto.pagenumber + direction;
    } else {
        jsonqd809.dto.pagenumber = 1;
        jsonqd809.dto.itemsperpage = parseInt($('#CSP_Per_Page').val());
    }
    search_submit(false);
}

function bell_update(color) {
    if (color == 'bell-green') {
        if ($('#fai_report').is(':visible')) {
            $('#save_report').show();
            $('#cancel_report').show();
        }
    }
    if (color == 'bell-transparent') {
        $('#save_report').hide();
        $('#cancel_report').hide();
    }
    $('.busy-bell').removeClass('bell-transparent bell-red bell-green bell-yellow');
    $('.busy-bell').addClass(color);
    $('.busy-bell').attr('title','');
}

function adminEmailChanged(response) {
    switch (response.type) {
        case 'update':
            adminEmailUpdate(response);
            break;
        case 'insert':
            adminEmailInsert(response);
            break;
    }
}

function adminEmailInsert(response) {
    var email = {};
    email.key =response.key;
    email.firstname = response.updatedto.firstname;
    email.lastname = response.updatedto.lastname;
    email.emailaddress = response.updatedto.firstname + ' ' + response.updatedto.lastname + '<' + response.updatedto.emailaddress + '>';
    email.address = response.updatedto.emailaddress;
    email.isactive = 'False';
    email.engineering = 'False';
    email.production = 'False';
    email.integration = 'False';
    $.each(response.updatedto, function (key, value) {
        switch (key) {
            case 'engineering':
            case 'production':
            case 'integration':
            case 'isactive':
                email[key] = value == '1' ? 'True' : 'False';
                break;
        }
    });
    jsonqd809.dropdowns.emails.push(email);
    updateAllDropdowns(response.key);
}

function idenityDropDownUpdate(id){
    var index = jsonqd809.dropdowns.emails.map(function (id) { return id['key']; }).indexOf(id);
    if (index >= 0) {
        jsonqd809.dropdowns.emails[index].isactive = jsonqd809.dropdowns.emails[index].isactive == 'True' ? 'False' : 'True';
        logs = alasql("select qd809serial from ? where integrationtestname = '" + id + "'", [jsonqd809.dto.records]);
        $.each(logs, function (i, item) {
            var row = $('#Cust_Results tbody').find("tr[id='qd_" + item.qd809serial + "']");
            if (row.is("tr")) {                    
                //$(row).find("td[qd=integrationtestname]").html(jsonqd809.dropdowns.emails[index].firstname + ' ' + jsonqd809.dropdowns.emails[index].lastname);
                if (jsonqd809.dropdowns.emails[index].isactive == 'True') {
                    $(row).find("td[qd=integrationtestname]").removeClass('strikethrough');
                }
                else {
                    $(row).find("td[qd=integrationtestname]").addClass('strikethrough');
                }
            }
        });
        logs = alasql("select qd809serial from ? where productiontestname = '" + id + "'", [jsonqd809.dto.records]);
        $.each(logs, function (i, item) {
            var row = $('#Cust_Results tbody').find("tr[id='qd_" + item.qd809serial + "']");
            if (row.is("tr")) {
                if (jsonqd809.dropdowns.emails[index].isactive == 'True') {
                    $(row).find("td[qd=productiontestname]").removeClass('strikethrough');
                }
                else {
                    $(row).find("td[qd=productiontestname]").addClass('strikethrough');
                }
            }
        });
    }
    updateAllDropdowns(id);
}

function changeactivestatus(key) {
    if (ineditnotification) {
        idenityUpdateNotify('iupdate',key);
    }
    else {
        idenityDropDownUpdate(key)
    }
}

function addnewuser(newuser) {
    if (ineditnotification) {
        idenityUpdateNotify('inewuser', newuser);
    }
    else {
        idenityNewUser(newuser)
    }
}

function edituser(user) {
    if (ineditnotification) {
        idenityUpdateNotify('iedituser', user);
    }
    else {
        idenityEditUser(user)
    }
}

function deleteuser(user) {
    if (ineditnotification) {
        idenityUpdateNotify('idetete', user);
    }
    else {
        idenityDeteteUser(user)
    }
}

function idenityDeteteUser(user) {
    var userInfo = user.split(',');
    //console.log(userInfo[0] + ' - ' + userInfo[1] + ' - ' + userInfo[2] + ' - ' + userInfo[3]);
    var index = jsonqd809.dropdowns.emails.map(function (id) { return id['key']; }).indexOf(userInfo[3]);
    if (index >= 0) {
        jsonqd809.dropdowns.emails.splice(index, 1);
        updateAllDropdowns(userInfo[3]);
    }
}

function idenityEditUser(user) {
    var userInfo = user.updatedto.split(',');
    var index = jsonqd809.dropdowns.emails.map(function (id) { return id['key']; }).indexOf(userInfo[3]);
    if (index >= 0) {
        jsonqd809.dropdowns.emails[index].firstname = userInfo[0];
        jsonqd809.dropdowns.emails[index].lastname = userInfo[1];
        jsonqd809.dropdowns.emails[index].address = userInfo[2];
        jsonqd809.dropdowns.emails[index].emailaddress = userInfo[0] + ' ' + userInfo[1] + '<' + userInfo[2] + '>';
        logs = alasql("select qd809serial from ? where integrationtestname = '" + userInfo[3] + "'", [jsonqd809.dto.records]);
        $.each(logs, function (i, item) {
            var row = $('#Cust_Results tbody').find("tr[id='qd_" + item.qd809serial + "']");
            if (row.is("tr")) {
                $(row).find("td[qd=integrationtestname]").html(jsonqd809.dropdowns.emails[index].firstname + ' ' + jsonqd809.dropdowns.emails[index].lastname);
                if (jsonqd809.dropdowns.emails[index].isactive == 'True') {
                    $(row).find("td[qd=integrationtestname]").removeClass('strikethrough');
                }
                else {
                    $(row).find("td[qd=integrationtestname]").addClass('strikethrough');
                }
            }
        });
        logs = alasql("select qd809serial from ? where productiontestname = '" + userInfo[3] + "'", [jsonqd809.dto.records]);
        $.each(logs, function (i, item) {
            var row = $('#Cust_Results tbody').find("tr[id='qd_" + item.qd809serial + "']");
            if (row.is("tr")) {
                $(row).find("td[qd=productiontestname]").html(jsonqd809.dropdowns.emails[index].firstname + ' ' + jsonqd809.dropdowns.emails[index].lastname);
                if (jsonqd809.dropdowns.emails[index].isactive == 'True') {
                    $(row).find("td[qd=productiontestname]").removeClass('strikethrough');
                }
                else {
                    $(row).find("td[qd=productiontestname]").addClass('strikethrough');
                }
            }
        });

    }
    updateAllDropdowns(userInfo[3]);
    //console.log(jsonqd809.dropdowns.emails[index]);
}

function idenityNewUser(newuser) {
    var userInfo = newuser.split(',');
    //console.log(userInfo[0] + ' - ' + userInfo[1] + ' - ' + userInfo[2]);
    var email = {};
    email.key = userInfo[3];
    email.firstname = userInfo[0];
    email.lastname = userInfo[1];
    email.address = userInfo[2];
    email.emailaddress = userInfo[0] + ' ' + userInfo[1] + '<' + userInfo[2] + '>';
    email.engineering = 'False';
    email.integration = 'False';
    email.isactive = 'True';
    email.production = 'False';
    jsonqd809.dropdowns.emails.push(email);
    var newrow = '<tr id="qd_' + email.key + '">';
    newrow += '<td class="text-center" style="width:30px;"><span style="color:green;" onmouseover="this.style.cursor=\'pointer\';" onclick="editEmail(\'' + email.key + '\');">Edit</span></td>';
    newrow += '<td class="emailaddr ' + (email.isactive == "False" ? "strikethrough" : "") + '" qd="emailaddress" style="width:300px;">' + email.address + '</td>';
    newrow += '<td class="text-center" style="width:50px;"><input class="emailInteg" qd="integration" type="checkbox" disabled readonly ' + (email.integration == "True" ? "checked" : "") + '/></td>';
    newrow += '<td class="text-center" style="width:50px;"><input class="emailProd" qd="production" type="checkbox" disabled readonly  ' + (email.production == "True" ? "checked" : "") + '/></td>';
    newrow += '<td class="text-center" style="width:50px;"><input class="emailEng" qd="engineering" type="checkbox" disabled readonly  ' + (email.engineering == "True" ? "checked" : "") + '/></td>';
    newrow += '</tr>';
    $('#email_table_tbody').append(newrow);
}

function adminEmailUpdate(response) {
    var index = jsonqd809.dropdowns.emails.map(function (id) { return id['key']; }).indexOf(response.key);
    //console.log(index);
    $.each(response.updatedto, function (key, value) {
        //console.log('adminEmailChanged  ' + key + '  ' + value);
        switch (key) {
            case 'engineering':
            case 'production':
            case 'integration':
                if (value == '0') {
                    jsonqd809.dropdowns.emails[index][key] = 'False';
                    jsonqd809.dropdowns[key + 'testnames'] = alasql("select key, firstname, lastname from ? where " + key + " = 'True' and isactive = 'True' order by firstname", [jsonqd809.dropdowns.emails]);
                    rebuildDropdown($('#' + key + 'testname'), jsonqd809.dropdowns[key + 'testnames'], response.key)
                    if (key == 'integration' || key == 'production') {
                        rebuildDropdown($('#s_' + key + 'testname'), jsonqd809.dropdowns[key + 'testnames'], response.key);
                        emailadminupdategrid(value, '')
                    }
                    emailadminupdategrid(jsonqd809.dropdowns.emails[index].key, '')
                    emailadminclearrecords(response.key, key + 'testname')
                }
                else {
                    jsonqd809.dropdowns.emails[index][key] = 'True';
                    jsonqd809.dropdowns[key + 'testnames'] = alasql("select key, firstname, lastname from ? where " + key + " = 'True' and isactive = 'True' order by firstname", [jsonqd809.dropdowns.emails]);
                    rebuildDropdown($('#' + key + 'testname'), jsonqd809.dropdowns[key + 'testnames'], response.key)
                    if (key == 'integration' || key == 'production') {
                        rebuildDropdown($('#s_' + key + 'testname'), jsonqd809.dropdowns[key + 'testnames'], response.key)
                    }
                }
                break;
            case 'isactive':
                jsonqd809.dropdowns.emails[index][key] = value == '1' ? 'True' : 'False';
                updateAllDropdowns(response.key);
                emailadminclearrecords(response.key, 'engineeringtestname')
                emailadminclearrecords(response.key, 'integrationtestname')
                emailadminclearrecords(response.key, 'productiontestname')
                if (value == '0')
                    emailadminupdategrid(jsonqd809.dropdowns.emails[index].key, '')

                break;
            case 'firstname':
                jsonqd809.dropdowns.emails[index][key] = value;
                jsonqd809.dropdowns.emails[index].emailaddress = value + ' ' + jsonqd809.dropdowns.emails[index].lastname + '<' + jsonqd809.dropdowns.emails[index].address + '>';
                emailadminupdategrid(jsonqd809.dropdowns.emails[index].key, jsonqd809.dropdowns.emails[index].firstname + ' ' + jsonqd809.dropdowns.emails[index].lastname)
                updateAllDropdowns(response.key);
                break;
            case 'lastname':
                jsonqd809.dropdowns.emails[index][key] = value;
                jsonqd809.dropdowns.emails[index].emailaddress = jsonqd809.dropdowns.emails[index].firstname + ' ' + value + '<' + jsonqd809.dropdowns.emails[index].address + '>';
                emailadminupdategrid(jsonqd809.dropdowns.emails[index].key, jsonqd809.dropdowns.emails[index].firstname + ' ' + jsonqd809.dropdowns.emails[index].lastname)
                updateAllDropdowns(response.key);
                break;
            case 'emailaddress':
                jsonqd809.dropdowns.emails[index].address = value;
                jsonqd809.dropdowns.emails[index].emailaddress = jsonqd809.dropdowns.emails[index].firstname + ' ' + jsonqd809.dropdowns.emails[index].lastname + '<' + jsonqd809.dropdowns.emails[index].address + '>';
                updateAllDropdowns(response.key);
                break;
        }
    });

}

function emailadminclearrecords(key, column) {
    var records = alasql("select qd809serial from ? where " + column + " = '" + key + "'", [jsonqd809.dto.records]);
    if (records.length > 0) {
        $.each(records, function (index, item) {
            item['previous' + column] = item[column];
            item[column] = '';
         });
    }
    if (jsonqd809.record != null) {
        if (jsonqd809.record[column] == key) {
            jsonqd809.record['previous' + column] = jsonqd809.record[column];
            jsonqd809.record[column] = '';
        }
    }
}

function emailadminupdategrid(key, name) {
    var grid = alasql("select qd809serial from ? where integrationtestname = '" + key + "'", [jsonqd809.dto.records]);
    if (grid.length > 0) {
        $.each(grid, function (index, item) {
            var row = $('#Cust_Results tbody').find("tr[id='qd_" + item.qd809serial + "']");
            if (row.is("tr")) {
                $(row).find("td[qd=integrationtestname]").html(name);
            }
        });
    }
}

function updateAllDropdowns(key) {
    //jsonqd809.dropdowns.engineeringtestnames = alasql("select key, firstname, lastname from ? where engineering = 'True' and isactive = 'True' order by firstname", [jsonqd809.dropdowns.emails]);
    //jsonqd809.dropdowns.integrationtestnames = alasql("select key, firstname, lastname from ? where integration = 'True' and isactive = 'True' order by firstname", [jsonqd809.dropdowns.emails]);
    //jsonqd809.dropdowns.productiontestnames = alasql("select key, firstname, lastname from ? where production = 'True' and isactive = 'True' order by firstname", [jsonqd809.dropdowns.emails]);
    jsonqd809.dropdowns.engineeringtestnames = alasql("select key, firstname, lastname, isactive  from ? where engineering = 'True' order by firstname", [jsonqd809.dropdowns.emails]);
    jsonqd809.dropdowns.integrationtestnames = alasql("select key, firstname, lastname, isactive from ? where integration = 'True' order by firstname", [jsonqd809.dropdowns.emails]);
    jsonqd809.dropdowns.productiontestnames = alasql("select key, firstname, lastname, isactive from ? where production = 'True' order by firstname", [jsonqd809.dropdowns.emails]);
    rebuildDropdown($('#s_integrationtestname'), jsonqd809.dropdowns.integrationtestnames, key)
    rebuildDropdown($('#integrationtestname'), jsonqd809.dropdowns.integrationtestnames, key)
    rebuildDropdown($('#engineeringtestname'), jsonqd809.dropdowns.engineeringtestnames, key)
    rebuildDropdown($('#productiontestname'), jsonqd809.dropdowns.productiontestnames, key)
    rebuildDropdown($('#s_productiontestname'), jsonqd809.dropdowns.productiontestnames, key)
}

function addemailtoarray(obj, key, sortOn, sortOrder) {
    var index = jsonqd809.dropdowns.emails.map(function (id) { return id['key']; }).indexOf(key);
    //console.log(key + ' ' + index);
    var email = {};
    email.key =key;
    email.firstname = jsonqd809.dropdowns.emails[index].firstname;
    email.lastname = jsonqd809.dropdowns.emails[index].lastname;
    obj.push(email);
    obj = sortArray(sortOn, sortOrder, obj)
}

function rebuildDropdown(dropdown, obj, key) {   
    var selectedValue = dropdown.val();
    if (selectedValue == null) {
        if (window.parent.jsonqd809.record != null) {// && $(dropdown).attr('id') != 's_integrationtestname'
            selectedValue = window.parent.jsonqd809.record[$(dropdown).attr('id')] == '' ? null : window.parent.jsonqd809.record[$(dropdown).attr('id')];
        }
    }
    dropdown.empty();
    $("<option />", {
        val: '-2',
        text: 'Select Name',
        disabled: true
    }).appendTo(dropdown);
    $.each(obj, function (index, item) {
        $("<option />", {
            val: item.key,
            text: item.firstname + ' ' + item.lastname,
            disabled: $(dropdown).attr('id') == 's_integrationtestname' || $(dropdown).attr('id') == 's_productiontestname' ? false : item.isactive == 'False' ? true : false,
            style: $(dropdown).attr('id') == 's_integrationtestname' || $(dropdown).attr('id') == 's_productiontestname' ? item.isactive == 'False' ? 'color:graytext' : '' : ''
        }).appendTo(dropdown);
    });
    if ($(dropdown).attr('id') == 's_integrationtestname' || $(dropdown).attr('id') == 's_productiontestname') {
        $("<option />", {
            val: '-3',
            text: 'Unassigned'
        }).appendTo(dropdown);
        $("<option />", {
            val: '-1',
            text: 'Reset'
        }).appendTo(dropdown);

    }
    else {
        if (selectedValue == null) {
            $("<option />", {
                val: '-1',
                text: 'No Name'
            }).appendTo(dropdown);
        }
    }
    console.log(selectedValue + '   ' + $(dropdown).attr('id'));
    dropdown.val(selectedValue == null ? '-2' : selectedValue);
}

function removeOption(dropdown, key) {
    var selectedValue = dropdown.val();
    dropdown.find('option[value=' + key + ']').remove();
    if (selectedValue == key)
        dropdown.val('-2');

}

function sortArray(prop, direction, obj) {
    return obj.sort(function (a, b) {
        if (direction) {
            return (a[prop] > b[prop]) ? 1 : ((a[prop] < b[prop]) ? -1 : 0);
        } else {
            return (b[prop] > a[prop]) ? 1 : ((b[prop] < a[prop]) ? -1 : 0);
        }
    });
}

function updateemailgrid() {
    jsonqd809.dropdowns.emails = sortArray('firstname', 'asc', jsonqd809.dropdowns.emails)
    var newrow = '';
    $.each(jsonqd809.dropdowns.emails, function (index, email) {
        newrow += '<tr id="qd_' + email.key + '">';
        newrow += '<td class="text-center" style="width:30px;"><span style="color:green;" onmouseover="this.style.cursor=\'pointer\';" onclick="editEmail(\'' + email.key + '\');">Edit</span></td>';
        newrow += '<td class="emailaddr ' + (email.isactive == "False" ? "strikethrough" : "") + '" qd="emailaddress" style="width:300px;">' + email.address + '</td>';
        newrow += '<td class="text-center" style="width:50px;"><input class="emailInteg" qd="integration" type="checkbox" disabled readonly ' + (email.integration == "True" ? "checked" : "") + '/></td>';
        newrow += '<td class="text-center" style="width:50px;"><input class="emailProd" qd="production" type="checkbox" disabled readonly  ' + (email.production == "True" ? "checked" : "") + '/></td>';
        newrow += '<td class="text-center" style="width:50px;"><input class="emailEng" qd="engineering" type="checkbox" disabled readonly  ' + (email.engineering == "True" ? "checked" : "") + '/></td>';
        newrow += '</tr>';
    });
    return newrow;
}

function updateprodrangegrid() {
    window.parent.jsonqd809.dropdowns.productranges = sortArray('product_range', 'asc', window.parent.jsonqd809.dropdowns.productranges)
    var newrow = '';
    $.each(window.parent.jsonqd809.dropdowns.productranges, function (index, prod) {
        //console.log(prod)
        newrow += '<tr id="qd_' + prod.key + '">';
        newrow += '<td class="text-center" style="width:30px;"><span class="edit" style="color:green;text-decoration: underline green" onmouseover="this.style.cursor=\'pointer\';" onclick="editprod(this,\'' + prod.key + '\');">Edit</span></td>';
        newrow += '<td class="text-center" style="width:30px;"><span class="delete" style="color:red;text-decoration: underline red" onmouseover="this.style.cursor=\'pointer\';" onclick="editprod(this,\'' + prod.key + '\');">Del</span></td>';
        newrow += '<td class="prod" qd="prod" style="width:300px;"><input type="text" class="noBorder ' + (prod.isactive == "False" ? "strikethrough" : "") + '" style="width:98%;" readonly value="' + prod.product_range + '" /></td>';
        newrow += '<td class="prod qd=active" style="width:80px;"><select id="prodselect" style="height:23px;" class="readonly noBorder"><option value="1" ' + (prod.isactive == "True" ? "selected" : "") + '>Active</option><option value="0" ' + (prod.isactive == "False" ? "selected" : "") + '>In Active</option></select></td>';
        newrow += '</tr>';
    });
    return newrow;
}

function newresultsrow() {
    var newrow = '';
    newrow += '<tr id="qd_new">';
    newrow += '<td class="text-center" style="width:30px;"><span class="edit" style="color:green;text-decoration: underline green" onmouseover="this.style.cursor=\'pointer\';" onclick="editresults(this,\'new\');">Save</span></td>';
    newrow += '<td class="text-center" style="width:30px;"><span class="delete" style="display:none;color:red;text-decoration: underline red" onmouseover="this.style.cursor=\'pointer\';" onclick="">Del</span></td>';
    newrow += '<td class="results" qd="results" style="width:300px;"><input type="text" class="" style="width:98%;" value="" /></td>';
    newrow += '<td class="results qd=active" style="width:80px;"><select id="resultsselect" style="height:23px;" class=""><option value="1" selected>Active</option><option value="0">In Active</option></select></td>';
    newrow += '</tr>';
    return newrow;
}

function updateresultsgrid() {
    jsonqd809.dropdowns.results = sortArray('status', 'asc', jsonqd809.dropdowns.results)
    var newrow = '';
    $.each(jsonqd809.dropdowns.results, function (index, results) {
        newrow += '<tr id="qd_' + results.key + '">';
        newrow += '<td class="text-center" style="width:30px;"><span class="edit" style="color:green;text-decoration: underline green" onmouseover="this.style.cursor=\'pointer\';" onclick="editresults(this,\'' + results.key + '\');">Edit</span></td>';
        newrow += '<td class="text-center" style="width:30px;"><span class="delete" style="color:red;text-decoration: underline red" onmouseover="this.style.cursor=\'pointer\';" onclick="editresults(this,\'' + results.key + '\');">Del</span></td>';
        newrow += '<td class="prod" qd="results" style="width:300px;"><input type="text" class="noBorder ' + (results.isactive == "False" ? "strikethrough" : "") + '" style="width:98%;" readonly value="' + results.status + '" /></td>';
        newrow += '<td class="prod qd=active" style="width:80px;"><select id="resultsselect" style="height:23px;" class="readonly noBorder"><option value="1" ' + (results.isactive == "True" ? "selected" : "") + '>Active</option><option value="0" ' + (results.isactive == "False" ? "selected" : "") + '>In Active</option></select></td>';
        newrow += '</tr>';
    });
    return newrow;
}

function updateresultsdefaultgrid() {
    jsonqd809.dropdowns.results = sortArray('status', 'asc', jsonqd809.dropdowns.results)
    var options = '';
    $.each(jsonqd809.dropdowns.results, function (index, results) {
        if (results.isactive) {
            options += ' <option value="' + results.key + '">' + results.status + '</option>';
        }
    });
    return options;
}

function newprodrangerow() {
    var newrow = '';
    newrow += '<tr id="qd_new">';
    newrow += '<td class="text-center" style="width:30px;"><span class="edit" style="color:green;text-decoration: underline green" onmouseover="this.style.cursor=\'pointer\';" onclick="editprod(this,\'new\');">Save</span></td>';
    newrow += '<td class="text-center" style="width:30px;"><span class="delete" style="display:none;color:red;text-decoration: underline red" onmouseover="this.style.cursor=\'pointer\';" onclick="">Del</span></td>';
    newrow += '<td class="prod" qd="prod" style="width:300px;"><input type="text" class="" style="width:98%;" value="" /></td>';
    newrow += '<td class="prod qd=active" style="width:80px;"><select id="prodselect" style="height:23px;" class=""><option value="1" selected>Active</option><option value="0">In Active</option></select></td>';
    newrow += '</tr>';
    return newrow;
}

function displaymessage(msg, closetime) {
    $('#msg').html(msg);
    $('#message').center();
    $('#message').slideDown("slow", function () {
        setTimeout(function () {
            $('#message').slideUp("slow", function () {
            });
        }, closetime);
    });
}

function editprod(obj, key) {
    var parent = $(obj).parent('td').parent('tr');
    if ($(parent).find(':input[type=text]').val() == '') {
        displaymessage('Please enter a Production Range.', 2000);
        return;
    }

    switch($(obj).html()){
        case 'Edit':
            $(obj).html('Save');
            $(parent).find(':input[type=text]').prop('readonly', false).removeClass('noBorder').removeClass('strikethrough');
            $(parent).find('select').removeClass('readonly').removeClass('noBorder');
            break;
        case 'Save':
            var dto = {};
            dto.updatetype = key == 'new' ? 'insert' : 'update';
            dto.productrange = $(parent).find(':input[type=text]').val();
            if (key != 'new') {
                var index = window.parent.jsonqd809.dropdowns.productranges.map(function (id) { return id['key']; }).indexOf(parseInt(key));
                dto.previousproductrange = window.parent.jsonqd809.dropdowns.productranges[index].product_range;
            } else {
                dto.previousproductrange = null;
            }
            dto.isactive = $(parent).find('select').val();
            dto.key = key == 'new' ? null : key;
            dto.status = null;
            productrangeupdate_ajax(dto);
            break;
        case 'Del':
            var ret = confirm('Are you sure you wish to delete ' + $(parent).find(':input[type=text]').val() + '?');
            if (!ret) return;
            var dto = {};
            dto.updatetype = 'delete';
            dto.productrange = $(parent).find(':input[type=text]').val();
            dto.previousproductrange = dto.productrange;
            dto.isactive = $(parent).find('select').val();
            dto.key = key;
            dto.status = null;
            productrangeupdate_ajax(dto);
            break;

    }
}

function editresults(obj, key) {
    var parent = $(obj).parent('td').parent('tr');
    if ($(parent).find(':input[type=text]').val() == '') {
        displaymessage('Please enter a Results.', 2000);
        return;
    }

    switch ($(obj).html()) {
        case 'Edit':
            $(obj).html('Save');
            $(parent).find(':input[type=text]').prop('readonly', false).removeClass('noBorder').removeClass('strikethrough');
            $(parent).find('select').removeClass('readonly').removeClass('noBorder');
            break;
        case 'Save':
            var dto = {};
            dto.updatetype = key == 'new' ? 'insert' : 'update';
            if (dto.updatetype == 'update') {
                if (ifdefaultvalue(key))
                    return;
            }
            dto.results = $(parent).find(':input[type=text]').val();
            if (key != 'new') {
                var index = window.parent.jsonqd809.dropdowns.results.map(function (id) { return id['key']; }).indexOf(parseInt(key));
                dto.previousresults = window.parent.jsonqd809.dropdowns.results[index].status;
            } else {
                dto.previousresults = null;
            }
            dto.isactive = $(parent).find('select').val();
            dto.key = key == 'new' ? null : key;
            dto.status = null;
            resultsupdate_ajax(dto);
            break;
        case 'Del':
            if (ifdefaultvalue(key))
                return;
            var ret = confirm('Are you sure you wish to delete ' + $(parent).find(':input[type=text]').val() + '?');
            if (!ret) return;
            var dto = {};
            dto.updatetype = 'delete';
            dto.productrange = $(parent).find(':input[type=text]').val();
            dto.previousresults = dto.results;
            dto.isactive = $(parent).find('select').val();
            dto.key = key;
            dto.status = null;
            resultsupdate_ajax(dto);
            break;
    }
}

function ifdefaultvalue(key) {
    var msg = '';
    console.log(key);
    console.log($('#results_default_table_tbody #integration').val());
    console.log($('#results_default_table_tbody #production').val());
    if ($('#results_default_table_tbody #integration').val() == key) {
        msg += 'Can not update, this results is a default value for integration';
    }
    if ($('#results_default_table_tbody #production').val() == key) {
        msg += (msg != '' ? '<br>' : '') + 'Can not update, this results is a default value for production';
    }

    if (msg != '') {
        displaymessage(msg, 2000);
        return true;
    }

    return false;
}

function editresultsdefault(obj) {
    var parent = $(obj).parent('td').parent('tr');
    switch ($(obj).html()) {
        case 'Edit':
            $(obj).html('Save');
            $(parent).find('select').removeClass('readonly').removeClass('noBorder');
            break;
        case 'Save':
            var dto = {};
            dto.updatetype = 'update';
            dto.key = $(parent).find('select').val();
            dto.results = $(parent).find('select').attr('id');
            dto.status = null;
            resultsupdatedefault_ajax(dto);
            break;
    }
}

function updateResultsScreen(dto) {
    var parent = $('#results_table_tbody').find("tr[id='qd_" + (dto.updatetype == 'insert' ? 'new' : dto.key) + "']")
    var msg = '';
    if (dto.updatetype == 'delete') {
        $(parent).hide();
        msg = 'Delete Completed';
    } else {
        msg = 'Update Completed';
        $(parent).find('span.edit').html('Edit');
        if (dto.updatetype == 'insert') {
            $(parent).attr('id', 'qd_' + dto.key);
            $(parent).find('span.delete').show();
            $(parent).find('span').attr("onclick", "editresults(this, " + dto.key + ")");
            msg = 'Insert Completed';
        }
        $(parent).find(':input[type=text]').prop('readonly', true).addClass('noBorder');
        $(parent).find('select').addClass('readonly').addClass('noBorder');
        if ($(parent).find('select').val() == '0')
            $(parent).find(':input[type=text]').addClass('strikethrough');
    }
    displaymessage(msg, 2000);

}

function updateResultsScreenDefault(dto) {
    var parent = $('#results_default_table_tbody').find("tr[id='qd_" + dto.results + "']")
    var msg = '';

    msg = 'Update Completed';
    $(parent).find('span.edit').html('Edit');
    $(parent).find('select').addClass('readonly').addClass('noBorder');
    displaymessage(msg, 2000);
}

function updateResultsDropdown(dto, from) {
    switch (dto.updatetype) {
        case 'update':
            var index = jsonqd809.dropdowns.results.map(function (id) { return id['key']; }).indexOf(parseInt(dto.key));
            //console.log(index);
            jsonqd809.dropdowns.results[index].status = dto.results
            jsonqd809.dropdowns.results[index].isactive = dto.isactive == '1' ? 'True' : 'False';
            break;
        case 'insert':
            var json = {};
            json.key = parseInt(dto.key);
            json.status = dto.results;
            json.isactive = dto.isactive == '1' ? 'True' : 'False';
            jsonqd809.dropdowns.results.push(json);
            break;
        case 'delete':
            var index = jsonqd809.dropdowns.results.map(function (id) { return id['key']; }).indexOf(parseInt(dto.key));
            jsonqd809.dropdowns.results.splice(index, 1);
            break;
    }
    jsonqd809.dropdowns.results = sortArray('status', 'asc', jsonqd809.dropdowns.results)

    rebuildResultsDropdown($('#fai_form #integrationtestresult'), jsonqd809.dropdowns.results, dto.key, dto.updatetype,'integration')
    rebuildResultsDropdown($('#fai_form #productiontestresult'), jsonqd809.dropdowns.results, dto.key, dto.updatetype,'production')
    rebuildResultsFilterDropdown($('#fai_Search_Form #s_integrationtestresult'), jsonqd809.dropdowns.results, dto.key, dto.updatetype)
    rebuildResultsFilterDropdown($('#fai_Search_Form #s_productiontestresult'), jsonqd809.dropdowns.results, dto.key, dto.updatetype)
    if (dto.updatetype == 'update' || dto.updatetype == 'delete') {
        $('#Cust_Results tbody tr').each(function (index, item) {
            col = $(item).find("td[qd=integrationtestresult]")
            if (col.is("td") && col.html() == dto.previousresults) {
                switch (dto.updatetype) {
                    case 'update':
                        col.html(dto.results);
                        col.removeClass('strikethrough');
                        if (dto.isactive == '0') {
                            col.addClass('strikethrough');
                        }
                        break;
                    case 'delete':
                        col.removeClass('strikethrough');
                        col.addClass('red');
                        break;
                }
            }
            col = $(item).find("td[qd=productiontestresult]")
            if (col.is("td") && col.html() == dto.previousresults) {
                switch (dto.updatetype) {
                    case 'update':
                        col.html(dto.results);
                        col.removeClass('strikethrough');
                        if (dto.isactive == '0') {
                            col.addClass('strikethrough');
                        }
                        break;
                    case 'delete':
                        col.removeClass('strikethrough');
                        col.addClass('red');
                        break;
                }
            }

        });
    }
}

function updateResultsDefault(dto, from) {
    switch (dto.updatetype) {
        case 'update':
            var index = jsonqd809.dropdowns.results.map(function (id) { return id[dto.results + "defaultvalue"]; }).indexOf('True');
            jsonqd809.dropdowns.results[index][dto.results + "defaultvalue"] = 'False';
            index = jsonqd809.dropdowns.results.map(function (id) { return id['key']; }).indexOf(parseInt(dto.key));
            jsonqd809.dropdowns.results[index][[dto.results + "defaultvalue"]] = 'True';
            break;
    }
}

function updateProductRangeScreen(dto) {
    var parent = $('#prodrange_table_tbody').find("tr[id='qd_" + (dto.updatetype == 'insert' ? 'new' : dto.key) + "']")
    var msg = '';
    if (dto.updatetype == 'delete') {
        $(parent).hide();
        msg = 'Delete Completed';
    } else {
        msg = 'Update Completed';
        $(parent).find('span.edit').html('Edit');
        if (dto.updatetype == 'insert') {
            $(parent).attr('id', 'qd_' + dto.key);
            $(parent).find('span.delete').show();
            $(parent).find('span').attr("onclick", "editprod(this, " + dto.key + ")");
            msg = 'Insert Completed';
        }
        $(parent).find(':input[type=text]').prop('readonly', true).addClass('noBorder');
        $(parent).find('select').addClass('readonly').addClass('noBorder');
        if ($(parent).find('select').val() == '0')
            $(parent).find(':input[type=text]').addClass('strikethrough');
    }
    displaymessage(msg, 2000);

}

function updateProductRangeDropdown(dto,from) {
    //console.log('updateProductRangeDropdown');
    //console.log(dto);
    switch(dto.updatetype){
        case 'update':
            var index = jsonqd809.dropdowns.productranges.map(function (id) { return id['key']; }).indexOf(parseInt(dto.key));
            //console.log(index);
            jsonqd809.dropdowns.productranges[index].product_range = dto.productrange
            jsonqd809.dropdowns.productranges[index].isactive = dto.isactive == '1' ? 'True' : 'False';
            break;
        case 'insert':
            var json = {};
            json.key = parseInt(dto.key);
            json.product_range = dto.productrange;
            json.isactive = dto.isactive == '1' ? 'True' : 'False';
            jsonqd809.dropdowns.productranges.push(json);
            break;
        case 'delete':
            var index = jsonqd809.dropdowns.productranges.map(function (id) { return id['key']; }).indexOf(parseInt(dto.key));
            jsonqd809.dropdowns.productranges.splice(index, 1);
            break;
    }
    jsonqd809.dropdowns.productranges = sortArray('product_range', 'asc', jsonqd809.dropdowns.productranges)
    rebuildProductRangeDropdown($('#fai_form #productrange'), jsonqd809.dropdowns.productranges, dto.key, dto.updatetype)
    if (dto.updatetype == 'update' || dto.updatetype == 'delete') {
        $('#Cust_Results tbody tr').each(function (index, item) {
            col = $(item).find("td[qd=productrange]")
            if (col.is("td") && col.html() == dto.previousproductrange) {
                switch (dto.updatetype) {
                    case 'update':
                        col.html(dto.productrange);
                        col.removeClass('strikethrough');
                        if (dto.isactive == '0') {
                            col.addClass('strikethrough');
                        }
                        break;
                    case 'delete':
                        col.removeClass('strikethrough');
                        col.addClass('red');
                        break;
                }
             }
        });
    }
    //if (from == 'signalR') {
    //    if (jsonUpdate.id == $.connection.hub.id) {
    //        var o = document.getElementsByTagName('iframe')['dbAdminModal_iframe'];
    //        o.contentWindow.addnewProductRangeCompleted(json);
    //    }
    //} else {
    //    addnewProductRangeCompleted(json);
    //}

}

function rebuildResultsDropdown(dropdown, obj, key, updatetype, element) {
    var selectedValue = dropdown.val();
    dropdown.empty();
    $.each(obj, function (index, item) {
        $("<option />", {
            val: item.key,
            text: item.status,
            disabled: item.isactive == "True" ? false : true
        }).appendTo(dropdown);
    });
    dropdown.val(selectedValue == null ? window.parent.getresultsdefault(element) : selectedValue);
}

function getresultsdefault(element) {
    console.log("select key from ? where " + element + "defaultvalue = 'True'");
    var tmp = alasql("select key from ? where " + element + "defaultvalue = 'True'", [jsonqd809.dropdowns.results]);
    return tmp[0].key;
}

function rebuildResultsFilterDropdown(dropdown, obj, key, updatetype) {
    var selectedValue = dropdown.val();
    dropdown.empty();

    $("<option />", {
        val: '-2',
        text: 'Select Results',
        disabled: true
    }).appendTo(dropdown);

    $.each(obj, function (index, item) {
        $("<option />", {
            val: item.key,
            text: item.status,
            disabled: false,
            style: item.isactive == 'False' ? 'color:graytext' : ''
        }).appendTo(dropdown);
    });
    $("<option />", {
        val: '-3',
        text: 'Unassigned',
        disabled: false
    }).appendTo(dropdown);
    $("<option />", {
        val: '-1',
        text: 'Reset',
        disabled: false
    }).appendTo(dropdown);

    $(dropdown).find('option').each(function () {
        if ($(this).is(':disabled') && $(this).val() != '-2') {
            $(this).css({ 'color': 'graytext' });
        }
    });
    dropdown.val(selectedValue == null ? '-2' : selectedValue);
}

function rebuildProductRangeDropdown(dropdown, obj, key,updatetype) {
    var selectedValue = dropdown.val();
    dropdown.empty();
    $("<option />", {
        val: '-2',
        text: 'Select Product Range',
        disabled: true
    }).appendTo(dropdown);

    $.each(obj, function (index, item) {
        $("<option />", {
            val: item.key,
            text: item.product_range,
            disabled: item.isactive == "True" ? false : true
        }).appendTo(dropdown);
    });
 
    //$(dropdown).find('option').each(function () {
    //    if ($(this).is(':disabled') && $(this).val() != '-2') {
    //        $(this).css({ 'color': 'red' });
    //    }
    //});
    dropdown.val(selectedValue == null ? '-2' : selectedValue);
}

function radioFromClick(key) {
    //console.log(key);
    var row = $('#transferFrom_table tbody').find("tr[id='qd_" + key + "']");
    $('#transferTo_table tbody').find("tr").show();
    $('#transferTo_table tbody').find("tr[id='qd_" + key + "']").hide();
    $('#transferTo_table tbody input:radio').attr('checked', false);
    $('#transferTo_table tbody input:radio').attr('disabled', true);
    row.find("input:checkbox").each(function () {
        if (this.checked) {
            $('#transferTo_table tbody').find('input[qd=' + $(this).attr("qd") + ']').attr('disabled', false);
        }
    });
    $('#transferTo_table > tbody  > tr').each(function () {
        if ($(this).is(':visible')) {
            var index = window.parent.jsonqd809.dropdowns.emails.map(function (id) { return id['key']; }).indexOf($(this).attr('id').replace('qd_', ''));
            if ($(this).find("input:radio[qd=integration]").attr('disabled') != 'disabled' && window.parent.jsonqd809.dropdowns.emails[index]['integration'] == 'False') {
                $(this).find("input:radio[qd=integration]").attr('disabled', true)
            }
            if ($(this).find("input:radio[qd=engineering]").attr('disabled') != 'disabled' && window.parent.jsonqd809.dropdowns.emails[index]['engineering'] == 'False') {
                $(this).find("input:radio[qd=engineering]").attr('disabled', true)
            }
            if ($(this).find("input:radio[qd=production]").attr('disabled') != 'disabled' && window.parent.jsonqd809.dropdowns.emails[index]['production'] == 'False') {
                $(this).find("input:radio[qd=production]").attr('disabled', true)
            }
        }
    });
}

function transferemailgrid(direction) {
    window.parent.jsonqd809.dropdowns.emails = sortArray('firstname', 'asc', window.parent.jsonqd809.dropdowns.emails)
    var newrow = '';
    $.each(window.parent.jsonqd809.dropdowns.emails, function (index, email) {
        if ((email.isactive == "True" && direction == "to") || (direction == "from")) {
            newrow += '<tr id="qd_' + email.key + '">';
            newrow += '<td class="text-center" style="width:30px;">' + (direction == "from" ? "<input class='transferFrom' qd='radio' type='radio' onclick=\"radioFromClick('" + email.key + "')\" name='transferfrom'/>" : "");
            newrow += '<td class="emailaddr ' + (email.isactive == "False" ? "strikethrough" : "") + '" qd="emailaddress" style="width:300px;">' + email.address + '</td>';
            newrow += '<td class="text-center" style="width:50px;"><input class="emailInteg ' + (direction == "to" ? "toggle-radio" : "") + '" disabled qd="integration"' + (direction == "from" ? " type=\"checkbox\"" : " type=\"radio\" name=\"integration\"") + (direction == "from" ? (email.integration == "True" ? "checked" : "") : "") + '/></td>';
            newrow += '<td class="text-center"  style="width:50px;"><input class="emailProd ' + (direction == "to" ? "toggle-radio" : "") + '" disabled qd="production"' + (direction == "from" ? " type=\"checkbox\"" : " type=\"radio\" name=\"production\"") + (direction == "from" ? (email.production == "True" ? "checked" : "") : "") + '/></td>';
            newrow += '<td class="text-center"  style="width:50px;"><input class="emailEng ' + (direction == "to" ? "toggle-radio" : "") + '" disabled qd="engineering"' + (direction == "from" ? " type=\"checkbox\"" : " type=\"radio\" name=\"engineering\"") + (direction == "from" ? (email.engineering == "True" ? "checked" : "") : "") + '/></td>';
            newrow += '</tr>';
        }
    });
    return newrow;
}

function updatetransferEmail() {
    if (!($('#transferFrom_table tbody input[type="radio"]:checked').val())) {
        $('#msg').html('A "Transfer From" has not been selected.');
        $('#message').center();
        $('#message').slideDown("slow", function () {
            setTimeout(function () {
                $('#message').slideUp("slow", function () {
                });
            }, 2000);
        });
        return;
    }

    if (!($('#transferTo_table tbody input[type="radio"]:checked').val())) {
        $('#msg').html('A "Transfer To" has not been selected.');
        $('#message').center();
        $('#message').slideDown("slow", function () {
            setTimeout(function () {
                $('#message').slideUp("slow", function () {
                });
            }, 2000);
        });
        return;
    }

    var jsonUpdate = {}
    jsonUpdate.list = []
    jsonUpdate.fromid = $('#transferFrom_table tbody input[type="radio"]:checked').parent('td').parent('tr').attr('id').replace('qd_', '');
    $('#transferTo_table tbody input[type="radio"]:checked').each(function () {
        var json = {};
        json.toList = $(this).attr('qd');
        json.toid = $(this).parent('td').parent('tr').attr('id').replace('qd_', '');
        jsonUpdate.list.push(json);
    });
    //console.log(jsonUpdate);
    email_transfer_ajax(jsonUpdate);
}

function transferemail(jsonUpdate) {
    console.log(jsonUpdate);
    var updatedto = JSON.parse(jsonUpdate.updatedto);
    var sql = '';
    $.each(updatedto.list, function (index, email) {
        sql += (sql == '' ? '' : ' or ') + email.toList + 'testname = ' + "'" + updatedto.fromid + "'";
    });
    var logs = alasql("select * from ? where " + sql, [jsonqd809.dto.records]);
    $.each(logs, function (index, item) {
        var offset = jsonqd809.dto.records.map(function (qd809serial) { return qd809serial['qd809serial']; }).indexOf(item.qd809serial);
        $.each(updatedto.list, function (index, email) {
            if (email.toList == 'integration') {
                var row = $('#Cust_Results tbody').find("tr[id='qd_" + item.qd809serial + "']");
                if (row.is("tr")) {
                    var i = jsonqd809.dropdowns.emails.map(function (id) { return id['key']; }).indexOf(email.toid);
                    if (i >= 0) {
                        $(row).find("td[qd=" + email.toList + 'testname' + "]").html(jsonqd809.dropdowns.emails[i].firstname + ' ' + jsonqd809.dropdowns.emails[i].lastname);

                    }
                }
            }
            jsonqd809.dto.records[offset][email.toList + 'testname'] = email.toid;
            if (jsonqd809.record != null && jsonqd809.record.qd809serial == item.qd809serial) {
                //jsonqd809.record[email.toList + 'testname'] = email.toid;
                if ($('#' + email.toList + 'testname').val() == updatedto.fromid) {
                    $('#' + email.toList + 'testname').val(email.toid);
                }
            }
        });     
    });
    if (jsonUpdate.id == $.connection.hub.id) {
        var o = document.getElementsByTagName('iframe')['dbAdminModal_iframe'];
        o.contentWindow.transferCompleted();
    }
}

function cancel_report() {
    if (changed_flag && !override) {
        $('#continue_function').val('cancel_report');
        $('#continue_function_parms').val('');
        $('#edit_active').center();
        //$('#edit_active').css('top', '80px');
        $('#edit_active').slideDown("slow", function () {
            // Animation complete.
        });
        return;
    }
    notification('canceled');
    //clearreport();
    //showreport();
    //newreportclick();
    if (jsonqd809.record != null)
        updatereportpage(jsonqd809.record);
    else{
        newreportclick();
    }
}

function getemailList(key)
{
    return alasql("select * from ? where key = '" + key + "'", [jsonqd809.dropdowns.emails]);
}

